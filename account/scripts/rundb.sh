DATA=$(pwd)/.data
SHOULD_DELETE_DATA=$1
echo "$SHOULD_DELETE_DATA"


if [ "$SHOULD_DELETE_DATA" = "delete_data" ]; then 
    echo "Deleting data"; 
    sudo rm -rf $DATA
fi

if [ -d "$DATA" ]; then echo "Data directory exists"; else mkdir $DATA; fi



podman run --rm -d -p 27017:27017 --name dev_db -v ./.data:/data/db docker.io/percona/percona-server-mongodb:5.0
podman run --rm -d -p 6379:6379   --name dev_redis redis