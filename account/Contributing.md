# Contributing guide
Like the other projects, this contributing guide is just a supplementary of the main one present at [Contributing guide](https://higenku.org/legal/contributing)

## Code Style
On typescript / svelte files, please follow the [JavaScript coding style](https://javascript.info/coding-style) and in rust files follow the [Rust Coding style](https://rustc-dev-guide.rust-lang.org/conventions.html)

## extending the api
You may first talk with our team to see if someone isn't already implementing this feature. 

You must first decide if it's going to be part of the rest api or the graphql api, then:

### Graphql 
Our graphql api is structure in the following form:<br>
`server/src/graphql/[query|mutation]/[operation_group]/_[operation_name].rs`: <br>
```rs
// the following can be adapted as needed
#[derive(Debug, Deserialize, GraphQLInputObject)]
pub struct OperationNameRequest {
    // ... code
}

pub async fn operation_name(
    ctx: &mut Ctx,
    req: OperationNameRequest
) ->  ApiResult /* can be other return types as well */ {
    // ... code
}
```
then alter the  `server/src/graphql/[mutation|query]/mod.rs`:
```rs
/*This*/
pub struct Query;
/*Or*/
pub struct Mutation;

#[graphql_object(Context = CtxLock)]
impl Query /*or Mutation*/ {


    pub async fn operation_group_operation_name(
        ctx: &CtxLock, req: operation_group::OperationNameRequest
    ) -> FieldResult<T> /*or the other return type*/ {
        /*this*/
        operation_group::operation_name(&*ctx.read().await, req).await
        /*or*/
        operation_group::operation_name(&*ctx.write().await, req).await
    }
}
```

### Rest
Our rest api is structure in the following form:<br>
`server/src/api/[operation_group]/[operation_name].rs`: <br>
```rs
// the following can be adapted as needed
#[derive(Debug, Deserialize, GraphQLInputObject)]
pub struct OperationNameRequest {
    // ... code
}
#[post("/operation_name/....other_actix_parameters")]
pub async fn operation_name(
    // ....more actix code
) ->  ApiResult /* can be other return types as well */ {
    // ... code
}
```
then change  `server/src/api/mod.rs`:
```rs
use actix_web::{Scope, web};

pub mod external_auth;
pub fn routes() -> Scope {
    web::scope("/v1")
        // add the following:
        .service(operationgroup::operationgroup_route())
}
```
then change  `server/src/api/[operation_group]/mod.rs`:

```rs
/*Can be altered as needed*/
use actix_web::{web, Scope, HttpResponse};

mod operation1;
mod operation2;
mod operation3;

pub fn operation_group_route() -> Scope {
    web::scope("/external_auth")
        .service(operation1::operation1)
        .service(operation2::operation2)
        .service(operation3::operation3)
}
```