pub fn new_session(geoinfo: &str, ip: &str) -> String {
format!("Hello there,
It looks like there is a new login from your account!
The login attemp comes from {geoinfo} in {ip}

If you did not create this session, enter into your account and secure it NOW! They already have access to your account at this stage!
Any Questions, you can enter in contact https://higenku.org/community (discord and revolt will be the faster way)")
}


pub fn session_renewed(session_id: &str, ip: &str, geoinfo: &str) -> String {

    format!("Hello there, 
we noticed that your session {session_id} in {ip} was about to expire, we renewed it for you.
The request came from {ip} {geoinfo}.

If you dd not have this as an active session, please remove it in https://user.higenku.org/profile in the \"sessions\" tab

Automated Message from Higenku")
}