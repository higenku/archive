
/// ask for the change in the email
pub fn email_change(username: &str, email: &str, code: &str) -> String {
    format!("Hello {username}, good day!
It appears that a you changed your email address to {email}!
Please use this verification code {code} to verify your new email address
If this was not iniciated by you, please contact our security team in https://help.higenku.org/")
}

/// event that happens after the email changed
pub fn email_changed(username: &str, email: &str) -> String {
    format!("Hello {username}, good day!
We have successfully changed your email to {email} address and you are now verified to login again.
If this action was not done by you, please reach out to us in https://help.higenku.org/")
}