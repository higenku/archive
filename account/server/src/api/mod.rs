use actix_web::{Scope, web};

pub mod external_auth;
mod oauth;
mod user;

pub fn routes() -> Scope {
    web::scope("/v1")

        .service(external_auth::route())
        .service(oauth::route())
        .service(user::route())
}