use actix_web::{Scope, web, get, HttpResponse, http::StatusCode};
use serde::Serialize;

type OStr = Option<String>;

#[derive(Serialize)]
struct UserResponse {
    pub id: OStr,
    pub name: OStr,
    pub username: OStr,
    pub email: OStr,
}


#[get("/")]
pub async fn info() -> HttpResponse {

    HttpResponse::build(StatusCode::OK)

    .json(UserResponse {
        id: None,
        name: None,
        username: None,
        email: None,
    })
}

pub fn route() -> Scope {
    web::scope("/user")
        .service(info)
}