use actix_web::{HttpResponse, get, web, Responder};
use chrono::Utc;
use mongodb::{Database, bson::doc};
use serde_json::json;

use crate::{abstraction::AppInfo, services::Jwt, models::{User, meta}};

use super::{OAuthResult, OAuthStore, redirect_error, OAuthProvider, redirect_success};

#[get("/github")]
pub async fn back(
    result: web::Query<OAuthResult>, oauth_store: web::Data<OAuthStore>, jwt: web::Data<Jwt>,
    db: web::Data<Database>, app_info: web::Data<AppInfo>
) -> impl Responder {

    if !result.error.is_none() {
        return redirect_error("provider_error", &app_info.host);
    }

    if let Some(code) = &result.code {
        // Get access_token and check permissions information

        let OAuthProvider { client_id, client_secret, redirect_uri} = oauth_store.github();
        let res = surf::post("https://github.com/login/oauth/access_token")
            .header("content-type", "application/json")
            .header("accept", "application/json")
            .body_json(&json!({
                "client_id": &client_id,
                "client_secret": &client_secret,
                "code": &code,
                "redirect_uri": &redirect_uri
            })).unwrap().send().await;

        if res.is_err() {
            return redirect_error("get_info", &app_info.host);
        }
        let mut res = res.unwrap();

        let gh_token = res.body_json::<serde_json::Value>().await;

        if gh_token.is_err() {
            return redirect_error("get_info", &app_info.host);
        }
        let gh_token = gh_token.unwrap();

        let scope = gh_token["scope"].as_str();
        if scope.is_none() {
            return redirect_error("get_info", &app_info.host);
        }
        let scope = scope.unwrap();
        if scope != "read:user,user:email" {
            return redirect_error("get_info", &app_info.host);
        }
        let access_token = gh_token["access_token"].as_str().unwrap_or("INVALID");

        // Github emails 
        let res = surf::get("https://api.github.com/user/emails")
            .header("authorization", format!("Bearer {}", access_token))
            .recv_json::<serde_json::Value>().await;
        if res.is_err() {
            return redirect_error("get_info", &app_info.host);
        }
        let email;
        let emails = res.unwrap();
        let emails = emails.as_array().map(|e| e.clone()).unwrap_or(vec![]);

        if emails.len() == 1 {
            email = emails[0]["email"].as_str().unwrap();
        }
        else {
            let pos = emails.iter().position( |e| !e["email"].as_str().unwrap().contains("users.noreply.github.com")).unwrap_or(0);
            email = emails[pos]["email"].as_str().unwrap();
        }

        // Get provider user information
        let res = surf::get("https://api.github.com/user")
            .header("authorization", format!("Bearer {}", access_token))
            .recv_json::<serde_json::Value>().await;

        if res.is_err() {
            return redirect_error("get_info", &app_info.host);
        }
        let gh_user = res.unwrap();
        
        let id = gh_user["id"].as_i64().map(|e| e.to_string()).unwrap_or(String::from(""));
        let username = gh_user["login"].as_str().map(|e| e.to_string()).unwrap_or(String::from(""));
        let name = gh_user["name"].as_str().map(|e| e.to_string()).unwrap_or(String::from(""));
        let avatar = gh_user["avatar_url"].as_str().map(|e| e.to_string()).unwrap_or(String::from(""));

        // Db check
        let db = User::new_users_col(&db);

        let query = format!("meta.{}", meta::OAUTH_GITHUB);
        let user = match User::find(&db, doc!{
                    query: id.to_string()
                }).await {
            Ok(e) => e,
            Err(_) => return HttpResponse::InternalServerError().body("Database connection error"),
        };

        // check state
        if let Some(user_id) = &result.state {

            if user.len() != 0 {
                return HttpResponse::BadRequest().body("Bad Request")
            }

            let mut dbuser = match User::find_by_id(&db, &user_id).await {
                Ok(e) => {
                    if e.len() != 1 { return HttpResponse::InternalServerError().body("Internal Server Error") }
                    e[0].clone()
                },
                Err(_) => return HttpResponse::InternalServerError().finish(),
            };

            dbuser.meta.insert(meta::OAUTH_GITHUB.to_string(), id.to_string());

            match dbuser.update(&db).await {
                Ok(_) => {},
                Err(_) => return HttpResponse::InternalServerError().finish(),
            }

            return HttpResponse::TemporaryRedirect()
                .append_header(("Location", format!("https://{}/profile", app_info.host).as_str()))
                .body("Everything worked fine")
        }

        // no users found with a gitlab connected account
        if user.len() == 0 {

            let token = jwt.encode(&mut json!({
                "email": &email,
                "data": {
                    "provider": "github",
                    "id": &id,
                    "name": &name,
                    "username": &username,
                    "avatar": &avatar,
                },
                "exp": Utc::now()
                    .checked_add_signed(chrono::Duration::minutes(30))
                    .expect("invalid timestamp")
                    .timestamp(),
                "iat": Utc::now().timestamp()
            }), None);
            
            if token.is_err() {
                return redirect_error("token_error", &app_info.host)
            }
    
            //token was generated successfully
            return redirect_success(&token.unwrap(), &app_info.host);
        }
        
        let user = user[0].clone();

        let mut payload = json!({
            "credential": &user.email,
            "step": 0 as i32,
            "step_type": "email",
            "iat": Utc::now().timestamp(),
            "exp": Utc::now()
                .checked_add_signed(chrono::Duration::minutes(30))
                .expect("invalid timestamp")
                .timestamp(),
        });

        let token = jwt.encode(&mut payload, None);

        if token.is_err() {
            return redirect_error("token_error", &app_info.host);
        }
        let token = token.unwrap();

        if user.update(&db).await.is_err() {
            return redirect_error("db_error", &app_info.host);
        }

        return HttpResponse::TemporaryRedirect()
            .append_header(("Location", format!("https://{}/auth/login/{}", app_info.host, token).as_str()))
            .body("Everything worked fine")
    }   

    return HttpResponse::BadRequest().finish();
}