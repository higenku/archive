use actix_web::{web, Scope, HttpResponse};
use serde::Deserialize;

mod github;
mod gitlab;
mod microsoft;
mod google;

pub fn redirect_error(error_type: &str, host: &str) -> HttpResponse {
    HttpResponse::TemporaryRedirect()
        .append_header(("Location", format!("https://{host}/auth/error?error={}", error_type).as_str()))
        .body("An error Occured!")
}

pub fn redirect_success(token: &str, host: &str) -> HttpResponse {
    HttpResponse::TemporaryRedirect()
            .append_header(("Location", format!("https://{host}/auth/register/{}", token).as_str()))
            .body("Everything worked fine")
}

pub struct OAuthProvider {
    pub client_id: String,
    pub client_secret: String,
    pub redirect_uri: String,
}


type OAuthFn = fn() -> OAuthProvider;
pub struct OAuthStore {
    _gitlab: OAuthFn,
    pub _github: OAuthFn,
    pub _google: OAuthFn,
    pub _microsoft: OAuthFn,
}

impl OAuthStore {
    pub fn new(_gitlab: OAuthFn, _github: OAuthFn, _google: OAuthFn, _microsoft: OAuthFn) -> Self {
        Self {
            _gitlab,
            _github,
            _google,
            _microsoft,
        }
    }

    pub fn gitlab(&self) -> OAuthProvider {
        let func = self._gitlab;
        func()
    }

    pub fn github(&self) -> OAuthProvider {
        let func = self._github;
        func()
    }

    pub fn google(&self) -> OAuthProvider {
        let func = self._google;
        func()
    }

    pub fn microsoft(&self) -> OAuthProvider {
        let func = self._microsoft;
        func()
    }
}

#[derive(Debug, Deserialize)]
pub struct OAuthResult {
   pub code:  Option<String>,
   pub error: Option<String>,
   pub state: Option<String>,
}

pub fn route() -> Scope {
    web::scope("/extauth")
        .service(gitlab::back)
        .service(github::back)
        .service(google::back)
        .service(microsoft::back)
        .service(web::resource("/").to(|| HttpResponse::Ok()))
        // .service(google::)
}
