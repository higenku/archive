use actix_web::{web, Responder, get, HttpResponse};
use chrono::Utc;
use mongodb::Database;
use mongodb::bson::doc;
use serde_json::{Value, json};

use crate::abstraction::AppInfo;
use crate::{services::Jwt, models::{User, meta}, api::external_auth::OAuthProvider};

use super::{OAuthStore, redirect_error, redirect_success, OAuthResult};



#[get("/gitlab")]
pub async fn back(
    result: web::Query<OAuthResult>, oauth_store: web::Data<OAuthStore>, jwt: web::Data<Jwt>,
    db: web::Data<Database>, app_info: web::Data<AppInfo>
) -> impl Responder {
    if !result.error.is_none() {
        return redirect_error("provider_error", &app_info.host);
    }

    if let Some(code) = &result.code {
        let OAuthProvider { client_id, client_secret, redirect_uri } = oauth_store.gitlab();

        let res = surf::post("https://gitlab.com/oauth/token")
            .header("content-type", "application/x-www-form-urlencoded")
            .body(&*format!("grant_type=authorization_code&code={}&client_id={}&client_secret={}&redirect_uri={}", code, client_id, client_secret, redirect_uri))
            .send().await;

        if res.is_err() {
            return redirect_error("get_info", &app_info.host)
        }
        let mut res = res.unwrap();

        let body = res.take_body();
        let body = body.into_json::<Value>().await;
        if body.is_err() {
            return redirect_error("get_info", &app_info.host)
        }
        let body = body.unwrap();

        let scope = body["scope"].as_str().unwrap_or(" ").to_string();
        let access_token = body["access_token"].as_str().unwrap_or(" ").to_string();

        // checks if token has access to the user
        if !scope.contains("read_user") {
            return redirect_error("get_info", &app_info.host)
        }

        let res = surf::get(format!("https://gitlab.com/api/v4/user/?access_token={access_token}")).send().await;
        if res.is_err() {
            return redirect_error("get_info", &app_info.host)
        }

        let mut res = res.unwrap();
        let body = res.take_body();
        let body = body.into_json::<Value>().await;
        if body.is_err() {
            return redirect_error("get_info", &app_info.host)
        }
        let user = body.unwrap();

        let email = user["email"].as_str().unwrap_or(" ").to_string();
        let username = user["username"].as_str().unwrap_or(" ").to_string();
        let name = user["name"].as_str().unwrap_or(" ").to_string();
        let avatar = user["avatar_url"].as_str().unwrap_or(" ").to_string();
        let id = match user["id"].as_i64() {
            Some(e) => e,
            None => return redirect_error("get_info", &app_info.host),
        };

        let db = User::new_users_col(&db);

        let query = format!("meta.{}", meta::OAUTH_GITLAB);
        let user = match User::find(&db, doc!{
                    query: id.to_string()
                }).await {
            Ok(e) => e,
            Err(_) => return HttpResponse::InternalServerError().body("Database connection error"),
        };
        
        // check state
        if let Some(user_id) = &result.state {

            if user.len() != 0 {
                return HttpResponse::BadRequest().body("Bad Request")
            }

            let mut dbuser = match User::find_by_id(&db, &user_id).await {
                Ok(e) => {
                    if e.len() != 1 { return HttpResponse::InternalServerError().body("Internal Server Error") }
                    e[0].clone()
                },
                Err(_) => return HttpResponse::InternalServerError().finish(),
            };

            dbuser.meta.insert(meta::OAUTH_GITLAB.to_string(), id.to_string());

            match dbuser.update(&db).await {
                Ok(_) => {},
                Err(_) => return HttpResponse::InternalServerError().finish(),
            }

            return HttpResponse::TemporaryRedirect()
                .append_header(("Location", format!("https://{}/profile", app_info.host).as_str()))
                .body("Everything worked fine")
        }
        
        // no users found with a gitlab connected account
        if user.len() == 0 {

            let token = jwt.encode(&mut json!({
                "email": &email,
                "data": {
                    "provider": "gitlab",
                    "id": id,
                    "name": &name,
                    "username": &username,
                    "avatar": &avatar,
                },
                "exp": Utc::now()
                    .checked_add_signed(chrono::Duration::minutes(30))
                    .expect("invalid timestamp")
                    .timestamp(),
                "iat": Utc::now().timestamp()
            }), None);
            
            if token.is_err() {
                return redirect_error("token_error", &app_info.host)
            }
    
            return redirect_success(&token.unwrap(), &app_info.host);
        }
        
        let user = user[0].clone();

        let mut payload = json!({
            "credential": &user.email,
            "step": 0 as i32,
            "step_type": "email",
            "iat": Utc::now().timestamp(),
            "exp": Utc::now()
                .checked_add_signed(chrono::Duration::minutes(30))
                .expect("invalid timestamp")
                .timestamp(),
        });

        let token = jwt.encode(&mut payload, None);

        if token.is_err() {
            return redirect_error("token_error", &app_info.host);
        }
        let token = token.unwrap();

        if user.update(&db).await.is_err() {
            return redirect_error("db_error", &app_info.host);
        }

        return HttpResponse::TemporaryRedirect()
            .append_header(("Location", format!("https://{}/auth/login/{}", app_info.host, token).as_str()))
            .body("Everything worked fine")
    }

    return redirect_error("unknown_error", &app_info.host)
}
