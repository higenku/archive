use actix_web::{ post, HttpResponse, web, http::StatusCode};
use chrono::Utc;
use mongodb::Database;
use serde::{Deserialize, Serialize};
use serde_json::json;

use crate::{models::{App, User}, services::{SodiumCrypto, Jwt}};

#[derive(Serialize, Deserialize)]
pub struct TokenRequest {
    pub grant_type: String,
    pub code: String,
    pub redirect_uri: String,
    pub client_id: String,
    pub client_secret: String,
}

#[derive(Serialize, Deserialize)]
pub struct  OAuthToken {
    pub token_id: String,
    pub app_id: String,
    pub user_id: String,
    pub scope: String,
    pub exp: i64,
}
fn invalid_request() -> HttpResponse {
    HttpResponse::build(StatusCode::BAD_REQUEST)
    .json(json!({
        "error": "invalid_request",
    }))
}

fn invalid_client() -> HttpResponse {
    HttpResponse::build(StatusCode::BAD_REQUEST)
    .json(json!({
        "error": "invalid_client",
    }))
}


#[post("/token")]
pub async fn token(
    form: web::Form<TokenRequest>,
    mongodb: web::Data<Database>, redis: web::Data<redis::Client>,
    crypto: web::Data<SodiumCrypto>, jwt: web::Data<Jwt>
) -> HttpResponse {

    if form.grant_type != "authorization_code" {
        return invalid_request();
    }

    let appsdb = App::new_apps_col(&mongodb);
    let usersdb = User::new_users_col(&mongodb);
    let mut con = match redis.get_tokio_connection_manager().await {
        Ok(e) => e,
        Err(e) => {
            log::error!("failed to connect to redis: {}", e);
            return invalid_request();
        }
    };

    let app = match App::find_by_id(&appsdb, &form.client_id).await {
        Ok(e) => e,
        Err(_) => return invalid_client(),
    };

    let hash_res = match crypto.argon2_hash_verify(&app.auth.client_secret, &form.client_secret) {
        Ok(e) => e,
        Err(e) => {
            log::error!("{:?}", e);
            return invalid_request() 
        }
    };

    if !hash_res {
        return invalid_client();
    }

    let key = format!("oauth:code:{}", &form.code);
    let result: Option<String> = match redis::cmd("GET").arg(&key).query_async(&mut con).await {
        Ok(e) => e,
        Err(e) => {
            log::error!("failed to execute GET COMMAND in redis: {}", e);
            return invalid_request()
        }
    };

    let _ = match redis::cmd("DEL").arg(&key).query_async(&mut con).await {
        Ok(e) => e,
        Err(_) => return invalid_request(),
    };

    if result.is_none() {
        return invalid_request();
    }
    let code = result.unwrap();
    let code = code.split(":").collect::<Vec<&str>>();

    if code.len() != 3 {
        return invalid_request();
    }

    let app_id = code[0];
    let user_id = code[1];
    let scope = code[2];

    let user = match User::find_by_id(&usersdb, user_id).await {
        Ok(u) => u,
        Err(_) => return invalid_request(),
    };

    if app_id != app.id {
        return invalid_client();
    }

    let exp =  Utc::now()
        .checked_add_signed(chrono::Duration::days(2))
        .expect("invalid timestamp")
        .timestamp();

    let token_id = uuid::Uuid::new_v4().as_simple().to_string();

    let token = match jwt.encode(&mut serde_json::to_value(OAuthToken {
        token_id,
        scope: scope.to_string(),
        app_id: app.id.clone(),
        user_id: user[0].id.clone(),
        exp,
    }).unwrap(), None) {
        Ok(t) => t,
        Err(_) => return invalid_request(),
    };


    HttpResponse::build(StatusCode::OK)
        .json(json! ({
            "access_token": token,
            "token_type": "bearer",
            "expires_in": exp
        }))
}
