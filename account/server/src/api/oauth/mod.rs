use actix_web::{web, Scope};

mod token;

pub fn route() -> Scope {
    web::scope("/oauth")
        .service(token::token)
}
