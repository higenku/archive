use actix_web::web::Data;
use bson::doc;
use mongodb::{options::IndexOptions, IndexModel};
use serde_json::Value;

pub const DB: &str = "higenku";
pub const COL_USERS: &str = "xaccount_users";
pub const COL_SESSIONS: &str = "xaccount_sessions";
pub const COL_APPS: &str = "xaccount_apps";

async fn setup_mongo() -> Result<mongodb::Database, ()> {
    use mongodb::{options::ClientOptions, Client};

    log::debug!("Setting up database");
    let host = std::env::var("DB_URL").unwrap_or("mongodb://localhost:27017".to_string());
    let app_name = std::env::var("DB_APP_NAME").unwrap_or("higenku.account".to_string());

    log::info!("Connecting to {host}");

    let mut opt = match ClientOptions::parse(host).await {
        Ok(e) => e,
        Err(e) => {
            log::error!("Failed to parse client options: {e}");
            return Err(());
        }
    };

    opt.app_name = Some(app_name);

    let client = match Client::with_options(opt) {
        Ok(e) => e,
        Err(e) => {
            log::error!("Failed to create client with the provided options: {e}");
            return Err(());
        }
    };

    let db = client.database(DB);

    let names = match db.list_collection_names(None).await {
        Ok(e) => e,
        Err(e) => {
            log::error!("Failed to list collections: {e:#?}");
            panic!("Failed to list collections")
        }
    };

    // build databases if they don't exists
    if !names.contains(&String::from(COL_APPS)) {
        match db.create_collection(COL_APPS, None).await {
            Ok(_) => {}
            Err(_) => {
                log::error!("Failed to create collection: {COL_APPS}");
                panic!("Database is not working properly")
            }
        };
        let col = db.collection::<Value>(COL_APPS);

        // author_id index
        let model = IndexModel::builder()
            .keys(doc! {
                "author_id": 1
            })
            .options(Some(
                IndexOptions::builder()
                    .expire_after(Some(std::time::Duration::new(1, 0)))
                    .build(),
            ))
            .build();
        match col.create_index(model, None).await {
            Ok(_) => {
                log::info!("Index `xaccount_apps:author_id_1` created successfully");
            }
            Err(_) => {
                log::error!("Failed to create index: `xaccount_apps:author_id_1`");
                panic!("Database is not working properly")
            }
        };
    } // COL_APPS
    if !names.contains(&String::from(COL_USERS)) {
        match db.create_collection(COL_USERS, None).await {
            Ok(_) => {}
            Err(_) => {
                log::error!("Failed to create collection: {COL_APPS}");
                panic!("Database is not working properly")
            }
        };
    } // COL_USERS
    if !names.contains(&String::from(COL_SESSIONS)) {
        match db.create_collection(COL_SESSIONS, None).await {
            Ok(_) => {}
            Err(_) => {
                log::error!("Failed to create collection: {COL_APPS}");
                panic!("Database is not working properly")
            }
        };

        let col = db.collection::<Value>(COL_SESSIONS);

        // expireation Index

        let model = IndexModel::builder()
            .keys(doc! {
                "expires": 1
            })
            .options(Some(
                IndexOptions::builder()
                    .expire_after(Some(std::time::Duration::new(1, 0)))
                    .build(),
            ))
            .build();
        match col.create_index(model, None).await {
            Ok(_) => {
                log::info!("Index `xaccount_sessions:expire_1` created successfully");
            }
            Err(_) => {
                log::error!("Failed to create index: `xaccount_sessions:expire_1`");
                panic!("Database is not working properly")
            }
        };

        // user_id Index

        let model = IndexModel::builder()
            .keys(doc! {
                "user_id": 1
            })
            .build();
        match col.create_index(model, None).await {
            Ok(_) => {
                log::info!("Index `xaccount_sessions:user_id_1` created successfully");
            }
            Err(_) => {
                log::error!("Failed to create index: `xaccount_sessions:user_id_1`");
                panic!("Database is not working properly")
            }
        };
    } // COL_SESSIONS

    Ok(db)
}

fn setup_redis() -> redis::Client {
    let redis_url = std::env::var("DB_REDIS_URL").unwrap_or("redis://localhost:11042".to_string());

    let client = match redis::Client::open(redis_url) {
        Ok(e) => e,
        Err(e) => {
            log::error!("Failed to connect to redis: {:?}", e);
            panic!("Failed to connect to redis: {:?}", e);
        }
    };

    client
}

pub async fn setup_db() -> Result<(Data<mongodb::Database>, Data<redis::Client>), ()> {
    let mongodb = match setup_mongo().await {
        Ok(e) => e,
        Err(_) => {
            log::error!("Failed to connect to mongodb");
            return Err(());
        }
    };

    let redis = setup_redis();
    Ok((Data::new(mongodb), Data::new(redis)))
}
