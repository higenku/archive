use std::env;

use actix_web::{
    http::header::{HeaderMap, HeaderValue},
    web, HttpResponse,
};

use crate::api::external_auth::{OAuthProvider, OAuthStore};

pub async fn not_found() -> HttpResponse {
    HttpResponse::Ok().body("404 Not Found")
}

fn headervalue_to_string(val: Option<&HeaderValue>) -> Option<String> {
    let val = val?;

    match val.to_str() {
        Ok(e) => Some(e.to_string()),
        Err(_) => None,
    }
}
#[derive(Debug, Clone)]
pub struct RequestInfo {
    pub user_agent: String,
    pub continent: String,
    pub country: String,
    pub city: String,
    pub ip: String,
}

pub fn get_req_info(headers: &HeaderMap) -> RequestInfo {
    let continent = headervalue_to_string(headers.get("x-geo-continent"))
        .unwrap_or("Couldn't get x-geo-continent".to_string());
    let country = headervalue_to_string(headers.get("x-geo-country"))
        .unwrap_or("Couldn't get x-geo-country".to_string());
    let city = headervalue_to_string(headers.get("x-geo-city"))
        .unwrap_or("Couldn't get x-geo-city".to_string());
    let ip = headervalue_to_string(headers.get("x-geo-ip"))
        .unwrap_or("Couldn't get x-geo-ip".to_string());
    let raw_user_agent = headervalue_to_string(headers.get("user-agent"))
        .unwrap_or("Couldn't get user-agent".to_string())
        .to_lowercase();

    let mut user_agent = String::new();

    // System
    if raw_user_agent.contains("linux") {
        user_agent.push_str("Linux ");
    } else if raw_user_agent.contains("android") {
        user_agent.push_str("Android ");
    } else if raw_user_agent.contains("windows") {
        user_agent.push_str("Windows ");
    } else if raw_user_agent.contains("applewebkit") {
        user_agent.push_str("Apple ");
    } else {
        user_agent.push_str("Unknown_Os ");
    }

    // Browser
    if raw_user_agent.contains("waterfox") {
        user_agent.push_str("Waterfox");
    } else if raw_user_agent.contains("orp") {
        user_agent.push_str("Opera");
    } else if raw_user_agent.contains("brave") {
        user_agent.push_str("Opera");
    } else if raw_user_agent.contains("edge") {
        user_agent.push_str("Edge");
    } else if raw_user_agent.contains("firefox") {
        user_agent.push_str("Firefox");
    } else if raw_user_agent.contains("chrome") {
        user_agent.push_str("Chrome");
    } else if raw_user_agent.contains("safari") || raw_user_agent.contains("applewebkit") {
        user_agent.push_str("Webkit");
    } else {
        user_agent.push_str("Unknown_Browser ");
    }

    RequestInfo {
        user_agent,
        continent,
        country,
        city,
        ip,
    }
}

pub fn setup_oauth_providers() -> web::Data<OAuthStore> {
    log::info!("setting up oauth providers");

    // prepare
    let gitlab = || OAuthProvider {
        client_id: env::var("GITLAB_CLIENT_ID").expect("GITLAB_CLIENT_ID was not set"),
        client_secret: env::var("GITLAB_CLIENT_SECRET").expect("GITLAB_CLIENT_SECRET was not set"),
        redirect_uri: env::var("GITLAB_REDIRECT_URI").expect("GITLAB_REDIRECT_URI was not set"),
    };

    let github = || OAuthProvider {
        client_id: env::var("GITHUB_CLIENT_ID").expect("GIHUB_CLIENT_ID was not set"),
        client_secret: env::var("GITHUB_CLIENT_SECRET").expect("GIHUB_CLIENT_SECRET was not set"),
        redirect_uri: env::var("GITHUB_REDIRECT_URI").expect("GIHUB_REDIRECT_URI was not set"),
    };

    let google = || OAuthProvider {
        client_id: env::var("GOOGLE_CLIENT_ID").expect("GOOGLE_CLIENT_ID was not set"),
        client_secret: env::var("GOOGLE_CLIENT_SECRET").expect("GOOGLE_CLIENT_SECRET was not set"),
        redirect_uri: env::var("GOOGLE_REDIRECT_URI").expect("GOOGLE_REDIRECT_URI was not set"),
    };

    let microsoft = || OAuthProvider {
        client_id: env::var("MICROSOFT_CLIENT_ID").expect("MICROSOFT_CLIENT_ID was not set"),
        client_secret: env::var("MICROSOFT_CLIENT_SECRET")
            .expect("MICROSOFT_CLIENT_SECRET was not set"),
        redirect_uri: env::var("MICROSOFT_REDIRECT_URI")
            .expect("MICROSOFT_REDIRECT_URI was not set"),
    };

    // check
    gitlab();
    google();
    microsoft();
    github();

    log::info!("initialization of oauth providers successful");
    // return
    web::Data::new(OAuthStore::new(gitlab, github, google, microsoft))
}

pub fn setup_term() {
    fern::Dispatch::new()
        // Perform allocation-free log formatting
        .format(|out, message, record| {
            let level = record.level();
            let target = record.target();
            let file = record.file().unwrap_or("");

            // let time = chrono::Local::now().format(" [%Y-%m-%d %H:%M:%S] ");
            // let time = chrono::Local::now().format("[%H:%M:%S]");
            match level {
                log::Level::Error => out.finish(format_args!(
                    "\x1b[91m{level}\x1b[36m {target}\x1b[38;5;91m {file}\x1b[0m {message}"
                )),
                log::Level::Warn | log::Level::Trace => out.finish(format_args!(
                    "\x1b[33m{level} \x1b[38;5;91m{target}\x1b[0m {message}"
                )),
                log::Level::Info | log::Level::Debug => out.finish(format_args!(
                    "\x1b[94m{level} \x1b[38;5;91m{target}\x1b[0m {message}"
                )),
            }
        })
        .level(log::LevelFilter::Info)
        .level_for("hyper", log::LevelFilter::Error)
        .level_for("couch", log::LevelFilter::Error)
        .level_for("isahc", log::LevelFilter::Error)
        .level_for("surf::middleware::logger::native", log::LevelFilter::Error)
        .chain(std::io::stdout())
        .apply()
        .unwrap();
}
