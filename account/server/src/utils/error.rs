use juniper::{FieldResult, FieldError, graphql_value};


pub fn graphres<T, E>(result: Result<T, E>, message: &str, internal_error: &str) -> FieldResult<T>
where E: std::fmt::Debug {
    match result {
        Ok(t) => Ok(t),
        Err(e) => {
            #[cfg(debug_assertions)]
            log::error!("Error: {:#?}", e);

            Err(FieldError::new(message, graphql_value!({"internal_error": internal_error})))
        },
    }
}


pub fn graphopt<T>(option: Option<T>, message: &str, internal_error: &str) -> FieldResult<T> {
    match option {
        Some(t) => Ok(t),
        None => Err(FieldError::new(message, graphql_value!({"internal_error": internal_error}))),
    }
}

pub fn grapherr(message: &str, internal_error: &str) -> FieldError {
    FieldError::new(message, graphql_value!({"internal_error": internal_error}))
}