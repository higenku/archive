
use validator::Validate;
use crate::models::{User, UserType};

#[test]
fn should_validate_valid_user() {
    let users = vec![
        User { 
            id: "NOT NEEDED FOR VALIDATION".to_string(), 
            u_type: UserType::Normal, 
            username: "myusername".to_string(), 
            name: "myname".to_string(), 
            email: "my@email.co".to_string(), 
            auth_steps: vec![],
            ..User::default()
        },

        User { 
            id: "NOT NEEDED FOR VALIDATION".to_string(), 
            u_type: UserType::Mod, 
            username: "myuse0rname".to_string(), 
            name: "myname".to_string(), 
            email: "my@email.co".to_string(), 
            auth_steps: vec![],
            ..User::default()
        },

        User { 
            id: "NOT NEEDED FOR VALIDATION".to_string(), 
            u_type: UserType::Admin, 
            username: "myusername".to_string(), 
            name: "my name".to_string(), 
            email: "my@eemail.co".to_string(), 
            auth_steps: vec![],
            ..User::default()
        }
    ];

    for u in users {
        assert!(u.validate().is_ok());
    }
}

#[test]
fn should_invalidate_invalid_user() {
    let users = vec![
        // invalid username
        User { 
            id: "NOT NEEDED FOR VALIDATION".to_string(), 

            u_type: UserType::Mod, 
            username: "☃myusername1".to_string(), 
            name: "myname".to_string(), 
            email: "myemail.co".to_string(), 
            ..User::default()
        },

        User { 
            id: "NOT NEEDED FOR VALIDATION".to_string(), 
            u_type: UserType::Admin, 
            username: "myu#sername".to_string(), 
            name: "my name".to_string(), 
            email: "my@eemail.co".to_string(), 
            auth_steps: vec![],
            ..User::default()
        },
        
        // Too much string
        User { 
            id: "NOT NEEDED FOR VALIDATION".to_string(), 
            u_type: UserType::Admin, 
            username: "myusername".to_string(), 
            name: "VERYLONGNAME___svwBteDYVDuBZUrqwlusofWfcUrQYuTHZqxKqbyhZLWcBDkEivsVzjGbfTHLnPiJlfNvAtpoWBZzCSmCzhJdGYnXQFBkxpGxZhgENndAIZYwdgiyVbjKJzsayvmVMFomOCJeAtnjBDaFxozKVEgaERgtLHSznisPQycRKWbBoKusBdrQubGPLRYNIjWEnytoNiKaaHrJXugsNonudNMTALWOSNcUwQxTJwRAQFbCOaYaOPFRRpqJshkGIAddrAgcvMJrVmRcVvGHSTDSJRwDdsCFnUNmscWKnpoADQABTQmHZhVVvoopDIVkQPYCHjsGFRCynecBOGwVlBGMHdBikoGyxrRUEyERFCRNHIighhWLlEyaNiinfDBxUfuxCnbUCkPbKGloqqIBWIudRyqVHNakLjQNPKyOayPRbNMnazSJJgHTMyjJgsHbgIHOKsTGefTFHtLcXolGuthrQigTnmvRYBVaZUeeekvivhoZAkeVkNrgnvUdlBDYauepPAjr".to_string(), 
            email: "my@eemail.co".to_string(), 
            auth_steps: vec![],
            ..User::default()
        },
    ];

    for u in users {
        assert!(u.validate().is_err());
    }
}