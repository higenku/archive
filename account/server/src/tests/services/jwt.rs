use serde_json::json;
use crate::services::Jwt;

#[test]
fn shoud_create_token() {
    std::env::set_var("JWT_SECRET_KEY", "MY_SECRET_KEY");
    let jwt = Jwt::new();

    assert!(jwt.encode(&mut json!({
        "name": "John Doe",
        "age": 43,
        "phones": [
            "+44 1234567",
            "+44 2345678"
        ]
    }), None).is_ok());
}

#[test]
fn should_not_validate_token() {
    let tokens = vec![
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c", // Security key error
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJleHAiOjE1MTYyMzkwMjJ9.SwstOQ9j3VLhFfKO4Az2_p4QHfbwkeGeRvhwA1tFoYw", // exp error
        "invalid.token.jwt",
    ];

    std::env::set_var("JWT_SECRET_KEY", "MY_SECRET_KEY");
    let jwt = Jwt::new();
    for token in tokens {
        assert!(jwt.decode::<serde_json::Value>(token).is_err());
    }
}