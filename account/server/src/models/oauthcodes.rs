use juniper::FieldResult;

use crate::utils::error::grapherr;

pub struct OAuthCodes {
    con: redis::aio::ConnectionManager,
}

impl OAuthCodes {
    pub async fn new_redis(redis: &redis::Client) -> FieldResult<Self> {
        let con = match redis.get_tokio_connection_manager().await {
            Ok(e) => e,
            Err(_) => return Err(grapherr("Failed to connect to redis", "redis.get_tokio_connection_manager")),
        };

        Ok(Self {
            con
        })
    }

    /// Generates a oauth code for the given client, sets it in the database and returns the code
    /// returns oauth code 
    pub async fn set_oauthcode(&mut self, client_id: &str, userid: &str) -> FieldResult<String> {
        let code = uuid::Uuid::new_v4().as_simple().to_string(); 
        
        let key = format!("oauth:code:{code}");
        
        let _: () = match redis::pipe()
            // Set the code
            .cmd("SET")
            .arg(&key)
            .arg(&format!("{client_id}:{userid}"))
            .ignore()

            // set expiration
            .cmd("EXPIRE")
            .arg(&key)
            .arg(420) // 7 minutes 
            .ignore()
            
            .query_async(&mut self.con).await {
            Ok(e) => e,
            Err(e) => {
                log::error!("Redis Error: {}", e);
                return Err(grapherr("Failed to set OAuth Code", "redis.set_oauthcode"));
            },
        };
        
        return Ok(code)
    }
}