use bson::doc;
use juniper::{FieldResult, futures::{TryStreamExt}};
use mongodb::{Collection, Database};
use serde::{Deserialize, Serialize};

use crate::utils::error::grapherr;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct UserSession {
    #[serde(rename = "_id")]
    pub id: String,
    pub user_id: String,
    pub expires: bson::DateTime,
    pub user_agent: String,
    pub locked: bool,
}

impl UserSession {
    pub fn new_sessions_col(db: &Database) -> Collection<Self> {
        db.collection::<Self>(crate::db::COL_SESSIONS)
    }

    pub async fn find_all_from_user(
        col: &Collection<Self>,
        user_id: &String,
    ) -> FieldResult<Vec<UserSession>> {
        let cursor = match col.find(Some(doc! { "user_id": user_id }), None).await {
            Ok(e) => e,
            Err(e) => {
                log::error!("Database Error: {e:#?}");
                return Err(grapherr("Database error", "db.find() error"));
            }
        };

        let docs = match cursor.try_collect::<Vec<Self>>().await {
            Ok(e) => e,
            Err(e) => {
                log::error!("Failed to Collect UserSessions: {e:#?}");
                return Err(grapherr(
                    "Failed to Collect UserSessions",
                    "sessions.try_collect() error",
                ));
            }
        };

        Ok(docs)
    }

    pub async fn insert(&self, col: &Collection<Self>) -> FieldResult<()> {
        match col.insert_one(self, None).await {
            Ok(_) => Ok(()),
            Err(e) => {
                log::error!("Failed to insert user session {:#?}", e);
                Err(grapherr("Failed to save session", "db.insert_one error"))
            },
        }
    }

    pub async fn update(&self, col: &Collection<Self>) -> FieldResult<()> {
        match col
            .find_one_and_replace(doc! {"_id": &self.id}, self, None)
            .await
        {
            Ok(e) => {
                if e.is_some() {
                    return Ok(());
                } else {
                    return Err(grapherr(
                        "Failed to find the session",
                        "db.find_one_and_replace did not find anything with the given id",
                    ));
                }
            }
            Err(e) => {
                log::error!("Failed to update user session {:#?}", e);
                Err(grapherr(
                    "Failed to find the session",
                    "db.find_one_and_replace did not responde with an acceptable response",
                ))
            }
        }
    }

    pub fn new(
        session_id: &str,
        user_agent: &str,
        user_id: &str,
        expiration: chrono::DateTime<chrono::Local>,
        locked: bool,
    ) -> Self {
        Self {
            user_id: user_id.to_string(),
            id: session_id.to_string(),
            expires: expiration.into(),
            user_agent: user_agent.to_string(),
            locked,
        }
    }

    pub async fn find_with_id(col: &Collection<Self>, id: &str) -> Option<UserSession> {
        match col
            .find_one(
                Some(doc! {
                    "_id": id,
                }),
                None,
            )
            .await
        {
            Ok(e) => e,
            Err(_) => None,
        }
    }

    pub async fn authenticate_session(col: &Collection<Self>, id: &str, user_agent: &str) -> bool {
        let sess = Self::find_with_id(col, id).await;
        if sess.is_none() {
            // if no session is found, then it's not authenticated
            return false;
        }

        let sess = sess.unwrap();

        return !sess.locked && sess.user_agent == user_agent;
    }
}
