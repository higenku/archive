use std::{collections::HashMap, str::FromStr};

use chrono::{Datelike, Utc};
use jsonwebtoken::TokenData;
use juniper::{futures::TryStreamExt, FieldError};
use mongodb::{
    bson::{doc, Document},
    Collection, Database,
};

use crate::{graphql::Ctx, utils::error::grapherr};

pub mod meta {
    pub const U2FA_SECRET: &str = "higenku:auth:u2fa_secret";

    pub const OAUTH_GITLAB: &str = "higenku:oauth:gitlab";
    pub const OAUTH_GITHUB: &str = "higenku:oauth:github";
    pub const OAUTH_MICROSOFT: &str = "higenku:oauth:microsoft";
    pub const OAUTH_GOOGLE: &str = "higenku:oauth:google";
}

use serde::{Deserialize, Serialize};
use validator::{Validate, ValidationError};

use super::{AuthToken, UserSession};

#[derive(Clone, Debug, Deserialize, Serialize, PartialEq)]
pub enum AuthStep {
    PasswordLess, // email token
    TfaToken,     // ToTp token
    SecurityKey,  // yubikey as an example
}

impl ToString for AuthStep {
    fn to_string(&self) -> String {
        match self {
            AuthStep::PasswordLess => format!("PasswordLess"),
            AuthStep::TfaToken => format!("TfaToken"),
            AuthStep::SecurityKey => format!("SecurityKey"),
        }
    }
}

impl FromStr for AuthStep {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "PasswordLess" => Ok(AuthStep::PasswordLess),
            "TfaToken" => Ok(AuthStep::TfaToken),
            "SecurityKey" => Ok(AuthStep::SecurityKey),
            _ => Err(()),
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize, PartialEq, Hash)]
pub enum UserType {
    Normal = 0,
    Mod,
    Admin,
}
#[derive(Clone, Debug, Deserialize, Serialize, Hash)]
pub struct UserCrypto {
    pub crypto_publickey: String,
    pub sign_publickey: String,
    /// used as backup for the user to recover it with security key (End to End encrypted)
    pub encrypted_store: String,
    /// used for key validation on the user side
    pub keycheck: String,
    /// Used for Private key verification (stored as a hash)
    pub message: String,
}

impl Default for UserType {
    fn default() -> Self {
        Self::Normal
    }
}


#[derive(Clone, Debug, Deserialize, Validate, Serialize)]
pub struct User {
    // server defined data
    #[serde(rename = "_id")]
    pub id: String,
    pub crypto: UserCrypto,

    pub u_type: UserType,
    pub auth_steps: Vec<AuthStep>,
    //                                      V specified the client that setted the info and the only one who can get it
    //          Supposed to be used with higenku:stripe_account or higenku:payment:card_[NICKNAME]: encrypted(token)
    pub meta: HashMap<String, String>,

    #[validate(length(min = 1, max = 300), custom = "validate_username")]
    pub username: String,

    #[validate(length(min = 1, max = 300))]
    pub name: String,

    // must be at least 6: a@b.bb
    #[validate(length(min = 6, max = 300), email)]
    pub email: String,
    pub avatar: String,
}

pub fn validate_username(username: &str) -> Result<(), ValidationError> {
    let regex = regex::Regex::new("[^a-zA-Z\\d_]").unwrap();
    if regex.is_match(username) {
        Err(ValidationError::new("Invalid Username"))
    } else {
        Ok(())
    }
}

impl Default for User {
    fn default() -> Self {
        Self {
            auth_steps: vec![AuthStep::PasswordLess],
            id: Default::default(),
            avatar: Default::default(),
            u_type: Default::default(),
            meta: Default::default(),
            username: Default::default(),
            name: Default::default(),
            email: Default::default(),
            crypto: UserCrypto {
                crypto_publickey: String::default(),
                sign_publickey: String::default(),
                encrypted_store: String::default(),
                keycheck: String::default(),
                message: String::default(),
            },
        }
    }
}

impl User {
    pub fn new(
        name: &str,
        username: &str,
        email: &str,
        message: &str,
        crypto_pubkey: &str,
        sign_pubkey: &str,
        encrypted_store: &str,
    ) -> Self {
        Self {
            crypto: UserCrypto {
                crypto_publickey: crypto_pubkey.to_string(),
                sign_publickey: sign_pubkey.to_string(),

                message: message.to_string(),
                encrypted_store: encrypted_store.to_string(),
                keycheck: uuid::Uuid::new_v4().as_simple().to_string(),
            },

            id: format!(
                "u_{}{}",
                Utc::now().year(),
                uuid::Uuid::new_v4().as_simple().to_string()
            ),
            avatar: format!("https://avatars.dicebear.com/api/identicon/{username}.svg"),
            u_type: UserType::Normal,
            auth_steps: vec![AuthStep::PasswordLess],
            username: username.to_string(),
            name: name.to_string(),
            email: email.to_string(),
            meta: HashMap::from([]),
        }
    }
}

/// Database implementation
impl User {
    pub fn new_users_col(db: &Database) -> Collection<Self> {
        db.collection::<Self>(crate::db::COL_USERS)
    }
    
    pub async fn from_token(
        col: &Collection<Self>,
        ctx: &Ctx,
    ) -> Result<(User, TokenData<AuthToken>), FieldError> {
        let token = ctx.refresh_token.clone();
        let user_agent = ctx.req_info.user_agent.clone();

        if token.is_none() {
            return Err(grapherr(
                "Refresh Token not supplied",
                "Server failed to authenticate user",
            ));
        }

        let token = token.unwrap();
        let token = ctx.jwt.decode::<AuthToken>(&token)?;

        let sessions_db = UserSession::new_sessions_col(&ctx.db);
        
        if !UserSession::authenticate_session(&sessions_db, &token.claims._id, &user_agent).await {
            return Err(grapherr(
                "Invalid Session",
                "user.sessions does not contain the given session",
            ));
        }

        let user = User::find_by_id(&col, &token.claims.user_id).await?;

        if user.len() == 0 {
            return Err(grapherr(
                "User not Found",
                "Server failed to get user from database",
            ));
        }
        let user = user[0].clone();
        Ok((user, token))
    }

    pub async fn find(col: &Collection<Self>, filter: Document) -> Result<Vec<Self>, FieldError> {
        Self::find_opt(col, Some(filter)).await
    }

    pub async fn find_opt(
        col: &Collection<Self>,
        filter: Option<Document>,
    ) -> Result<Vec<Self>, FieldError> {
        let cursor = match col.find(filter, None).await {
            Ok(e) => e,
            Err(e) => {
                log::error!("Database Error: {e:#?}");
                return Err(grapherr("Database error", "db.find() error"));
            }
        };

        let docs = match cursor.try_collect::<Vec<Self>>().await {
            Ok(e) => e,
            Err(e) => {
                log::error!("Failed to Collect Users: {e:#?}");
                return Err(grapherr(
                    "Failed to Collect Users",
                    "users.try_collect() error",
                ));
            }
        };

        Ok(docs)
    }

    pub async fn find_by_id(col: &Collection<Self>, id: &str) -> Result<Vec<Self>, FieldError> {
        User::find(col, doc! { "_id": id }).await
    }

    pub async fn find_by_email(
        col: &Collection<Self>,
        email: &str,
    ) -> Result<Vec<Self>, FieldError> {
        User::find(col, doc! {"email": email.to_string().to_lowercase() }).await
    }

    pub async fn find_by_username(
        col: &Collection<Self>,
        username: &str,
    ) -> Result<Vec<Self>, FieldError> {
        User::find(col, doc! {"username": username.to_string().to_lowercase() }).await
    }

    pub async fn insert(&self, col: &Collection<Self>) -> Result<(), FieldError> {
        match col.insert_one(self, None).await {
            Ok(_) => Ok(()),
            Err(e) => {
                log::error!("Failed to insert user {:#?}", e);
                Err(grapherr("Database error", "db.insert_one() error"))
            }
        }
    }

    pub async fn update(&self, col: &Collection<Self>) -> Result<(), FieldError> {
        match col
            .find_one_and_replace(
                doc! {
                    "_id": self.id.clone()
                },
                self,
                None,
            )
            .await
        {
            Ok(_) => Ok(()),
            Err(e) => {
                log::error!("Failed to save user {e:#?}");
                Err(grapherr(
                    "Failed to save user",
                    "Server failed to communicate with database",
                ))
            }
        }
    }
}
