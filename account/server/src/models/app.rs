use juniper::{futures::TryStreamExt, FieldError, FieldResult, GraphQLObject};
use mongodb::{
    bson::{doc, Document},
    Collection, Database,
};
use serde::{Deserialize, Serialize};
use sodiumoxide::{base64, randombytes};
use validator::Validate;

use crate::{
    services::SodiumCrypto,
    utils::error::{grapherr, graphres},
};

#[derive(Clone, Debug, Deserialize, Validate, Serialize, GraphQLObject)]
pub struct AppPerm {
    pub name: String,
    /// has access to the api named 'name'
    pub metadata: String,
}

#[derive(Clone, Debug, Deserialize, Serialize, GraphQLObject)]
pub struct AppAuth {
    /// hashed api key
    pub client_secret: String,
    pub redirect_uri: String,
}

#[derive(Clone, Debug, Deserialize, Validate, Serialize)]
pub struct App {
    #[serde(rename = "_id")]
    pub id: String,

    #[validate(length(min = 1, max = 300))]
    pub name: String,
    pub author_id: String,
    #[validate(length(min = 0, max = 300))]
    pub logo: String,
    pub perms: Vec<AppPerm>,

    pub auth: AppAuth,
}

impl App {
    pub fn gen_client_secret() -> String { 
        base64::encode(
            randombytes::randombytes(64),
            base64::Variant::UrlSafeNoPadding,
        )
    }

    pub fn new(
        sodium: &SodiumCrypto,
        name: String,
        author_id: String,
        logo: String,
        perms: Vec<AppPerm>,
        redirect_uri: String,
    ) -> FieldResult<(Self, String)> {

        let client_secret = Self::gen_client_secret();
        Ok((
            Self {
                id: format!("ap_{}", uuid::Uuid::new_v4().as_simple().to_string()),
                name,
                author_id,
                logo,
                perms,
                auth: AppAuth {
                    client_secret: sodium.argon2_hash_create(&client_secret)?,
                    redirect_uri: redirect_uri,
                },
            },
            client_secret,
        ))
    }

    pub fn new_apps_col(db: &Database) -> Collection<Self> {
        db.collection(crate::db::COL_APPS)
    }

    pub async fn find(col: &Collection<Self>, filter: Document) -> Result<Vec<Self>, FieldError> {
        Self::find_opt(col, Some(filter)).await
    }

    pub async fn find_opt(
        col: &Collection<Self>,
        filter: Option<Document>,
    ) -> Result<Vec<Self>, FieldError> {
        let cursor = match col.find(filter, None).await {
            Ok(e) => e,
            Err(e) => {
                log::error!("Database Error: {e:#?}");
                return Err(grapherr("Database error", "db.find() error"));
            }
        };

        let docs = match cursor.try_collect::<Vec<Self>>().await {
            Ok(e) => e,
            Err(e) => {
                log::error!("Failed to Collect Users: {e:#?}");
                return Err(grapherr(
                    "Failed to Collect Users",
                    "users.try_collect() error",
                ));
            }
        };

        Ok(docs)
    }

    pub async fn find_by_id(col: &Collection<Self>, id: &str) -> Result<Self, FieldError> {
        let apps = Self::find(col, doc! { "_id": id }).await?;

        if apps.len() <= 0 {
            return Err(grapherr("App not found", "Database couldn't find app"));
        }

        return Ok(apps[0].clone());
    }

    pub async fn find_by_author(col: &Collection<Self>, id: &str) -> Result<Vec<Self>, FieldError> {
        let apps = Self::find(col, doc! { "author_id": id }).await?;
        return Ok(apps);
    }

    pub async fn insert(&self, col: &Collection<Self>) -> Result<(), FieldError> {
        graphres(
            col.insert_one(self, None).await,
            "Database error",
            "Error creating document in database",
        )?;

        Ok(())
    }

    pub async fn update(&self, col: &Collection<Self>) -> Result<(), FieldError> {
        match col
            .find_one_and_replace(doc! {"_id": self.id.clone()}, self, None)
            .await
        {
            Ok(_) => Ok(()),
            Err(e) => {
                log::error!("Failed to save user {e:#?}");
                Err(grapherr(
                    "Failed to save user",
                    "Failed to save app to the database",
                ))
            }
        }
    }

    pub async fn delelte(&self, col: &Collection<Self>) -> FieldResult<()> {
        match col.find_one_and_delete(doc! {"_id": &self.id}, None).await {
            Ok(_) => Ok(()),
            Err(e) => {
                log::error!("Failed to delete app {e:#?}");
                Err(grapherr(
                    "Failed to delete app",
                    "db.find_one_and_delete() error",
                ))
            }
        }
    }
}
