use serde::{Serialize, Deserialize};
use serde_json::Value;


#[derive(Debug, Deserialize, Serialize)]
pub struct AuthToken {
    pub _id: String,
    pub user_id: String,

    pub username: String,
    pub avatar: String,

    pub exp: i64,
    pub iat: i64,
}

impl AuthToken {
    pub fn new(_id: &str, user_id: &str, username: &str, avatar: &str, exp: i64, iat: i64) -> Self {

        Self {
            _id: _id.to_string(),
            user_id: user_id.to_string(),
            username: username.to_string(),
            avatar: avatar.to_string(),
            exp,
            iat,
        }
    }

    pub fn to_value(&self) -> Result<Value, serde_json::Error> {
        serde_json::to_value(self)
    }
}