mod cookies;

// use actix_web::http::header::HeaderMap;
pub use cookies::Session;


// pub struct ReqInfo {
//     pub headers: HeaderMap,
// }

#[derive(Debug, Clone)]
pub struct AppInfo {
    pub cryptokey: String,
    pub secretkey: String,
    
    pub host: String,
    pub cookiehost: String,
}