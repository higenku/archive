use std::{collections::HashMap};

use actix_web::{cookie::{Cookie, SameSite}, HttpResponse, HttpRequest};
use time::{Duration};

struct CookieStore {
    ismod: bool,
    cookie: String
}

impl CookieStore {
    pub fn new(ismod: bool, cookie: String) -> Self { 
        Self { ismod, cookie } 
    }
}

pub struct Session {
    jar: HashMap<String, CookieStore>,
    pub made_first_change: bool,
}

impl Session {
    #[allow(unused)]
    pub fn new_req(req: &HttpRequest) -> Result<Self, String> {
        let cookies = match req.cookies() {
            Ok(e) => e,
            Err(_) => return Err(format!("Failed to extract cookies from request")),
        };

        Ok(Session::new(cookies.to_owned()))
    }

    pub fn new(cookies: Vec<Cookie<'static>>) -> Self {
        let mut jar = HashMap::new();

        for cookie in cookies  {
            let encoded_cookie = cookie.encoded().to_string();
            jar.insert(cookie.name().to_string(), CookieStore::new(false, encoded_cookie));
        }

        Self { 
            jar,
            made_first_change: false,
        }
    }


    pub fn finalize(&self, res: HttpResponse) -> HttpResponse {
        let mut res = res;
        if self.made_first_change {
            for (name, e) in &self.jar {
                if e.ismod {
                    if e.cookie == "#######--@@REMOVED@@--" {
                        
                        let cookie = Cookie::build(name, "removed")
                            .expires(actix_web::cookie::time::macros::datetime!(1970-01-01 0:00 UTC))
                            .domain("localhost")
                            .path("/")
                            .finish();
                        res.add_cookie(&cookie).unwrap_or(());
                    }
                    else {
                        res.add_cookie(&Cookie::parse(e.cookie.clone()).unwrap()).unwrap_or(());
                    }
                }
            }
        }

        return res;
    }

    pub fn get(&self, name: &str) -> Option<Cookie<'static>> {
        if self.jar.contains_key(name) {
            Some(Cookie::parse(self.jar[name].cookie.clone()).unwrap())
        }
        else {
            None
        }
    }


    pub fn set(&mut self, cookie: Cookie<'static>) {
        self.made_first_change = true;
        let name = cookie.name();
        
        if self.jar.contains_key(name) {
            *self.jar.get_mut(name).unwrap() = CookieStore::new(true, cookie.encoded().to_string());
        }
        else {
            self.jar.insert(name.to_string(), CookieStore::new(true, cookie.encoded().to_string()));
        }
    }

    #[allow(unused)]
    pub fn remove(&mut self, name: &str) {
        self.made_first_change = true;

        if self.jar.contains_key(name) {
            *self.jar.get_mut(name).unwrap() = CookieStore::new(true, "#######--@@REMOVED@@--".to_string());
        }
    }

    pub fn build_session(&mut self, cookiehost: &str, jwt: &str){
        let count: u32 = match self.get("co") {
            Some(e) => e.value().parse().unwrap_or(0),
            None => 0,
        };
    
        let count = count + 1;
    
        self.set(Cookie::build("co", format!("{}", &count))
            .same_site(SameSite::Lax)
            .secure(false)
            .http_only(false)
            .domain(cookiehost.to_string())
            .path("/")
            .max_age(Duration::days(15)) // 10 days
            .finish());
        
        let count = count - 1;
        
        self.set(Cookie::build(format!("rf-{count}"), jwt.to_string())
            .same_site(SameSite::Lax)
            .secure(false)
            .http_only(false)
            .domain(cookiehost.to_string())
            .path("/")
            .max_age(Duration::days(15)) // 10 days
            .finish());
    }
}