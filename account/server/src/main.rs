use actix_cors::Cors;
use actix_web::{
    middleware,
    web::{self, Data},
    App, HttpServer,
};
use std::{env, net::IpAddr, path::Path};

use dotenv::dotenv;

#[cfg(test)]
mod tests;

mod abstraction;
mod api;
mod db;
mod graphql;
mod models;
mod scheduler;
mod services;
mod templates;
mod utils;

use crate::graphql::generate_schema;
use services::{Captcha, Jwt, Mailer};

use crate::{abstraction::AppInfo, services::SodiumCrypto};

#[actix_web::main]
async fn main() {
    println!("setting up terminal");
    utils::setup_term();

    log::info!("setting environment");
    if Path::new(".env").exists() {
        dotenv().ok();
    }

    log::info!("setting up crypto");
    match sodiumoxide::init() {
        Ok(_) => log::info!("crypto Initialization Successful!"),
        Err(_) => {
            log::error!("Failed to initialize the crypto library");
            return;
        }
    };

    log::info!("setting up host address");

    let port = env::var("PORT")
        .unwrap_or("3001".to_string())
        .parse::<u16>()
        .unwrap();
    let host = env::var("HOST")
        .unwrap_or("::".to_string())
        .parse::<IpAddr>()
        .unwrap();
    log::info!("setting up services");

    // services
    let schema = Data::new(generate_schema());
    let app_info = Data::new(AppInfo {
        secretkey: env::var("SECRET_KEY").expect("No SECRET_KEY defined"),
        host: env::var("DOMAIN").expect("No DOMAIN defined"),
        cookiehost: env::var("COOKIE_DOMAIN").expect("No COOKIE_DOMAIN defined"),
        cryptokey: env::var("CRYPTO_KEY").expect("No CRYPTO_KEY defined"),
    });

    let (mongo, redis) = db::setup_db().await.expect("Failed to setup database");
    let sodiumcrypto = Data::new(SodiumCrypto::new());
    let captcha = Data::new(Captcha::new(
        env::var("HCAPTCHA_KEY").expect("HCaptcha Key not present"),
    ));
    let mailer = Data::new(Mailer::new());
    let jwt = Data::new(Jwt::new());

    // OAuth
    let oauth_store = utils::setup_oauth_providers();

    log::info!("Setting up server");

    let server = HttpServer::new(move || {
        let mut cors = Cors::default()
            .allowed_origin("https://user.higenku.org")
            .allowed_origin("https://store.higenku.org")
            .allowed_origin("https://suite.higenku.org")
            .allow_any_method()
            .supports_credentials()
            .allow_any_header()
            .max_age(3600);

        if cfg!(debug_assertions) {
            cors = cors
                .allowed_origin("http://localhost:3000")
                .allowed_origin("http://127.0.0.1:3000")
                .allowed_origin("http://localhost.localdomain:3000");

            log::warn!("Running in debug mode, localhost is an allowed origin")
        }

        App::new()
            // logging
            .wrap(middleware::Logger::new("%D %s %r"))
            .wrap(
                middleware::DefaultHeaders::new()
                    .add(("server", "Actix+HigenkuServers"))
                    .add(("Access-Control-Allow-Credentials", "true")),
                // .add(("Access-Control-Allow-Origin", "*"))
                // .add(("Access-Control-Allow-Headers", "*"))
                // .add(("Access-Control-Allow-Methods", "*"))
            )
            .wrap(middleware::NormalizePath::new(
                middleware::TrailingSlash::Trim,
            ))
            .wrap(cors)
            // databases
            .app_data(mongo.clone())
            .app_data(redis.clone())
            // Services
            .app_data(schema.clone())
            .app_data(sodiumcrypto.clone())
            .app_data(mailer.clone())
            .app_data(jwt.clone())
            .app_data(captcha.clone())
            .app_data(app_info.clone())
            // OAuth
            .app_data(oauth_store.clone())
            // Routes
            .service(graphql::route)
            .service(api::routes())
            .default_service(web::to(utils::not_found))
            .route("/ws", web::get().to(services::route))
    })
    .bind((host, port))
    .expect("Failed to bind IP Address")
    .run();

    if !cfg!(debug_assertions) {
        log::info!("Listening on http://[{host}]:{port} or http://{host}:{port}");
    } else {
        log::warn!("Listening on http://localhost:{port}");
        log::warn!("Frontend in  http://localhost:{}", port - 1);
    }

    match server.await {
        Ok(_) => {}
        Err(e) => {
            log::error!("An error occured while stopping the server {e:?}")
        }
    };
}
