#![allow(dead_code)]
use juniper::FieldResult;
use serde::de::DeserializeOwned;
use sodiumoxide::{
    base64::{self, decode, encode, Variant},
    crypto::secretbox::{self, gen_key, Key, Nonce},
    crypto::{
        pwhash::argon2id13::{
            pwhash, pwhash_verify, HashedPassword, MEMLIMIT_INTERACTIVE, OPSLIMIT_INTERACTIVE,
        },
        sign,
    },
};
use std::env;

use crate::utils::error::{grapherr, graphopt, graphres};

pub struct SodiumCrypto;

fn getkey() -> FieldResult<Key> {
    let key = env::var("SECRET_KEY").map_err(|_| {
        grapherr(
            "Environment Error, failed to get key",
            "Server environment error",
        )
    })?;

    let key = match decode(key, Variant::UrlSafe) {
        Ok(e) => e,
        Err(_) => {
            log::error!(
                "Generated key: {}",
                encode(secretbox::gen_key().0, Variant::UrlSafe)
            );
            log::error!("Failed to convert base64 key to raw");
            return Err(grapherr(
                "Environment Error, failed to decode encryption keys",
                "Server environment error",
            ));
        }
    };

    match Key::from_slice(&key) {
        Some(e) => Ok(e),
        None => {
            log::error!("Failed to get key from raw base64");
            let e = gen_key();
            let e = encode(e.0, Variant::UrlSafe);
            println!("You can use \"{}\"", e);
            Err(grapherr(
                "Environment Error, failed to decode encryption keys",
                "Server environment error",
            ))
        }
    }
}

fn getsecretkey() -> FieldResult<String> {
    env::var("HASH_SECRET_KEY")
        .map_err(|_| grapherr("Environment Error", "Server environment error"))
}

impl SodiumCrypto {
    pub fn new() -> Self {
        Self {}
    }

    pub fn sha256_hash_create<S>(&self, data: S) -> String
    where
        S: AsRef<[u8]>,
    {
        let data = data.as_ref();
        use sodiumoxide::crypto::hash::sha256::hash;

        let output = hash(data);
        encode(output.0, Variant::UrlSafe)
    }

    pub fn sha256_hash_verify<S>(&self, hash: &str, data: S) -> bool
    where
        S: AsRef<[u8]>,
    {
        let data = self.sha256_hash_create(data);
        &data == hash
    }

    pub fn argon2_hash_create(&self, passwd: &str) -> FieldResult<String> {
        let secret_key = getsecretkey()?;

        let passwd = format!("{passwd}::{secret_key}");
        let passwd = passwd.as_bytes();
        let hash = graphres(
            pwhash(passwd, OPSLIMIT_INTERACTIVE, MEMLIMIT_INTERACTIVE),
            "Failed to create Hash",
            "Server Cryptographic error",
        );

        let hash = hash?;
        Ok(encode(hash.as_ref(), Variant::UrlSafe))
    }

    pub fn argon2_hash_verify(&self, hash: &str, passwd: &str) -> FieldResult<bool> {
        let secret_key = getsecretkey()?;

        let hash = graphres(
            decode(hash, Variant::UrlSafe),
            "Failed to decode hash",
            "Server base64 decoding error",
        )?;

        let hash = match HashedPassword::from_slice(&hash) {
            Some(e) => e,
            None => return Ok(false),
        };

        Ok(pwhash_verify(
            &hash,
            format!("{passwd}::{secret_key}").as_bytes(),
        ))
    }

    pub fn box_create(&self, data: String) -> FieldResult<String> {
        let cryptokey = getkey()?;
        let data = data.as_bytes();
        let nonce = secretbox::gen_nonce();

        let raw_box = secretbox::seal(data, &nonce, &cryptokey);
        let raw_nonce = nonce.0;

        let sbox = encode(raw_box, Variant::UrlSafe);
        let snonce = encode(raw_nonce, Variant::UrlSafe);

        Ok(format!("{}:{}", sbox, snonce))
    }

    pub fn box_open(&self, _box: &String) -> FieldResult<String> {
        let cryptokey = getkey()?;
        let _box: Vec<&str> = _box.split(":").collect();
        let sbox = _box[0];
        let snonce = _box[1];

        let raw_box = graphres(
            decode(sbox, Variant::UrlSafe),
            "Failed to decode box",
            "Server base64 decoding error",
        )?;

        let raw_nonce = graphres(
            decode(snonce, Variant::UrlSafe),
            "Failed to decode Nounce",
            "Server base64 decoding error",
        )?;

        let nonce = match Nonce::from_slice(&raw_nonce) {
            Some(e) => e,
            None => {
                return Err(grapherr(
                    "Failed to decode Nounce",
                    "Server Crypto: Failed to decode Nonce",
                ))
            }
        };

        let contents = match secretbox::open(&raw_box, &nonce, &cryptokey) {
            Ok(e) => e,
            Err(_) => {
                return Err(grapherr(
                    "Failed to open box",
                    "Server Crypto: Failed to open",
                ))
            }
        };

        let string = match String::from_utf8(contents) {
            Ok(e) => e,
            Err(_) => {
                return Err(grapherr(
                    "Failed to transform decoded box into a string",
                    "Server Crypto: Failed to get contents of Box",
                ))
            }
        };

        Ok(string)
    }

    pub fn sign_verify<T>(&self, signature: &str, publickey: &str) -> FieldResult<T>
    where
        T: DeserializeOwned,
    {
        let bytes = graphres(
            base64::decode(signature, Variant::UrlSafeNoPadding),
            "Failed to decode signature",
            "Base64 Decoding",
        )?;

        let public_key = graphres(
            base64::decode(publickey, Variant::UrlSafeNoPadding),
            "Failed to decode user public key",
            "Base64 Decoding",
        )?;

        let pubkey = graphopt(
            sign::PublicKey::from_slice(&public_key),
            "Failed to get user public key",
            "No Public key",
        )?;

        let appsign = graphres(
            sign::verify(&bytes, &pubkey),
            "Failed to get signature",
            "cryptography errors",
        )?;

        let res = graphres(
            serde_json::from_slice::<T>(&appsign),
            "Faield to parse Json",
            "serialization error",
        )?;
        Ok(res)
    }
}
