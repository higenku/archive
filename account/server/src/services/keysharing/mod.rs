use actix::{Actor, StreamHandler, clock::Instant, ActorContext};
use actix_web::{web, Error, HttpRequest, HttpResponse};
use actix_web_actors::ws::{self, WebsocketContext};
use uuid::Uuid;


pub struct WsChatSession {
    pub id: String,
    pub room: Option<String>,
    // pub addr: Addr<ChatServer>,
    pub hb: Instant,
    pub user: String,
}

impl Actor for WsChatSession {
    type Context = WebsocketContext<Self>;

    fn started(&mut self, _ctx: &mut Self::Context) {
        // self.hb(ctx);
        // let addr = ctx.address();
        // self.addr
        //     .send(Connect {
        //         id: self.id.clone(),
        //         addr: addr.recipient(),
        //     })
        //     .into_actor(self)
        //     .then(|res, _act, ctx| {
        //         if let Err(err) = res {
        //             ctx.text(WsMessage::err(err.to_string()));
        //             ctx.stop();
        //         }
        //         fut::ready(())
        //     })
        //     .wait(ctx);
    }

    fn stopping(&mut self, _: &mut Self::Context) -> actix::Running {
        // notify chat server
        // self.addr.do_send(Disconnect {
        //     session: self.id.clone(),
        // });
        actix::Running::Stop
    }
}

impl WsChatSession {
    pub fn new() -> Self {
        WsChatSession {
            id: Uuid::new_v4().to_string(),
            room: None,
            hb: Instant::now(),
            user: format!("Hi"),
            // addr,
        }
    }
}


/// Handler for ws::Message message
impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for WsChatSession {
    fn handle(&mut self, msg: Result<ws::Message, ws::ProtocolError>, ctx: &mut Self::Context) {
        println!("Received a message");
        match msg {
            Ok(ws::Message::Ping(msg)) => ctx.pong(&msg),
            Ok(ws::Message::Text(text)) => {
                println!("received {}", text);
                ctx.text(text)

                
            },
            Ok(ws::Message::Binary(bin)) => ctx.binary(bin),
            _ => (),
        }
    }

    fn started(&mut self, _ctx: &mut Self::Context) {}

    fn finished(&mut self, ctx: &mut Self::Context) {
        ctx.stop()
    }
}

pub async fn route(req: HttpRequest, stream: web::Payload) -> Result<HttpResponse, Error> {
    println!("Connection received");

    let resp = ws::start(WsChatSession::new(), &req, stream);
    println!("{:?}", resp);
    resp
}
