use serde::{Serialize, Deserialize};
use serde_json::{Value};

#[derive(Serialize, Deserialize)]
struct CaptchaResponse {
    pub response: String,
    pub secret: String   
}
pub struct Captcha {
    privkey: String,
    client: surf::Client
}

impl Captcha {
    pub fn new(privkey: String) -> Self {
        Self { 
            privkey, 
            client: surf::Client::new()
        }
    }

    pub async fn verify_res(&self, captcha: &str) -> bool {
        let mut req = surf::post("https://challenges.cloudflare.com/turnstile/v0/siteverify").build();
        req.body_form(&CaptchaResponse {
            response: captcha.to_string(),
            secret: self.privkey.clone()
        }).unwrap();

        let mut res = match self.client.send(req).await {
            Ok(e) => e,
            Err(_) => return false,
        };

        let body = match res.body_json::<Value>().await {
            Ok(e) => e,
            Err(_) => return false,
        };

        if !body["success"].as_bool().unwrap_or(false) {
            log::debug!("Failed to solve captcha");
            return false;
        }

        true
    }
}