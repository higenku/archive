use jsonwebtoken::{Header, Algorithm, Validation, TokenData, decode, DecodingKey, errors::ErrorKind};
use juniper::{FieldError};
use serde::de::DeserializeOwned;

use crate::utils::error::grapherr;

const ISSUER   : &str = "Higenku Token Authority";
const AUDIENCE : &str = "*.higenku.org";

#[derive(Debug, Clone)]
pub struct Jwt {
   key: Vec<u8>,
}

impl Jwt {
    pub fn new() -> Self {
        Self {
            key: std::env::var("JWT_SECRET_KEY").expect("JWT_SECRET_KEY Not found or not defined").into_bytes()
        }
    }

    pub fn encode(&self, payload: &mut serde_json::Value, alg: Option<Algorithm>) -> Result<String, FieldError> {

        let header;

        if let Some(alg) = alg {
            header = Header {
                typ: Some(format!("JWT")),
                alg,
                ..Default::default()
            };   
        }
        else {
            header = Header {
                typ: Some(format!("JWT")),
                alg: Algorithm::HS256,
                ..Default::default()
            };   
        }

        // validation configuration
        payload["aud"] = serde_json::Value::from(AUDIENCE);
        payload["iss"] = serde_json::Value::from(ISSUER);
    
        jsonwebtoken::encode(
            &header,
            payload,
            &jsonwebtoken::EncodingKey::from_secret(&self.key.as_slice()),
        ).map_err(|e|{
            log::error!("Creation of token error {e:?}");
            grapherr(
                "Error Creating Token",
                "Server failed to create token"
            )
        })
    }

    pub fn decode<T: DeserializeOwned>(&self, token: &str) -> Result<TokenData<T>, FieldError> {
        let mut validator = Validation::new(jsonwebtoken::Algorithm::HS256);
        validator.set_audience(&[AUDIENCE.to_string()]);
        validator.set_issuer(&[ISSUER.to_string()]);

        decode::<T>(&token, &DecodingKey::from_secret(&self.key), &validator).map_err(|e| {
            match e.kind() {
                ErrorKind::InvalidSignature | ErrorKind::ExpiredSignature => {
                    grapherr(
                        "Auth Failed, you were logged out or your tokens expired",
                        "Invalid or Expired Signature"
                    )       
                }
                ErrorKind::InvalidToken  => {
                    grapherr(
                        "Invalid Token",
                        "Invalidation of Token"
                    )
                }
                _ => {
                    log::debug!("Failed to decode token {e:#?}");
                    grapherr(
                        "Failed to decode token",
                        "JWT Error"
                    )
                }
            }

            
        })
    }
}