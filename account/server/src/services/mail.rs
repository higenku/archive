use std::sync::Arc;
use sodiumoxide::base64::{Variant, self};
use tokio::runtime::{Runtime, Builder};
use lettre::{SmtpTransport, Message, Transport, message::{SinglePart}};

pub struct Mailer {
   client: Arc<SmtpTransport>,
   rt: Runtime,
}

impl Mailer {
    pub fn new() -> Self {
        let smtp_email = std::env::var("SMTP_EMAIL").expect("SMTP_EMAIL Configuration variable not set");
        let smtp_host = std::env::var("SMTP_HOST").expect("SMTP_HOST Configuration variable not set");
        let smtp_pass = std::env::var("SMTP_PASS").expect("SMTP_PASS Configuration variable not set");

        let creds =
            lettre::transport::smtp::authentication::Credentials::new(smtp_email, smtp_pass);

        let client = SmtpTransport::relay(smtp_host.as_str())
            .expect("Couldn't create smtp relay")
            .credentials(creds)
            .build();

        Self { 
            client: Arc::new(client),
            rt: Builder::new_current_thread().build().expect("Couldn't build mailer runtime"), 
        }
    }


    pub fn send(&self, msg: Message) {
        let client = Arc::clone(&self.client);
        self.rt.spawn_blocking(move || { 
            match client.send(&msg) {
                Err(e) => { log::error!("Error while sending email {e:?}"); },
                _ => {}
            };
        });   
    }

    pub fn send_new_account(&self, host: &str, email: &str, token: &str, ip: &str, geoinfo: &str, ) {
        let mail = lettre::Message::builder()
            .from("Higenku Communication System <noreply@higenku.org>".parse().unwrap())
            .to(format!("Name <{}>", email).parse().unwrap())
            .subject("New Account")
            .singlepart(SinglePart::builder()
                // .header(header::ContentType::TEXT_HTML)
                // .header(header::ContentTransferEncoding::QuotedPrintable)
                .body(crate::templates::new_account(email,  &format!("https://{host}/auth/register/{token}"), ip, geoinfo))
            ).unwrap();

        self.send(mail);
    }

    #[allow(dead_code)]
    pub fn send_change_email(&self, token: &str, username: &str, email: &str) {
        let code = base64::encode(token, Variant::UrlSafeNoPadding);

        let mail = lettre::Message::builder()
            .from("Higenku Communication System <noreply@higenku.org>".parse().unwrap())
            .to(format!("Name <{email}>").parse().unwrap())
            .subject("Confirm new Email Address")
            .singlepart(SinglePart::builder()
                .body(crate::templates::email_change(username, email, &code))
            ).unwrap();
        self.send(mail);
    }

    #[allow(dead_code)]
    pub fn send_email_changed(&self, username: &str, email: &str) {
        let mail = lettre::Message::builder()
            .from("Higenku Communication System <noreply@higenku.org>".parse().unwrap())
            .to(format!("Name <{email}>").parse().unwrap())
            .subject("New change email request")
            .singlepart(SinglePart::builder()
                .body(crate::templates::email_changed(username, email))
            ).unwrap();

        self.send(mail);
    }


    pub fn send_passwordless_login(&self, email: &str, host: &str, token: &str, geoinfo: &str, ip: &str) {
        
        let link = format!("https://{host}/auth/login/{token}");
        let mail = lettre::Message::builder()
            .from("Higenku Communication System <noreply@higenku.org>".parse().unwrap())
            .to(format!("Name <{email}>").parse().unwrap())
            .subject("New Login Request")
            .singlepart(SinglePart::builder()
                .body(crate::templates::passwordless(geoinfo, ip, &link))
            ).unwrap();

        self.send(mail);
    }

    pub fn send_new_session(&self, email: &str, geoinfo: &str, ip: &str) {

        let mail = lettre::Message::builder()
            .from("Higenku Communication System <noreply@higenku.org>".parse().unwrap())
            .to(format!("Name <{email}>").parse().unwrap())
            .subject("New Login Session")
            .singlepart(SinglePart::builder()
                // .header(header::ContentType::TEXT_HTML)
                // .header(header::ContentTransferEncoding::QuotedPrintable)
                .body(crate::templates::new_session(geoinfo, ip))
            ).unwrap();

        self.send(mail);
    }

    pub fn send_session_renewed(&self, session_id: &str, email: &str, geoinfo: &str, ip: &str) {

        let mail = lettre::Message::builder()
            .from("Higenku Communication System <noreply@higenku.org>".parse().unwrap())
            .to(format!("Name <{email}>").parse().unwrap())
            .subject("Session Renewed")
            .singlepart(SinglePart::builder()
                // .header(header::ContentType::TEXT_HTML)
                // .header(header::ContentTransferEncoding::QuotedPrintable)
                .body(crate::templates::session_renewed(session_id, ip, geoinfo))
            ).unwrap();

        self.send(mail);
    }
}
