use juniper::{FieldResult, GraphQLObject};

use crate::{
    graphql::Ctx,
    models::{App, AppPerm, User},
};

#[derive(Debug, GraphQLObject)]
pub struct AppResponse {
    pub _id: String,
    pub name: String,
    pub logo: String,
    pub redirect_uri: String,
    pub perms: Vec<AppPerm>,
}

pub async fn apps(ctx: &Ctx) -> FieldResult<Vec<AppResponse>> {
    let usersdb = User::new_users_col(&ctx.db);
    let (user, _) = User::from_token(&usersdb, &ctx).await?;

    let appsdb = App::new_apps_col(&ctx.db);

    let apps = App::find_by_author(&appsdb, &user.id).await?;

    let apps = apps.iter().map(|a| AppResponse {
        _id: a.id.clone(),
        name: a.name.clone(),
        logo: a.logo.clone(),
        perms: a.perms.clone(),
        redirect_uri: a.auth.redirect_uri.clone(),
    }).collect::<Vec<AppResponse>>();

    Ok(apps)
}

pub async fn app(ctx: &Ctx, id: String) -> FieldResult<AppResponse> {
    let db = App::new_apps_col(&ctx.db);
    let app = App::find_by_id(&db, &id).await?;

    Ok(AppResponse {
        _id: app.id,
        name: app.name,
        logo: app.logo,
        perms: app.perms,
        redirect_uri: app.auth.redirect_uri,
    })

}