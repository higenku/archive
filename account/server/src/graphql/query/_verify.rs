use chrono::{Utc, Duration, NaiveDateTime, DateTime};
use juniper::{GraphQLObject, FieldResult};

use crate::{graphql::{Ctx}, models::{User, AuthToken}};

#[derive(Debug, GraphQLObject)]
pub struct UserResponse {
    pub _id: String,
    pub name: String,
    pub username: String,
    pub email: String,
    pub avatar: String,
    pub keycheck: String,
    pub access_token: Option<String>,
}

pub async fn verify(
    ctx: &Ctx,     
) -> FieldResult<UserResponse> {
    let db =  User::new_users_col(&ctx.db);
    let (user, token) = User::from_token(&db, &ctx).await?;

    
    let allowed_newtoken = 
        Utc::now()
            .checked_add_signed(Duration::days(3)).unwrap();
    let exp = DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(token.claims.exp, 0), Utc);

    let mut access_token = None;

    if allowed_newtoken.timestamp() > exp.timestamp() {
        let t_id = token.claims._id;
        let jwt = AuthToken::new(
            &t_id,
            &user.id,
            &user.username,
            &user.avatar,

            Utc::now()
                .checked_add_signed(chrono::Duration::days(15))
                .expect("invalid timestamp")
                .timestamp(),
            Utc::now().timestamp()
        );
        let jwt = ctx.jwt.encode(&mut jwt.to_value().unwrap(), None)?;

        access_token = Some(jwt);

        let info = &ctx.req_info;
        ctx.mailer.send_session_renewed(&t_id, &user.email, &format!("{} {} {}", &info.city, &info.country, &info.continent), &info.ip);
    } 

    Ok(UserResponse {
        _id: user.id.clone(),
        name: user.name.clone(),
        username: user.username.clone(),
        email: user.email.clone(),
        avatar: user.avatar.clone(),
        keycheck: user.crypto.keycheck.clone(),
        access_token,
    })
}