use juniper::{FieldResult, GraphQLInputObject, GraphQLObject};
use serde::Deserialize;

use crate::{models::User, graphql::Ctx};

#[derive(Debug, Deserialize, GraphQLInputObject)]
pub struct KeycheckRequest {
    token: String,
    keycheck: String,
}

#[derive(Debug, Deserialize, GraphQLObject)]
pub struct KeyResponse {
    encrypted_store: String,
    keycheck: String,
}


pub async fn key(
    ctx: &Ctx,
) ->  FieldResult<KeyResponse> {
    let db = User::new_users_col(&ctx.db);
    let (user, _) = User::from_token(&db, &ctx).await?;
    let crypto = user.crypto.clone();

    Ok(KeyResponse {
        encrypted_store: crypto.encrypted_store.clone(),
        keycheck: crypto.keycheck.clone(),
    })
}

pub async fn verify(ctx: &Ctx, encrypted_message: String) -> FieldResult<bool> {
    
    let db = User::new_users_col(&ctx.db);
    let (user, _) = User::from_token(&db, &ctx).await?;
    
    Ok(ctx.crypto.argon2_hash_verify(&user.crypto.message, &encrypted_message)?)
}