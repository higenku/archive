use juniper::GraphQLObject;

use crate::graphql::{ApiResult, Ctx, ApiStatus};

/*
    Get information about https://gitlab.com/higenku/apps/account/-/issues/6
*/



#[derive(GraphQLObject)]
pub struct PageRespone {
    pub result: ApiResult,
    pub contents: String, 
    pub url: String,
}

pub async fn get_page(_ctx: &Ctx, _username: String) -> PageRespone {

    PageRespone {
        result: ApiResult::new(ApiStatus::Success, "Ok"),
        contents: "# Hello world\n I'm from Brazil".to_string(),
        url: "https://github.com/{username}".to_string(),
    }
}