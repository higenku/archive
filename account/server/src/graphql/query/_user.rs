use sodiumoxide::{
    base64::{encode, Variant},
    randombytes::randombytes,
};

use totp_rs::{Algorithm, TOTP};

use juniper::{FieldResult, GraphQLObject};

use crate::{
    graphql::Ctx,
    models::{meta, User, UserSession},
    utils::error::graphres,
};

#[derive(Debug, Clone, GraphQLObject)]
pub struct GSession {
    pub id: String,
    pub user_id: String,
    pub expires: String,
    pub user_agent: String,
    pub locked: bool,
}

pub async fn sessions(ctx: &Ctx) -> FieldResult<Vec<GSession>> {
    let usersdb = User::new_users_col(&ctx.db);
    let sessiondb = UserSession::new_sessions_col(&ctx.db);
    let (user, _) = User::from_token(&usersdb, &ctx).await?;

    let sess = UserSession::find_all_from_user(&sessiondb, &user.id).await?.into_iter().map(|e|  GSession {
        id: e.id,
        user_id: e.user_id,
        expires: e.expires.to_string(),
        user_agent: e.user_agent,
        locked: e.locked,
    }).collect::<Vec<GSession>>();

    Ok(sess)
}

pub async fn u2fa_configured(ctx: &Ctx) -> FieldResult<bool> {
    let db =  User::new_users_col(&ctx.db);
    let (user, _) = User::from_token(&db, &ctx).await?;
    Ok(user.meta.contains_key(meta::U2FA_SECRET))
}

#[derive(Debug, GraphQLObject)]
pub struct U2fa {
    pub uri: String,
    pub qr: String,
    pub secret: String,
}

pub async fn u2fa_new_uri(ctx: &Ctx) -> FieldResult<U2fa> {
    let db =  User::new_users_col(&ctx.db);
    let (user, _) = User::from_token(&db, &ctx).await?;
    let secret = encode(randombytes(64), Variant::UrlSafe);

    let totp = graphres(
        TOTP::new(
            Algorithm::SHA1,
            8,
            1,
            30,
            &secret,
            Some("Higenku Account".to_string()),
            user.email,
        ),
        "Error creating totp system",
        "Error creating totp system",
    )?;

    let url = totp.get_url();
    let qr = graphres(
        totp.get_qr(),
        "Failed to generate qr code",
        "Error generating qr code",
    )?;

    Ok(U2fa {
        uri: url,
        qr: qr,
        secret,
    })
}

#[derive(GraphQLObject)]
pub struct OAuthResponse {
    pub gitlab: bool,
    pub github: bool,
    pub google: bool,
    pub microsoft: bool,
}

pub async fn oauth2(ctx: &Ctx) -> FieldResult<OAuthResponse> {
    let db = User::new_users_col(&ctx.db);
    let (user, _) = User::from_token(&db, &ctx).await?;

    let gitlab = user.meta.get(meta::OAUTH_GITLAB).is_some();
    let github = user.meta.get(meta::OAUTH_GITHUB).is_some();
    let google = user.meta.get(meta::OAUTH_GOOGLE).is_some();
    let microsoft = user.meta.get(meta::OAUTH_MICROSOFT).is_some();

    Ok(OAuthResponse {
        gitlab,
        github,
        google,
        microsoft,
    })
}
