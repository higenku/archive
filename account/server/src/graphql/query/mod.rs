use juniper::{graphql_object, FieldResult};

use crate::{graphql::query::_user::GSession};

use self::_verify::UserResponse;

use super::context::Ctx;

mod _page;
mod _key;
mod _user;
mod _verify;
mod _apps;


pub struct Query;

#[graphql_object(Context = Ctx)]
impl Query {
    pub async fn api_version() -> String {
        env!("CARGO_PKG_VERSION").to_string()
    }

    pub async fn get_page(ctx: &Ctx, username: String) -> _page::PageRespone {
        _page::get_page(&ctx, username).await
    }
    
    // key
    pub async fn key(ctx: &Ctx) -> FieldResult<_key::KeyResponse> {
        _key::key(&ctx).await
    }

    pub async fn key_verify(ctx: &Ctx, encrypted_message: String) -> FieldResult<bool> {
        _key::verify(&ctx, encrypted_message).await
    }

    // auth
    pub async fn auth_verify(ctx: &Ctx) -> FieldResult<UserResponse> {
        _verify::verify(&ctx).await
    }

    // user
    pub async fn user_sessions(ctx: &Ctx) -> FieldResult<Vec<GSession>> {
        _user::sessions(&ctx).await
    }

    pub async fn user_u2fa_configured(ctx: &Ctx) -> FieldResult<bool> {
        _user::u2fa_configured(&ctx).await
    }
    
    pub async fn user_u2fa_new_uri(ctx: &Ctx) -> FieldResult<_user::U2fa> {
        _user::u2fa_new_uri(&ctx).await
    }

    pub async fn user_oauth2(ctx: &Ctx) -> FieldResult<_user::OAuthResponse> {
        _user::oauth2(&ctx).await
    }

    // apps

    pub async fn apps(ctx: &Ctx) -> FieldResult<Vec<_apps::AppResponse>> {
        _apps::apps(&ctx).await
    }

    pub async fn app(ctx: &Ctx, id: String) -> FieldResult<_apps::AppResponse> {
        _apps::app(&ctx, id).await
    }
}