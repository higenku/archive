use juniper::{FieldError};

use crate::{models::{User, UserSession}, graphql::{Ctx}, utils::error::grapherr};

pub async fn logout(
    ctx: &Ctx, 
) ->  Result<String, FieldError> {

    let usersdb = User::new_users_col(&ctx.db);
    let sessionsdb = UserSession::new_sessions_col(&ctx.db);

    let (_, token) = User::from_token(&usersdb, &ctx).await?;

    let sess = UserSession::find_with_id(&sessionsdb, &token.claims._id).await;

    if let Some(mut sess) = sess {

        let expiration = match chrono::Local::now()
                            .checked_add_signed(chrono::Duration::days(1)) {
                Some(e) => e,
                None => return Err(grapherr("Invalid timestamp", "server couldn't create a timestamp, it may be currupted")),
        };

        sess.expires = expiration.into();
        sess.locked = true;
        sess.update(&sessionsdb).await?;
    }
    
    Ok(format!("Bye"))
}
