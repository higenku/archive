use chrono::Utc;
use juniper::{FieldError, FieldResult, GraphQLInputObject};
use mongodb::bson::doc;
use serde::Deserialize;
use serde_json::{json, Value};
use uuid::Uuid;
use validator::Validate;

use crate::{
    graphql::Ctx,
    models::{meta, AuthToken, User, UserSession},
    utils::error::{graphopt, grapherr},
};

#[derive(Debug, Deserialize, GraphQLInputObject)]
pub struct AuthRegisterRequest {
    pub captcha: Option<String>,
    pub email: String,

    // registrarion
    pub token: Option<String>,
    pub name: Option<String>,
    pub username: Option<String>,
    pub crypto_pubkey: Option<String>,
    pub sign_pubkey: Option<String>,
    pub encrypted_store: Option<String>,
    pub encrypted_message: Option<String>,

    // Oauth2 credentials
    pub avatar: Option<String>,
    pub provider: Option<String>, // Provider::ID
}
fn validate_none_input<T>(o: Option<T>, name: &str) -> FieldResult<T> {
    graphopt(
        o,
        &format!("{} is necessary for registration", name),
        "validation error",
    )
}

pub async fn register(ctx: &Ctx, req: AuthRegisterRequest) -> Result<String, FieldError> {
    let db = User::new_users_col(&ctx.db);

    if let Some(captcha) = &req.captcha {
        // Captcha verification
        if !ctx.captcha.verify_res(captcha).await {
            return Err(grapherr(
                "Captcha verification error",
                "captcha verification failed",
            ));
        }

        // email validation
        if req.email.is_empty() || !req.email.contains("@") || !req.email.contains(".") {
            return Err(grapherr(
                "Not a Valid email",
                "input validation error",
            ));
        }

        let in_db = User::find_by_email(&db, &req.email).await?;

        if in_db.len() > 0 {
            return Err(grapherr(
                "User already exists",
                "user validation error",
            ));
        }

        // mail verification
        let token = ctx.jwt.encode(
            &mut json!({
                "email": &req.email,
                "exp": Utc::now()
                    .checked_add_signed(chrono::Duration::minutes(30))
                    .expect("invalid timestamp")
                    .timestamp(),
                "iat": Utc::now().timestamp()
            }),
            None,
        )?;

        let info = ctx.req_info.clone();
        let app_info = ctx.info.clone();
        ctx.mailer.send_new_account(
            &app_info.host,
            &req.email,
            &token,
            &info.ip,
            &format!("{} {} {}", info.city, info.country, info.continent),
        );

        return Ok(format!("Check your email"));
    } else if let Some(token) = &req.token {
        // token verification
        let token = match ctx.jwt.decode::<Value>(token) {
            Ok(e) => e,
            Err(_) => {
                return Err(grapherr(
                    "Token Error",
                    "Token creation error"
                ))
            }
        };

        let email = token.claims["email"]
            .as_str()
            .unwrap_or("")
            .to_string()
            .to_lowercase();
        let username = validate_none_input(req.username, "username")?.to_lowercase();
        let name = validate_none_input(req.name, "name")?;
        let encrypted_store = validate_none_input(req.encrypted_store, "Encrypted Private key")?;
        let message = validate_none_input(req.encrypted_message, "Encrypted Message")?;
        let crypto_pubkey = validate_none_input(req.crypto_pubkey, "Public key")?;
        let sign_pubkey = validate_none_input(req.sign_pubkey, "Public key")?;

        // 2 email validation
        let in_db = User::find_by_email(&db, &req.email).await?;
        let len = in_db.len();
        if len > 0 {
            return Err(grapherr(
                "Email already registered",
                "User validation error"
            ));
        }

        // 2 username validation
        let in_db = User::find_by_username(&db, &username).await?;
        let len = in_db.len();

        if len > 0 {
            return Err(grapherr(
                "Username already registered",
                "User validation error"
            ));
        }

        // End of DB validations
        // private key hashing
        let hashed_message = ctx.crypto.argon2_hash_create(&message)?;

        let mut user = User::new(
            &name,
            &username,
            &email,
            &hashed_message,
            &crypto_pubkey,
            &sign_pubkey,
            &encrypted_store,
        );
        if let Err(e) = user.validate() {
            return Err(grapherr(
                "User validation error",
                &format!("{e:#?}")
            ));
        }
        if let Some(provider) = &req.provider {
            let parts: Vec<&str> = provider.split("::").collect();

            if parts.len() < 2 || parts.len() > 2 {
                return Err(grapherr(
                    "User Oauth2 provider validation error",
                    "invalid parts"
                ));
            }

            let provider = parts[0].to_string();
            let id = parts[1].to_string();

            let key;
            match provider.as_str() {
                "gitlab" => key = meta::OAUTH_GITLAB,
                "github" => key = meta::OAUTH_GITHUB,
                "google" => key = meta::OAUTH_GOOGLE,
                "microsoft" => key = meta::OAUTH_MICROSOFT,
                _ => {
                    return Err(grapherr(
                        "Invalid Provider",
                        "not a valid provider for Oauth2 authentication"
                    ))
                }
            }

            let db_users = User::find(
                &db,
                doc!{
                    "meta": {
                        key: id.to_string()
                    }
                },
            )
            .await?;

            if db_users.len() > 0 {
                return Err(grapherr(
                    "User already exists",
                    "User validation error"
                ));
            }

            user.meta.insert(key.to_string(), id);
        }

        // used for session later
        let t_id = Uuid::new_v4().to_string();
        let expiration = match chrono::Local::now()
                            .checked_add_signed(chrono::Duration::days(15)) {
            Some(e) => e,
            None => return Err(grapherr("Invalid timestamp", "server couldn't create a timestamp, it may be currupted")),
        };

        let sessionsdb = UserSession::new_sessions_col(&ctx.db);
        let sess = UserSession::new(&t_id, &ctx.req_info.user_agent, &user.id, expiration, false);
        
        let prom1 = sess.insert(&sessionsdb);
        let prom2 = user.insert(&db);

        // Session management
        let jwt = AuthToken::new(
            &t_id,
            &user.id,
            &user.username,
            &user.avatar,
            Utc::now()
                .checked_add_signed(chrono::Duration::days(15))
                .expect("invalid timestamp")
                .timestamp(),
            Utc::now().timestamp(),
        );
        let jwt = ctx.jwt.encode(&mut jwt.to_value().unwrap(), None)?;

        let cookiehost = ctx.info.cookiehost.clone();

        // Wait for the db tasks
        prom2.await?;
        prom1.await?;

        ctx.sess.write().await.build_session(&cookiehost, &jwt);

        Ok(format!("{}::{}", user.crypto.keycheck, user.id))
    } else {
        return Err(grapherr(
            "Failed to decode body",
            "invalid request",
        ));
    }
}
