use crate::{
    graphql::Ctx,
    models::{meta, AuthStep, AuthToken, User, UserSession},
    utils::error::{grapherr, graphres},
};
use chrono::Utc;
use juniper::{FieldError, GraphQLInputObject};
use serde::Deserialize;
use serde_json::{json, Value};
use totp_rs::TOTP;
use uuid::Uuid;

#[derive(Debug, Deserialize, GraphQLInputObject)]
pub struct AuthLoginRequest {
    credential: String,
    captcha: Option<String>,
    token: Option<String>,
    input: Option<String>,
}

pub async fn login(ctx: &Ctx, req: AuthLoginRequest) -> Result<String, FieldError> {
    let db = User::new_users_col(&ctx.db);

    // find user
    if req.credential.len() <= 0 {
        return Err(grapherr("Credential must be provided", "Credential is 0"));
    }

    let res = if req.credential.contains("@") {
        User::find_by_email(&db, &req.credential).await
    } else {
        User::find_by_username(&db, &req.credential).await
    }?;

    if res.len() == 0 {
        return Err(grapherr("No User found", "User not found"));
    }

    let local_verification = format!(
        "{} {} {} {} {}",
        &ctx.req_info.city,
        &ctx.req_info.country,
        &ctx.req_info.continent,
        &ctx.req_info.ip,
        &ctx.req_info.user_agent
    );

    let user = res[0].clone();

    // ! first request, get info and start the login process
    if req.token.is_none() {
        let captcha = match req.captcha {
            Some(e) => e,
            None => return Err(grapherr("Invalid Captcha", "Captcha validation failed"))
        };
        
        if !ctx.captcha.verify_res(&captcha).await {
            return Err(grapherr("Invalid Captcha", "Captcha verification failed"))
        }

        let mut payload = json!({
            "credential": &req.credential,
            "step": 0 as i32,
            "t_id": Uuid::new_v4().as_simple().to_string(),
            "iat": Utc::now().timestamp(),
            "exp": Utc::now()
                .checked_add_signed(chrono::Duration::minutes(30))
                .expect("invalid timestamp")
                .timestamp(),
        });

        // get steps and generate token
        match res[0].auth_steps[0] {
            AuthStep::PasswordLess => {
                payload["step_type"] = json!("email");
                payload["location"] = json!(ctx.crypto.sha256_hash_create(&local_verification));

                let jwt = ctx.jwt.encode(&mut payload, None)?;

                let info = ctx.req_info.clone();

                #[cfg(debug_assertions)] // check if debug
                log::error!("http://localhost:3000/auth/login/{}", &jwt);
                ctx.mailer.send_passwordless_login(
                    &user.email,
                    &ctx.info.host,
                    &jwt,
                    &format!("{}, {} ({})", info.city, info.country, info.continent),
                    &info.ip,
                );
                return Ok(format!("Check your email"));
            }
            AuthStep::TfaToken => payload["step_type"] = json!("tfa"),
            AuthStep::SecurityKey => payload["step_type"] = json!("securitykey"),
        }

        let jwt = ctx.jwt.encode(&mut payload, None)?;

        return Ok(format!("NextStep:{jwt}"));
    }
    // ! Other steps
    else if let Some(token) = &req.token {
        let token = match ctx.jwt.decode::<Value>(token) {
            Ok(e) => e,
            Err(_) => {
                return Err(grapherr(
                    "Token Expired or is invalid",
                    "Token check failed",
                ))
            }
        };

        let current_step = match token.claims["step"].as_i64() {
            Some(e) => e as usize,
            None => return Err(grapherr("Token is missing step", "invalid token")),
        };

        match user.auth_steps[current_step] {
            AuthStep::PasswordLess => { 
                let hash = token.claims["location"].as_str().unwrap_or("");
                /* The step itself was sent by email, so there is no need for more auth */
                if !ctx.crypto.sha256_hash_verify(hash, &local_verification) {
                    return Err(grapherr("It looks like you aren't on the same computer you made the request", "Failed to verify location"))
                }
            
            }
            AuthStep::TfaToken => {
                if !user.meta.contains_key(meta::U2FA_SECRET) {
                    return Err(grapherr(
                        "Contact support and state that your accounts 2fa backend is not working",
                        "Unknown token error",
                    ));
                }

                if req.input.is_none() || req.input.as_ref().unwrap().len() < 4 {
                    return Err(grapherr("Must contain a totp code", "code not provided"));
                }

                let secret = &user.meta[meta::U2FA_SECRET];
                let secret = ctx.crypto.box_open(secret)?;

                let totp = graphres(
                    TOTP::new(
                        totp_rs::Algorithm::SHA1,
                        8,
                        1,
                        30,
                        &secret,
                        Some("Higenku Account".to_string()),
                        user.email.clone(),
                    ),
                    "Error creating totp system",
                    "failed to get secrets",
                )?;

                let input = req.input.as_ref().unwrap().as_str();

                let check = graphres(
                    totp.check_current(input),
                    "System Error, invalid timestamp",
                    "internal check error",
                )?;

                if !check {
                    return Err(grapherr("Invalid code", "code check error"));
                }
            }

            // Todo:
            // AuthStep::SecurityKey => todo!(),
            _ => {
                return Err(grapherr(
                    "Not Implemented",
                    "system did not impelemnt this kind of security",
                ));
            }
        }

        // ! End, session approved
        if user.auth_steps.len() == (current_step + 1) {
            
            let token_id = match token.claims["t_id"].as_str() {
                Some(e) => e.to_string(),
                None => return Err(grapherr("Invalid token", "couldn't retrieve token_id from token claims")),
            };

            let jwt = AuthToken::new(
                &token_id,
                &user.id,
                &user.username,
                &user.avatar,
                Utc::now()
                    .checked_add_signed(chrono::Duration::days(15))
                    .expect("invalid timestamp")
                    .timestamp(),
                Utc::now().timestamp(),
            );

            let jwt = ctx.jwt.encode(&mut jwt.to_value().unwrap(), None)?;


            let sessionsdb = UserSession::new_sessions_col(&ctx.db);
            let sess = UserSession::find_with_id(&sessionsdb, &token_id).await;

            if sess.is_some() {
                return Err(grapherr("You have already logged in, using this token", "Session id already exists"))
            }

            let expiration = match chrono::Local::now()
                            .checked_add_signed(chrono::Duration::days(15)) {
                Some(e) => e,
                None => return Err(grapherr("Invalid timestamp", "server couldn't create a timestamp, it may be currupted")),
            };

            let sess = UserSession::new(&token_id, &ctx.req_info.user_agent, &user.id, expiration, false);

            sess.insert(&sessionsdb).await?;

            let email = user.email.clone();

            // add session to the user
            user.update(&db).await?;

            // ? Session management
            let info = ctx.req_info.clone();

            ctx.mailer.send_new_session(
                &email,
                &format!("{} {} {}", &info.city, &info.country, &info.continent),
                &info.ip,
            );
            let cookiehost = ctx.info.cookiehost.clone();
            ctx.sess.write().await.build_session(&cookiehost, &jwt);
            return Ok(format!("You are logged in"));
        }
        // ! Continue to next step
        else {
            let next_step = current_step + 1;
            let mut payload = json!({
                "credential": &req.credential,
                "step": next_step,
                "t_id": token.claims["t_id"],
                "iat": Utc::now().timestamp(),
                "exp": Utc::now()
                    .checked_add_signed(chrono::Duration::minutes(30))
                    .expect("invalid timestamp")
                    .timestamp(),
            });

            // get steps and generate token
            match user.auth_steps[next_step] {
                AuthStep::PasswordLess => {
                    payload["step_type"] = json!("email");

                    let jwt = ctx.jwt.encode(&mut payload, None)?;
                    let info = ctx.req_info.clone();

                    ctx.mailer.send_passwordless_login(
                        &user.email,
                        &ctx.info.host,
                        &jwt,
                        &format!("{}, {} ({})", info.city, info.country, info.continent),
                        &info.ip,
                    );
                    return Ok(format!("Check your email"));
                }
                AuthStep::TfaToken => payload["step_type"] = json!("tfa"),
                AuthStep::SecurityKey => payload["step_type"] = json!("securitykey"),
            }

            let jwt = ctx.jwt.encode(&mut payload, None)?;
            return Ok(jwt);
        }
    } else {
        log::error!("It was bad");
        return Err(grapherr(
            "System Error",
            "Unreachable code reached, please report it in https://higenku.org/support ",
        ));
    }
}
