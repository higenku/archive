use juniper::{FieldResult};
use serde::Deserialize;
use validator::Validate;

use crate::{
    graphql::Ctx,
    models::{App, AppPerm, User}, utils::error::grapherr,
};

#[derive(Debug, Deserialize)]
pub struct CreateAppSignature {
    /// Should be 1.0.0
    pub _v: String,
    pub name: String,
    pub logo: Option<String>,
    pub perms: Vec<AppPerm>,
    pub redirect_uri: String,
}

pub async fn create(ctx: &Ctx, signature: String) -> FieldResult<String> {
    let usersdb = User::new_users_col(&ctx.db);
    let appsdb = App::new_apps_col(&ctx.db);

    let (user, _) = User::from_token(&usersdb, &ctx).await?;
    let appsign = ctx.crypto.sign_verify::<CreateAppSignature>(&signature, &user.crypto.sign_publickey)?;
    let (app, client_secret) = App::new(
        &ctx.crypto,
        appsign.name,
        user.id.clone(),
        appsign.logo.unwrap_or("".to_string()),
        appsign.perms,
        appsign.redirect_uri,
    )?;

    if let Err(e) = app.validate() {
        return Err(grapherr("validation failed", &e.to_string()));
    }

    app.insert(&appsdb).await?;
    Ok(client_secret)
}
