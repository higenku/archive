use juniper::{FieldResult};
use serde::{Deserialize, Serialize};
use validator::Validate;

use crate::{
    graphql::Ctx,
    models::{App, AppPerm, User},
    utils::error::grapherr,
};

// use super::AppPermRequest;
#[derive(Debug, Deserialize)]
pub struct UpdateAppSignature {
    /// Should be 1.0.0
    pub _v: String,
    pub id: String,
    pub name: String,
    pub redirect_uri: String,
    pub logo: Option<String>,
    pub perms: Vec<AppPerm>,
}

pub async fn update(ctx: &Ctx, signature: String) -> FieldResult<bool> {
    let usersdb = User::new_users_col(&ctx.db);
    let (user, _) = User::from_token(&usersdb, &ctx).await?;

    let appsdb = App::new_apps_col(&ctx.db);

    let appsign = ctx
        .crypto
        .sign_verify::<UpdateAppSignature>(&signature, &user.crypto.sign_publickey)?;

    if appsign._v != "1.0.0" {
        return Err(grapherr(
            "Icompatible SIgnature version",
            "signature version",
        ));
    }

    let mut app = App::find_by_id(&appsdb, &appsign.id).await?;

    if app.author_id != user.id {
        return Err(grapherr(
            "Userid do not match",
            "userid comparison",
        ));
    }

    app.logo = appsign.logo.unwrap_or("".to_string());
    app.name = appsign.name;
    app.perms = appsign.perms;
    app.auth.redirect_uri = appsign.redirect_uri;

    if let Err(e) = app.validate() {
        return Err(grapherr("validation failed", &e.to_string()));
    }

    app.update(&appsdb).await?;

    Ok(true)
}

#[derive(Debug, Serialize, Deserialize)]
pub struct RegenerateClientSecret {
    pub id: String,
}

pub async fn regen_client_secret(ctx: &Ctx, signature: String) -> FieldResult<String> {
    let usersdb = User::new_users_col(&ctx.db);
    let (user, _) = User::from_token(&usersdb, &ctx).await?;

    let appsdb = App::new_apps_col(&ctx.db);
    let appsign = ctx
        .crypto
        .sign_verify::<RegenerateClientSecret>(&signature, &user.crypto.sign_publickey)?;
    let mut app = App::find_by_id(&appsdb, &appsign.id).await?;

    if app.author_id != user.id {
        return Err(grapherr("User does not have access to this app", "app.author_id != user.id"));
    }

    let client_secret = App::gen_client_secret();

    app.auth.client_secret = ctx.crypto.argon2_hash_create(&client_secret)?;    
    app.update(&appsdb).await?;

    Ok(client_secret)
}