use juniper::{FieldResult};
use serde::Deserialize;

use crate::{graphql::Ctx, models::{App, User}, utils::error::grapherr};

#[derive(Debug, Deserialize)]
pub struct DeleteAppSignature {
    pub _v: String,
    pub id: String,
    pub captcha: String,
}

pub async fn delelte(ctx: &Ctx, signature: String) -> FieldResult<bool> {
    let usersdb = User::new_users_col(&ctx.db);
    let appsdb = App::new_apps_col(&ctx.db);

    let (user, _) = User::from_token(&usersdb, &ctx).await?;
    let sign = ctx.crypto.sign_verify::<DeleteAppSignature>(&signature, &user.crypto.sign_publickey)?;
    let app = App::find_by_id(&appsdb, &sign.id).await?;
    
    if !ctx.captcha.verify_res(&sign.captcha).await {
        return Err(grapherr("Invalid Captcha Response, try again later", "Server failed to verify captcha"));
    }

    if app.author_id != user.id {
        return Err(grapherr("userid comparison failed", "You are not authorized to delete this app"));
    }

    app.delelte(&appsdb).await?;
    Ok(true)
}