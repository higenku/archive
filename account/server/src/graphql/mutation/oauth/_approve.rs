// https://www.rfc-editor.org/rfc/rfc6749#section-4.1.2
// https://docs.gitlab.com/ee/api/oauth2.html#authorization-code-flow
use juniper::FieldResult;
use serde::Deserialize;

use crate::{graphql::Ctx, models::{User, App, OAuthCodes}, utils::error::grapherr};

#[derive(Debug, Deserialize)]
pub struct OauthApprove {
    pub captcha: String,
    pub scope: String,
    pub redirect_uri: String,
    pub client_id: String,
    pub response_type: String,
}

/// Approves the app and returns the authorization code
pub async fn approve(ctx: &Ctx, signature: String) -> FieldResult<String> {
    let appsdb = App::new_apps_col(&ctx.db);
    let usersdb = User::new_users_col(&ctx.db);
    let (user, _) = User::from_token(&usersdb, &ctx).await?;

    let sign = ctx
        .crypto
        .sign_verify::<OauthApprove>(&signature, &user.crypto.sign_publickey)?;

    if sign.response_type != "code" {
        return Err(grapherr("unsupported_response_type", "response_type must be 'code'"));
    }

    if !ctx.captcha.verify_res(&sign.captcha).await {
        return Err(grapherr("Invalid captcha", "Failed to verify captcha"));
    }

    let app = App::find_by_id(&appsdb, &sign.client_id).await?;

    if !app.perms.iter().any(|perm| perm.name == "oauth") {
        return Err(grapherr("unauthorized_client", "The client does not have oauth permissions"));
    }

    if app.auth.redirect_uri != sign.redirect_uri {
        return Err(grapherr("invalid_request", "invalid redirect_uri"));
    }

    let mut oauthcache = OAuthCodes::new_redis(&ctx.redis).await?;
    let code = oauthcache.set_oauthcode(&sign.client_id, &user.id).await?;

    return Ok(code);
}
