use juniper::FieldResult;
use super::Ctx;

mod auth;
mod user;
mod apps;
mod oauth;

pub struct Mutation;

#[juniper::graphql_object(Context = Ctx)]
impl Mutation {
    // Auth Mod
    pub async fn auth_register(ctx: &mut Ctx, req: auth::AuthRegisterRequest) -> FieldResult<String> {
        auth::register(&ctx, req).await
    }

    pub async fn auth_login(ctx: &mut Ctx, req: auth::AuthLoginRequest) -> FieldResult<String> {
        auth::login(&ctx, req).await
    }

    pub async fn auth_logout(ctx: &mut Ctx) -> FieldResult<String> {
        auth::logout(&ctx).await
    }

    // user
    pub async fn user_update(ctx: &mut Ctx, signature: String) -> FieldResult<bool> {
        user::update(&ctx, signature).await
    }

    pub async fn user_session_delete(ctx: &mut Ctx, signature: String) -> FieldResult<bool> {
        user::session_delete(&ctx, signature).await
    }

    pub async fn user_oauth2_remove(ctx: &mut Ctx, signature: String) -> FieldResult<bool> {
        user::oauth_remove(&ctx, signature).await
    }

    pub async fn user_u2fa_configure(ctx: &Ctx, signature: String) -> FieldResult<bool> {
        user::u2fa_configure(&ctx, signature).await
    }

    pub async fn user_u2fa_remove(ctx: &Ctx, signature: String) -> FieldResult<bool> {
        user::u2fa_remove(&ctx, signature).await
    }

    // Apps
    pub async fn apps_create(ctx: &Ctx, signature: String) -> FieldResult<String> {
        apps::create(&ctx, signature).await
    }

    pub async fn apps_update(ctx: &Ctx, signature: String) -> FieldResult<bool> {
        apps::update(&ctx, signature).await
    }

    pub async fn apps_delete(ctx: &Ctx, signature: String) -> FieldResult<bool> {
        apps::delelte(&ctx, signature).await
    }

    pub async fn apps_regen_client_secret(ctx: &Ctx, signature: String) -> FieldResult<String> {
        apps::regen_client_secret(&ctx, signature).await
    }


    // Oauth
    pub async fn oauth_approve(ctx: &Ctx, signature: String) -> FieldResult<String> {
        oauth::approve(&ctx, signature).await
    }
}

