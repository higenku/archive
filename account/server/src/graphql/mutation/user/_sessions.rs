use juniper::FieldResult;
use serde::Deserialize;

use crate::{graphql::Ctx, models::{User, UserSession}, utils::error::grapherr};

#[derive(Debug, Deserialize)]
pub struct RemoveSessionSignature {
    pub ids: Vec<String>,
}

pub async fn session_delete(ctx: &Ctx, signature: String) -> FieldResult<bool> {
    let usersdb = User::new_users_col(&ctx.db);
    let sessionsdb = UserSession::new_sessions_col(&ctx.db);
    let (user, _) = User::from_token(&usersdb, &ctx).await?;

    let sign = ctx
        .crypto
        .sign_verify::<RemoveSessionSignature>(&signature, &user.crypto.sign_publickey)?;

    let expiration = match chrono::Local::now().checked_add_signed(chrono::Duration::days(1)) {
        Some(e) => e,
        None => return Err(grapherr("Invalid timestamp", "server couldn't create a timestamp, it may be currupted")),
    };
    
    for sess_id in sign.ids {
        // transaction
        let sess = UserSession::find_with_id(&sessionsdb, &sess_id).await;
        
        if let Some(mut sess) = sess {
            sess.locked = true;
            sess.expires = expiration.clone().into();
            sess.update(&sessionsdb).await?;
        }
    }

    Ok(true)
}
