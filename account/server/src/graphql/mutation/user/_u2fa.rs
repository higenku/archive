use crate::{
    graphql::Ctx,
    models::{meta, AuthStep, User},
    utils::error::{grapherr, graphres},
};
use chrono::Utc;
use juniper::FieldResult;
use serde::Deserialize;
use totp_rs::{Algorithm, TOTP};

#[derive(Debug, Deserialize)]

pub struct Configure2FASignature {
    pub code: String,
    pub secret: String,
    pub captcha: String,
}

pub async fn u2fa_configure(ctx: &Ctx, signature: String) -> FieldResult<bool> {
    let db = User::new_users_col(&ctx.db);
    let (mut user, _) = User::from_token(&db, &ctx).await?;

    let sign = ctx
        .crypto
        .sign_verify::<Configure2FASignature>(&signature, &user.crypto.sign_publickey)?;

    if !ctx.captcha.verify_res(&sign.captcha).await {
        return Err(grapherr("Invalid captcha", "Failed to verify captcha"));
    }

    let totp = graphres(
        TOTP::new(
            Algorithm::SHA1,
            8,
            1,
            30,
            &sign.secret,
            Some("Higenku Account".to_string()),
            user.email.clone(),
        ),
        "Error creating totp system",
        "internal server error",
    )?;

    if !totp.check(&sign.code, Utc::now().timestamp() as u64) {
        return Err(grapherr("Failed to verify code", "authorization error"));
    }

    if user.meta.contains_key(meta::U2FA_SECRET) {
        user.meta.remove(meta::U2FA_SECRET);
    }

    let secret = ctx.crypto.box_create(sign.secret)?;

    user.meta.insert(meta::U2FA_SECRET.to_string(), secret);

    if !user.auth_steps.contains(&AuthStep::TfaToken) {
        user.auth_steps.push(AuthStep::TfaToken);
    }

    user.update(&db).await?;
    Ok(true)
}

#[derive(Debug, Deserialize)]
pub struct Remove2FASignature {
    pub remove: bool,
    pub code: String,
    pub captcha: String,
}


pub async fn u2fa_remove(ctx: &Ctx, signature: String) -> FieldResult<bool> {
    let db = User::new_users_col(&ctx.db);
    let (mut user, _) = User::from_token(&db, &ctx).await?;

    let sign = ctx
        .crypto
        .sign_verify::<Remove2FASignature>(&signature, &user.crypto.sign_publickey)?;

    if !ctx.captcha.verify_res(&sign.captcha).await {
        return Err(grapherr("Invalid captcha", "Failed to verify captcha"));
    }

    if !sign.remove {
        return Err(grapherr("This version of the program is probably outdated. Please hardrefresh the page", "Invalid signature"))
    }
    if !user.meta.contains_key(meta::U2FA_SECRET) {
        return Err(grapherr("You do not have 2 factor authentication enabled", "Invalid user"))    
    }

    if user.auth_steps.contains(&AuthStep::TfaToken) {
        let pos = user.auth_steps.iter().position(|e| e == &AuthStep::TfaToken);
        if pos.is_some() {
            user.auth_steps.remove(pos.unwrap());
        }
    }

    let secret = ctx.crypto.box_open(&user.meta[meta::U2FA_SECRET])?;

    let totp = graphres(
        TOTP::new(
            Algorithm::SHA1,
            8,
            1,
            30,
            &secret,
            Some("Higenku Account".to_string()),
            user.email.clone(),
        ),
        "Error creating totp system",
        "internal server error",
    )?;

    if !totp.check(&sign.code, Utc::now().timestamp() as u64) {
        return Err(grapherr("Failed to verify code", "authorization error"));
    }
    
    user.meta.remove(meta::U2FA_SECRET);

    user.update(&db).await?;
    Ok(true)
}
