use juniper::{FieldResult, GraphQLInputObject};

use crate::{graphql::Ctx, models::User, utils::error::grapherr};
use serde::Deserialize;

#[derive(Debug, Deserialize, GraphQLInputObject)]
pub struct UpdateUserSignature {
    pub username: String,
    pub avatar: String,
    pub name: String,
}

pub async fn update(ctx: &Ctx, signature: String) -> FieldResult<bool> {
    let db = User::new_users_col(&ctx.db);
    let (mut user, _) = User::from_token(&db, &ctx).await?;

    let sign = ctx
        .crypto
        .sign_verify::<UpdateUserSignature>(&signature, &user.crypto.sign_publickey)?;

    if user.avatar == sign.avatar && user.username == sign.username && user.name == sign.name {
        return Ok(false);
    }

    if user.username != sign.username
        && User::find_by_username(&db, &sign.username).await?.len() > 0
    {
        return Err(grapherr(
            "Username already exists",
            "User validation failed",
        ));
    }

    user.avatar = sign.avatar;
    user.username = sign.username;
    user.name = sign.name;

    user.update(&db).await?;
    Ok(true)
}
