use juniper::{FieldResult};
use serde::Deserialize;

use crate::{models::{User, meta}, graphql::Ctx, utils::error::grapherr};

#[derive(Debug, Deserialize)]
pub struct RemoveOAuth2Signature {
    pub provider: String,
}

pub async fn oauth_remove(ctx: &Ctx, signature: String) -> FieldResult<bool> {
    let db = User::new_users_col(&ctx.db);
    let (mut user, _) = User::from_token(&db, &ctx).await?;

    let sign = ctx.crypto.sign_verify::<RemoveOAuth2Signature>(&signature, &user.crypto.sign_publickey)?;

    match sign.provider.as_str() {
        meta::OAUTH_GITLAB | meta::OAUTH_GITHUB | 
        meta::OAUTH_GOOGLE | meta::OAUTH_MICROSOFT  => {
            let res = user.meta.remove(&sign.provider);

            if res.is_none() {
              return Err(grapherr(
                "Provider was not added to user",
                "User validation failed"
              ));
            }

            user.update(&db).await?;

            Ok(true)
        }
        
        _ => Err(grapherr(
            "Unknown Provider",
            "User validation failed"
        ))
    }
}