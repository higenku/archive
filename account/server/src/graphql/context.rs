use std::{sync::{Arc}};
use mongodb::Database;
use tokio::sync::{RwLock};
use crate::{abstraction::{AppInfo, Session}, services::{Jwt, Mailer, Captcha, SodiumCrypto}, utils::RequestInfo};


pub struct Ctx {
    pub db: Arc<Database>,
    pub redis: Arc<redis::Client>,
    
    pub captcha: Arc<Captcha>,
    pub mailer: Arc<Mailer>,
    pub jwt: Arc<Jwt>,
    pub sess: RwLock<Session>,
    pub crypto: Arc<SodiumCrypto>,

    pub info: AppInfo,
    pub req_info: RequestInfo,
    pub refresh_token: Option<String>,
}

impl<'a> juniper::Context for Ctx {}