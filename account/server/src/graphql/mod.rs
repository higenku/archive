use std::sync::Arc;

use actix_web::{web::{self, Data}, post, HttpResponse, HttpRequest};
use mongodb::Database;
use juniper::{EmptySubscription, RootNode, GraphQLObject, GraphQLEnum};
use juniper_actix::{graphql_handler};
use tokio::sync::RwLock;

mod context;
mod query;
mod mutation;

use crate::{abstraction::{AppInfo, Session}, services::{Mailer, Jwt, Captcha, SodiumCrypto}, utils::get_req_info};

pub use self::context::{Ctx};
pub use self::query::Query;
pub use self::mutation::Mutation;


pub type Schema = RootNode<'static, Query, Mutation, EmptySubscription<Ctx>>;

#[derive(GraphQLEnum)]
pub enum ApiStatus {
    Success,
    Error,
    DatabaseError,
    ServerError
}

#[derive(GraphQLObject)]
pub struct ApiResult {
    pub status: ApiStatus,
    pub message: String,
}

impl ApiResult {
    pub fn new(status: ApiStatus, message: &str) -> Self {
        let message = message.to_string();

        Self {
            status,
            message,
        }
    }
}


#[post("/gql")]
pub async fn route<'r>(
    req: HttpRequest,
    pay: web::Payload,
    schema: Data<Schema>,

    db: Data<Database>,
    redis: Data<redis::Client>,
    mailer: Data<Mailer>,
    jwt: Data<Jwt>,
    captcha: Data<Captcha>,
    app_info: Data<AppInfo>,
    crypto: Data<SodiumCrypto>

) -> HttpResponse {

    let sess = Session::new(req.cookies().unwrap().to_vec());
    let schema = schema.into_inner();

    let db = Arc::clone(&db);
    let redis = Arc::clone(&redis);

    let mailer = Arc::clone(&mailer);
    let jwt = Arc::clone(&jwt);
    let captcha = Arc::clone(&captcha);
    let crypto = Arc::clone(&crypto);

    let app_info = &*app_info;
    let app_info = app_info.as_ref().clone();
    let geoinfo = get_req_info(req.headers());

    let refresh_token = req.headers().get(actix_web::http::header::AUTHORIZATION)
        .map(|e| e.to_str().unwrap_or_else(|_| ""))
        .map(|e| e.to_string().replace("Bearer ", ""));
        
    // gets the cookies to the request
    let ctx = Ctx {
        redis,
        db,
        
        captcha,
        mailer,
        jwt,
        sess: RwLock::new(sess),
        req_info: geoinfo,
        crypto,

        info: app_info.clone(),
        refresh_token,
    };


    match graphql_handler(&schema, &ctx, req, pay).await {
        Ok(response) => {
            ctx.sess.write().await.finalize(response)
        }

        Err(e) => {
            log::error!("Graphql Error {e:?}");
            return HttpResponse::InternalServerError().body("Server Error, Try again later")
        }
    }
}


pub fn generate_schema() -> Schema {
    Schema::new(
        Query, 
        Mutation, 
        EmptySubscription::new()
    )
}
