# Higenku Account (XAccount)  [![pipeline status](https://gitlab.com/higenku/apps/account/badges/dev/pipeline.svg)](https://gitlab.com/higenku/apps/account/-/commits/dev) 

One Account, All Higenku Features! This app is used to authenticate all users with an simplified authentication mechanism

## Plan
Instead of reimplementing authentication systems in all higenku applications, we use OAuth2.0 to Authenticate our user through this system.

# Running
## Commands
```
# For running
make

# For building
make build
```

you have to have a `.env` file like the example `.env.example`