export async function sleep(milisecs = 100) {
    await new Promise((res) => setTimeout(() => res({}), milisecs))
}

export function getCookie(name: string) {
    if (!document.cookie) {
        return "";
    }

    name = name + "=";
    const decodedCookie = decodeURIComponent(document.cookie);
    const ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
}

export function setCookie(name: string, value: string, exp = -1, domain: string = import.meta.env.VITE_DOMAIN) {
    let expires = `expires=`
    if (exp !== -1) {
        const date = new Date();
        date.setTime(exp * 1000);
        expires = `${expires}${date.toUTCString()}`;
    }
    expires = `${expires};`

    document.cookie = `${name}=${value};path=/;domain=${domain};${expires}SameSite=Lax;Secure;`;
}


export const deleteCookie = (name: string) => {
    document.cookie = `${name}=John; expires=Thu, 18 Dec 1969 12:00:00 UTC; path=/;Domain=${import.meta.env.VITE_DOMAIN};Secure;`;
}