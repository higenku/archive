
import { writable } from "svelte/store";
import { User } from "$types/user";
import { deleteCookie, getCookie, setCookie } from "./utils";
import { goto } from "$app/navigation";
import { utils } from "@higenku/theme";
import { cryptoDb, type KeyStore } from "./db";
import { GraphQLClient } from "graphql-request";
import { getSdk } from "$gql/index";
import type { ClientType } from "$lib/gql";
import defaultclient from "$lib/gql";
import { tick } from "svelte";


export enum AuthState {
    authorizing = 0,
    authorized = 1,
    notAuthorized = -1,
}
export class AuthService  {
    private authPromise: Promise<User> | null = null;
    public client: ClientType = defaultclient
    private user: User = {
        _id: 'invalid',
        user_id: "default:::::",
        name: "name",
        avatar: "avatar",
        username: "username",
        email: "email"
    };

    private token = "";
    public users: User[] = [];
    public tokens: string[] = [];
    public needsKeySync: boolean = false;
    
    
    constructor(
        public current = 0,
        private doneAuth = false,
    ){};


    private async doAuth(): Promise<User> {
        authState.set(AuthState.authorizing)
        
        const cookieCounter = document.cookie.match(/(rf-\d+)+/g)
        const count = cookieCounter?.length || 0;
    
        
        if (count == 0) {
            authState.set(AuthState.notAuthorized);
            auth.endSessions();
            return this.user;
        }

        
        this.current = parseInt(getCookie('cu') || "0");
        if (this.current >= count) {
            window.location.href = '/'
        }
        
        this.token = getCookie(`rf-${this.current}`)
        if (this.token === ``) {
            deleteCookie(`co`)
            authState.set(AuthState.notAuthorized);
            return this.user;
        }
        this.client = getSdk(new GraphQLClient(`${import.meta.env.VITE_API_URL}/gql`, {
            credentials: 'include',
            headers: { 'authorization': `Bearer ${this.token}` }
        }));
        
        try {

            const {authVerify: user} = await this.client.authVerify()

            if (typeof user.accessToken !== 'undefined' && user.accessToken !== null) {
                const token = user.accessToken;
                const co = getCookie(`co`)
                const data = JSON.parse(window.atob(token.split('.')[1]));
                
                this.token = token;

                setCookie(`rf-${this.current++}`, user.accessToken, data.exp)
                setCookie(`co`, co, data.exp)
                setCookie(`changed`, `true`)
            }

            const tokenUser = User.from_token(this.token)
            this.user = {
                _id: tokenUser._id,
                user_id: user.id,
                name: user.name,
                username: user.username,
                email: user.email,
                avatar: user.avatar,
            };

            if (!await cryptoDb.contains(user.id)) {
                this.needsKeySync = true;
                await utils.newNotification('Error', `Looks like this is the first time you login in this device. Please sync your keys`, true, 'erro', 5000);
                await goto('/auth/keysync')
                authState.set(AuthState.authorized)
                return this.user
            }

            const keys = await cryptoDb.get(user.id)

            if (typeof keys === 'undefined' || keys.keycheck !== user.keycheck) {
                await utils.newNotification('Error', `keycheck doesn't match or wasn't save! please sync again!`, true, 'erro', 10000);
                await goto('/auth/keysync')
                authState.set(AuthState.authorized)
                return this.user;
            }

            authState.set(AuthState.authorized);
        }
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        catch (e: any) {
            this.endSessions()
            const err = e.response?.errors[0] || e;

            // If authentication error detected, move to login page
            if (typeof err.extensions !== 'undefined' && typeof err.extensions.type !== 'undefined' && err.extensions.type === 'Authorization') {
                await goto('/auth/login')
                await utils.newNotification('Auth Error', `${err.message}`, true, 'erro', 5000)
                authState.set(AuthState.notAuthorized);
                return this.user;
            }

            if (err.message == "Invalid Session") {
                await utils.newNotification('Invalid session', `It looks like your session is no longer valid, try login in again`, true, 'erro', 5000)
                authState.set(AuthState.notAuthorized);
                goto('/auth/logout')
                return this.user;
            }

            await utils.newNotification('An error Occured', `${err.message}`, true, 'erro', 5000)
            authState.set(AuthState.notAuthorized);
            return this.user;
        }
        
        this.doneAuth = true;
        // get other users:
        
        // for (let idx = 0; idx < count; idx++) {
        //     if (!this.getUser(idx, count)) {
        //         console.log(`error getting one of the users`);
        //         return this.user;
        //     }
        // }

        await tick();
        
        return this.user;
    }

    public async authenticate(current = 0): Promise<User> {
        // if (!this.firstRun) {
        //     this.firstRun = false
        //     window.onfocus = () => {
        //         this.refresh();
        //     }
        // }
        
        if (this.doneAuth)
            return Promise.resolve(this.user)
        
        this.current = current;

        if (this.authPromise === null)
            this.authPromise = this.doAuth();
        
        return await this.authPromise;
    }

    public async refresh(current = 0) {
        this.users = [];
        this.doneAuth = false;
        this.current = current;
        this.authPromise = this.doAuth();
        await this.authPromise;
    }

    public getAuthenticatedClient() {
        return this.client;
    }

    /** Returns true if user is authenticated */
    public authenticated() : boolean {
        return this.user.user_id != "default:::::"
    } 

    public endSessions() {
        const length = auth.users.length;                    
        deleteCookie(`co`)

        for (let i = 0; i < length; i++) {
            deleteCookie(`rf-${i}`)
        }

    }

    /* 
        Throws error if not authenticated 
    */
    public getCurrentUser(): User {
        return this.user
    }
    
    public getCurrentToken() : string {
        return this.token
    }

    public getUser(idx: number, count: number): boolean {
        if (count > 0) {
            if (idx >= 0)
                this.tokens.push(getCookie(`rf-${idx}`))
            else
                location.href = "/"
        }
        else 
            deleteCookie('co')

        const user = User.from_token(this.tokens[idx]);
        if (!user) {
            deleteCookie(`rf-${idx}`)
            authState.set(AuthState.notAuthorized)
            goto(`/auth/login`)
            return false;
        }

        this.users.push(user);
        return true
    }

    public getAllUsers(): User[] {
        return this.users
    }

    public async getKeys(): Promise<KeyStore> {
        const keys = await cryptoDb.get(this.user.user_id);

        if (typeof keys === 'undefined')
            throw new Error("Keys do not exist on this device, impossible to handle transaction");
        
        return keys!;
    }

} 

export const authState = writable<AuthState>(AuthState.authorizing);
export const auth = new AuthService();
