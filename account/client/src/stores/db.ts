import Dexie from 'dexie';
import type { Table } from 'dexie';

interface KeyStore {
    id: string;
    sign: {
        keyType: string;
        privateKey: string;
        publicKey: string;
    }
    crypto: {
        keyType: string;
        privateKey: string;
        publicKey: string;
    }
    keycheck: string;
}

class CryptoDb extends Dexie {
    // 'friends' is added by dexie when declaring the stores()
    // We just tell the typing system this is the case
    keys!: Table<KeyStore>;

    constructor() {
        super('higenku/keys');
        this.version(1).stores({
            keys: 'id', // Primary key and indexed props
            
        });
    }

    public async saveCreds(creds: KeyStore) {
        await this.keys.add(creds)
    }

    public async contains(id: string): Promise<boolean> {
        const val = await this.get(id);
        return typeof val !== 'undefined';
    }

    public async saveNewCreds(creds: KeyStore) {
        if (typeof this.keys.get(creds.id) !== "undefined") this.keys.delete(creds.id)
        await this.keys.add(creds)
    }

    public get(id: string): Promise<KeyStore | undefined> {
        return this.keys.get(id);
    }

    public async getLastIndex(): Promise<number> {
        const count = await this.keys.count()
        return count - 1; 
        
    }
}

export const cryptoDb = new CryptoDb();
export type {KeyStore}