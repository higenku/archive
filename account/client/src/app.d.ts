/// <reference types="@sveltejs/kit" />

// See https://kit.svelte.dev/docs/types#the-app-namespace
// for information about these interfaces
declare interface Window {
    turnstile: any;
    onloadTurnstileCallback: any;
    Stripe: (client_id: string, opt: any) => any;
}

declare namespace App {
	// interface Locals {}
	// interface Platform {}
	// interface Session {}
	// interface Stuff {}
}
