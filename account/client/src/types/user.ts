export class User {
    static from_token(token: string) : User {
        const dToken = JSON.parse(window.atob(token.split('.')[1]))
        return new User(
            dToken._id,
            dToken.user_id,
            dToken.email,
            dToken.avatar,
            dToken.username,
            dToken.email,
        )
    }
    constructor(
        public _id:      string,
        public user_id:      string,
        public name:     string,
        public avatar:   string,
        public username: string,
        public email:    string,
    ) {}
}



