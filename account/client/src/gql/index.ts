import type { GraphQLClient } from 'graphql-request';
import type * as Dom from 'graphql-request/dist/types.dom';
import type { DocumentNode } from 'graphql';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type ApiResult = {
  __typename?: 'ApiResult';
  message: Scalars['String'];
  status: ApiStatus;
};

export enum ApiStatus {
  DatabaseError = 'DATABASE_ERROR',
  Error = 'ERROR',
  ServerError = 'SERVER_ERROR',
  Success = 'SUCCESS'
}

export type AppPerm = {
  __typename?: 'AppPerm';
  /** has access to the api named 'name' */
  metadata: Scalars['String'];
  name: Scalars['String'];
};

export type AppResponse = {
  __typename?: 'AppResponse';
  id: Scalars['String'];
  logo: Scalars['String'];
  name: Scalars['String'];
  perms: Array<AppPerm>;
  redirectUri: Scalars['String'];
};

export type AuthLoginRequest = {
  captcha?: InputMaybe<Scalars['String']>;
  credential: Scalars['String'];
  input?: InputMaybe<Scalars['String']>;
  token?: InputMaybe<Scalars['String']>;
};

export type AuthRegisterRequest = {
  avatar?: InputMaybe<Scalars['String']>;
  captcha?: InputMaybe<Scalars['String']>;
  cryptoPubkey?: InputMaybe<Scalars['String']>;
  email: Scalars['String'];
  encryptedMessage?: InputMaybe<Scalars['String']>;
  encryptedStore?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  provider?: InputMaybe<Scalars['String']>;
  signPubkey?: InputMaybe<Scalars['String']>;
  token?: InputMaybe<Scalars['String']>;
  username?: InputMaybe<Scalars['String']>;
};

export type GSession = {
  __typename?: 'GSession';
  expires: Scalars['String'];
  id: Scalars['String'];
  locked: Scalars['Boolean'];
  userAgent: Scalars['String'];
  userId: Scalars['String'];
};

export type KeyResponse = {
  __typename?: 'KeyResponse';
  encryptedStore: Scalars['String'];
  keycheck: Scalars['String'];
};

export type Mutation = {
  __typename?: 'Mutation';
  appsCreate: Scalars['String'];
  appsDelete: Scalars['Boolean'];
  appsRegenClientSecret: Scalars['String'];
  appsUpdate: Scalars['Boolean'];
  authLogin: Scalars['String'];
  authLogout: Scalars['String'];
  authRegister: Scalars['String'];
  oauthApprove: Scalars['String'];
  userOauth2Remove: Scalars['Boolean'];
  userSessionDelete: Scalars['Boolean'];
  userU2faConfigure: Scalars['Boolean'];
  userU2faRemove: Scalars['Boolean'];
  userUpdate: Scalars['Boolean'];
};


export type MutationAppsCreateArgs = {
  signature: Scalars['String'];
};


export type MutationAppsDeleteArgs = {
  signature: Scalars['String'];
};


export type MutationAppsRegenClientSecretArgs = {
  signature: Scalars['String'];
};


export type MutationAppsUpdateArgs = {
  signature: Scalars['String'];
};


export type MutationAuthLoginArgs = {
  req: AuthLoginRequest;
};


export type MutationAuthRegisterArgs = {
  req: AuthRegisterRequest;
};


export type MutationOauthApproveArgs = {
  signature: Scalars['String'];
};


export type MutationUserOauth2RemoveArgs = {
  signature: Scalars['String'];
};


export type MutationUserSessionDeleteArgs = {
  signature: Scalars['String'];
};


export type MutationUserU2faConfigureArgs = {
  signature: Scalars['String'];
};


export type MutationUserU2faRemoveArgs = {
  signature: Scalars['String'];
};


export type MutationUserUpdateArgs = {
  signature: Scalars['String'];
};

export type OAuthResponse = {
  __typename?: 'OAuthResponse';
  github: Scalars['Boolean'];
  gitlab: Scalars['Boolean'];
  google: Scalars['Boolean'];
  microsoft: Scalars['Boolean'];
};

export type PageRespone = {
  __typename?: 'PageRespone';
  contents: Scalars['String'];
  result: ApiResult;
  url: Scalars['String'];
};

export type Query = {
  __typename?: 'Query';
  apiVersion: Scalars['String'];
  app: AppResponse;
  apps: Array<AppResponse>;
  authVerify: UserResponse;
  getPage: PageRespone;
  key: KeyResponse;
  keyVerify: Scalars['Boolean'];
  userOauth2: OAuthResponse;
  userSessions: Array<GSession>;
  userU2faConfigured: Scalars['Boolean'];
  userU2faNewUri: U2fa;
};


export type QueryAppArgs = {
  id: Scalars['String'];
};


export type QueryGetPageArgs = {
  username: Scalars['String'];
};


export type QueryKeyVerifyArgs = {
  encryptedMessage: Scalars['String'];
};

export type U2fa = {
  __typename?: 'U2fa';
  qr: Scalars['String'];
  secret: Scalars['String'];
  uri: Scalars['String'];
};

export type UserResponse = {
  __typename?: 'UserResponse';
  accessToken?: Maybe<Scalars['String']>;
  avatar: Scalars['String'];
  email: Scalars['String'];
  id: Scalars['String'];
  keycheck: Scalars['String'];
  name: Scalars['String'];
  username: Scalars['String'];
};

export type AppsCreateMutationVariables = Exact<{
  signature: Scalars['String'];
}>;


export type AppsCreateMutation = { __typename?: 'Mutation', appsCreate: string };

export type AppsDeleteMutationVariables = Exact<{
  signature: Scalars['String'];
}>;


export type AppsDeleteMutation = { __typename?: 'Mutation', appsDelete: boolean };

export type AppsRegenClientSecretMutationVariables = Exact<{
  signature: Scalars['String'];
}>;


export type AppsRegenClientSecretMutation = { __typename?: 'Mutation', appsRegenClientSecret: string };

export type AppsUpdateMutationVariables = Exact<{
  signature: Scalars['String'];
}>;


export type AppsUpdateMutation = { __typename?: 'Mutation', appsUpdate: boolean };

export type AppsQueryVariables = Exact<{ [key: string]: never; }>;


export type AppsQuery = { __typename?: 'Query', apps: Array<{ __typename?: 'AppResponse', id: string, name: string, redirectUri: string, logo: string, perms: Array<{ __typename?: 'AppPerm', name: string, metadata: string }> }> };

export type AppQueryVariables = Exact<{
  id: Scalars['String'];
}>;


export type AppQuery = { __typename?: 'Query', app: { __typename?: 'AppResponse', id: string, name: string, redirectUri: string, logo: string, perms: Array<{ __typename?: 'AppPerm', name: string, metadata: string }> } };

export type AuthRegisterMutationVariables = Exact<{
  req: AuthRegisterRequest;
}>;


export type AuthRegisterMutation = { __typename?: 'Mutation', authRegister: string };

export type AuthLoginMutationVariables = Exact<{
  req: AuthLoginRequest;
}>;


export type AuthLoginMutation = { __typename?: 'Mutation', authLogin: string };

export type AuthLogoutMutationVariables = Exact<{ [key: string]: never; }>;


export type AuthLogoutMutation = { __typename?: 'Mutation', authLogout: string };

export type AuthVerifyQueryVariables = Exact<{ [key: string]: never; }>;


export type AuthVerifyQuery = { __typename?: 'Query', authVerify: { __typename?: 'UserResponse', id: string, email: string, avatar: string, name: string, username: string, keycheck: string, accessToken?: string | null } };

export type ApiVersionQueryVariables = Exact<{ [key: string]: never; }>;


export type ApiVersionQuery = { __typename?: 'Query', apiVersion: string };

export type KeyQueryVariables = Exact<{ [key: string]: never; }>;


export type KeyQuery = { __typename?: 'Query', key: { __typename?: 'KeyResponse', encryptedStore: string, keycheck: string } };

export type KeyVerifyQueryVariables = Exact<{
  encryptedMessage: Scalars['String'];
}>;


export type KeyVerifyQuery = { __typename?: 'Query', keyVerify: boolean };

export type OauthApproveMutationVariables = Exact<{
  signature: Scalars['String'];
}>;


export type OauthApproveMutation = { __typename?: 'Mutation', oauthApprove: string };

export type UserSessionsQueryVariables = Exact<{ [key: string]: never; }>;


export type UserSessionsQuery = { __typename?: 'Query', userSessions: Array<{ __typename?: 'GSession', expires: string, id: string, locked: boolean, userAgent: string, userId: string }> };

export type UserU2faConfiguredQueryVariables = Exact<{ [key: string]: never; }>;


export type UserU2faConfiguredQuery = { __typename?: 'Query', userU2faConfigured: boolean };

export type UserU2faNewUriQueryVariables = Exact<{ [key: string]: never; }>;


export type UserU2faNewUriQuery = { __typename?: 'Query', userU2faNewUri: { __typename?: 'U2fa', uri: string, secret: string, qr: string } };

export type UserOauth2QueryVariables = Exact<{ [key: string]: never; }>;


export type UserOauth2Query = { __typename?: 'Query', userOauth2: { __typename?: 'OAuthResponse', gitlab: boolean, github: boolean, google: boolean, microsoft: boolean } };

export type UserSessionDeleteMutationVariables = Exact<{
  signature: Scalars['String'];
}>;


export type UserSessionDeleteMutation = { __typename?: 'Mutation', userSessionDelete: boolean };

export type UserUpdateMutationVariables = Exact<{
  signature: Scalars['String'];
}>;


export type UserUpdateMutation = { __typename?: 'Mutation', userUpdate: boolean };

export type UserU2faConfigureMutationVariables = Exact<{
  signature: Scalars['String'];
}>;


export type UserU2faConfigureMutation = { __typename?: 'Mutation', userU2faConfigure: boolean };

export type UserOauth2RemoveMutationVariables = Exact<{
  signature: Scalars['String'];
}>;


export type UserOauth2RemoveMutation = { __typename?: 'Mutation', userOauth2Remove: boolean };

export type UserU2faRemoveMutationVariables = Exact<{
  signature: Scalars['String'];
}>;


export type UserU2faRemoveMutation = { __typename?: 'Mutation', userU2faRemove: boolean };


export const AppsCreateDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"appsCreate"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"signature"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"appsCreate"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"signature"},"value":{"kind":"Variable","name":{"kind":"Name","value":"signature"}}}]}]}}]} as unknown as DocumentNode;
export const AppsDeleteDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"appsDelete"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"signature"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"appsDelete"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"signature"},"value":{"kind":"Variable","name":{"kind":"Name","value":"signature"}}}]}]}}]} as unknown as DocumentNode;
export const AppsRegenClientSecretDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"appsRegenClientSecret"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"signature"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"appsRegenClientSecret"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"signature"},"value":{"kind":"Variable","name":{"kind":"Name","value":"signature"}}}]}]}}]} as unknown as DocumentNode;
export const AppsUpdateDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"appsUpdate"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"signature"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"appsUpdate"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"signature"},"value":{"kind":"Variable","name":{"kind":"Name","value":"signature"}}}]}]}}]} as unknown as DocumentNode;
export const AppsDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"apps"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"apps"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"redirectUri"}},{"kind":"Field","name":{"kind":"Name","value":"logo"}},{"kind":"Field","name":{"kind":"Name","value":"perms"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"metadata"}}]}}]}}]}}]} as unknown as DocumentNode;
export const AppDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"app"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"app"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"redirectUri"}},{"kind":"Field","name":{"kind":"Name","value":"logo"}},{"kind":"Field","name":{"kind":"Name","value":"perms"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"metadata"}}]}}]}}]}}]} as unknown as DocumentNode;
export const AuthRegisterDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"authRegister"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"req"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"AuthRegisterRequest"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"authRegister"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"req"},"value":{"kind":"Variable","name":{"kind":"Name","value":"req"}}}]}]}}]} as unknown as DocumentNode;
export const AuthLoginDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"authLogin"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"req"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"AuthLoginRequest"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"authLogin"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"req"},"value":{"kind":"Variable","name":{"kind":"Name","value":"req"}}}]}]}}]} as unknown as DocumentNode;
export const AuthLogoutDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"authLogout"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"authLogout"}}]}}]} as unknown as DocumentNode;
export const AuthVerifyDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"authVerify"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"authVerify"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"email"}},{"kind":"Field","name":{"kind":"Name","value":"avatar"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"username"}},{"kind":"Field","name":{"kind":"Name","value":"keycheck"}},{"kind":"Field","name":{"kind":"Name","value":"accessToken"}}]}}]}}]} as unknown as DocumentNode;
export const ApiVersionDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"apiVersion"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"apiVersion"}}]}}]} as unknown as DocumentNode;
export const KeyDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"key"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"key"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"encryptedStore"}},{"kind":"Field","name":{"kind":"Name","value":"keycheck"}}]}}]}}]} as unknown as DocumentNode;
export const KeyVerifyDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"keyVerify"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"encryptedMessage"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"keyVerify"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"encryptedMessage"},"value":{"kind":"Variable","name":{"kind":"Name","value":"encryptedMessage"}}}]}]}}]} as unknown as DocumentNode;
export const OauthApproveDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"oauthApprove"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"signature"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"oauthApprove"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"signature"},"value":{"kind":"Variable","name":{"kind":"Name","value":"signature"}}}]}]}}]} as unknown as DocumentNode;
export const UserSessionsDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"userSessions"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"userSessions"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"expires"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"locked"}},{"kind":"Field","name":{"kind":"Name","value":"userAgent"}},{"kind":"Field","name":{"kind":"Name","value":"userId"}}]}}]}}]} as unknown as DocumentNode;
export const UserU2faConfiguredDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"userU2faConfigured"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"userU2faConfigured"}}]}}]} as unknown as DocumentNode;
export const UserU2faNewUriDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"userU2faNewUri"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"userU2faNewUri"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"uri"}},{"kind":"Field","name":{"kind":"Name","value":"secret"}},{"kind":"Field","name":{"kind":"Name","value":"qr"}}]}}]}}]} as unknown as DocumentNode;
export const UserOauth2Document = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"userOauth2"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"userOauth2"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"gitlab"}},{"kind":"Field","name":{"kind":"Name","value":"github"}},{"kind":"Field","name":{"kind":"Name","value":"google"}},{"kind":"Field","name":{"kind":"Name","value":"microsoft"}}]}}]}}]} as unknown as DocumentNode;
export const UserSessionDeleteDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"userSessionDelete"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"signature"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"userSessionDelete"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"signature"},"value":{"kind":"Variable","name":{"kind":"Name","value":"signature"}}}]}]}}]} as unknown as DocumentNode;
export const UserUpdateDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"userUpdate"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"signature"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"userUpdate"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"signature"},"value":{"kind":"Variable","name":{"kind":"Name","value":"signature"}}}]}]}}]} as unknown as DocumentNode;
export const UserU2faConfigureDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"userU2faConfigure"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"signature"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"userU2faConfigure"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"signature"},"value":{"kind":"Variable","name":{"kind":"Name","value":"signature"}}}]}]}}]} as unknown as DocumentNode;
export const UserOauth2RemoveDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"userOauth2Remove"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"signature"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"userOauth2Remove"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"signature"},"value":{"kind":"Variable","name":{"kind":"Name","value":"signature"}}}]}]}}]} as unknown as DocumentNode;
export const UserU2faRemoveDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"userU2faRemove"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"signature"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"userU2faRemove"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"signature"},"value":{"kind":"Variable","name":{"kind":"Name","value":"signature"}}}]}]}}]} as unknown as DocumentNode;

export type SdkFunctionWrapper = <T>(action: (requestHeaders?:Record<string, string>) => Promise<T>, operationName: string, operationType?: string) => Promise<T>;


const defaultWrapper: SdkFunctionWrapper = (action, _operationName, _operationType) => action();

export function getSdk(client: GraphQLClient, withWrapper: SdkFunctionWrapper = defaultWrapper) {
  return {
    appsCreate(variables: AppsCreateMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<AppsCreateMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<AppsCreateMutation>(AppsCreateDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'appsCreate', 'mutation');
    },
    appsDelete(variables: AppsDeleteMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<AppsDeleteMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<AppsDeleteMutation>(AppsDeleteDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'appsDelete', 'mutation');
    },
    appsRegenClientSecret(variables: AppsRegenClientSecretMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<AppsRegenClientSecretMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<AppsRegenClientSecretMutation>(AppsRegenClientSecretDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'appsRegenClientSecret', 'mutation');
    },
    appsUpdate(variables: AppsUpdateMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<AppsUpdateMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<AppsUpdateMutation>(AppsUpdateDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'appsUpdate', 'mutation');
    },
    apps(variables?: AppsQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<AppsQuery> {
      return withWrapper((wrappedRequestHeaders) => client.request<AppsQuery>(AppsDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'apps', 'query');
    },
    app(variables: AppQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<AppQuery> {
      return withWrapper((wrappedRequestHeaders) => client.request<AppQuery>(AppDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'app', 'query');
    },
    authRegister(variables: AuthRegisterMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<AuthRegisterMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<AuthRegisterMutation>(AuthRegisterDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'authRegister', 'mutation');
    },
    authLogin(variables: AuthLoginMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<AuthLoginMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<AuthLoginMutation>(AuthLoginDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'authLogin', 'mutation');
    },
    authLogout(variables?: AuthLogoutMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<AuthLogoutMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<AuthLogoutMutation>(AuthLogoutDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'authLogout', 'mutation');
    },
    authVerify(variables?: AuthVerifyQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<AuthVerifyQuery> {
      return withWrapper((wrappedRequestHeaders) => client.request<AuthVerifyQuery>(AuthVerifyDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'authVerify', 'query');
    },
    apiVersion(variables?: ApiVersionQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<ApiVersionQuery> {
      return withWrapper((wrappedRequestHeaders) => client.request<ApiVersionQuery>(ApiVersionDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'apiVersion', 'query');
    },
    key(variables?: KeyQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<KeyQuery> {
      return withWrapper((wrappedRequestHeaders) => client.request<KeyQuery>(KeyDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'key', 'query');
    },
    keyVerify(variables: KeyVerifyQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<KeyVerifyQuery> {
      return withWrapper((wrappedRequestHeaders) => client.request<KeyVerifyQuery>(KeyVerifyDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'keyVerify', 'query');
    },
    oauthApprove(variables: OauthApproveMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<OauthApproveMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<OauthApproveMutation>(OauthApproveDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'oauthApprove', 'mutation');
    },
    userSessions(variables?: UserSessionsQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<UserSessionsQuery> {
      return withWrapper((wrappedRequestHeaders) => client.request<UserSessionsQuery>(UserSessionsDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'userSessions', 'query');
    },
    userU2faConfigured(variables?: UserU2faConfiguredQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<UserU2faConfiguredQuery> {
      return withWrapper((wrappedRequestHeaders) => client.request<UserU2faConfiguredQuery>(UserU2faConfiguredDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'userU2faConfigured', 'query');
    },
    userU2faNewUri(variables?: UserU2faNewUriQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<UserU2faNewUriQuery> {
      return withWrapper((wrappedRequestHeaders) => client.request<UserU2faNewUriQuery>(UserU2faNewUriDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'userU2faNewUri', 'query');
    },
    userOauth2(variables?: UserOauth2QueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<UserOauth2Query> {
      return withWrapper((wrappedRequestHeaders) => client.request<UserOauth2Query>(UserOauth2Document, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'userOauth2', 'query');
    },
    userSessionDelete(variables: UserSessionDeleteMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<UserSessionDeleteMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<UserSessionDeleteMutation>(UserSessionDeleteDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'userSessionDelete', 'mutation');
    },
    userUpdate(variables: UserUpdateMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<UserUpdateMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<UserUpdateMutation>(UserUpdateDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'userUpdate', 'mutation');
    },
    userU2faConfigure(variables: UserU2faConfigureMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<UserU2faConfigureMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<UserU2faConfigureMutation>(UserU2faConfigureDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'userU2faConfigure', 'mutation');
    },
    userOauth2Remove(variables: UserOauth2RemoveMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<UserOauth2RemoveMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<UserOauth2RemoveMutation>(UserOauth2RemoveDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'userOauth2Remove', 'mutation');
    },
    userU2faRemove(variables: UserU2faRemoveMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<UserU2faRemoveMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<UserU2faRemoveMutation>(UserU2faRemoveDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'userU2faRemove', 'mutation');
    }
  };
}
export type Sdk = ReturnType<typeof getSdk>;