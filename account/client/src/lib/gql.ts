import { getSdk } from "$gql/index"
import { GraphQLClient } from "graphql-request"

const client = getSdk(new GraphQLClient(
    `${import.meta.env.VITE_API_URL}/gql`, 
    {
        credentials: 'include',
        headers: { authorization: `Bearer notAuthenticated` }
    }
))

export type ClientType = typeof client;
export default client;