
// eslint-disable-next-line @typescript-eslint/no-explicit-any
type Handler = any

interface Events { 
    [key: string]: Handler[]  
}

export class EventEmitter { 
    private _events: Events = {};

    public on(name: string, handler: Handler) {
        if (!this._events[name]) 
            this._events[name] = []
        this._events[name].push(handler)
    }

    public emit(name: string, args: any[] = []) {
        if (!this._events[name]) return;

        this._events[name].forEach(s => s(...args))
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    removeListener(name: string, listenerToRemove: any) {
        if (!this._events[name]) {
          throw new Error(`Can't remove a listener. Event "${name}" doesn't exits.`);
        }
    
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const filterListeners = (listener: any) => listener !== listenerToRemove;
    
        this._events[name] = this._events[name].filter(filterListeners);
      }
}