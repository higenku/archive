interface AdditionalInfo { 
    // eslint-disable-next-line
    header?: any
    method?: string
}


export const apiReq = (url: string, body?: string, additional?: AdditionalInfo) => {
 
    return fetch(new URL(url, `${import.meta.env.VITE_API_URL}/v1/`).toString(), { 
        credentials: 'include',
        method: additional?.method ?? "POST",
        headers: { 
            'Content-Type': 'application/json',
            ...(additional?.header ?? {} )
        },
        body,
    })
}

export const isLength = (str: string, { min = -1, max = -1 }) => {
    let res
    if (min != -1)
        res = str.length > min 

    if (max != -1)
        res = res && str.length < max
    
    return res
}

export const isEmail = (str: string) => {
    return isLength(str, {min: 4}) && str.includes('@')
}

// export const to_base64 = (input: string | Uint8Array) => sodium.to_base64(input, sodium.base64_variants.URLSAFE)
// export const from_base64 = (input: string) => sodium.from_base64(input, sodium.base64_variants.URLSAFE)
