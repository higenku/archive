import {resolve} from 'path';
import { sveltekit } from '@sveltejs/kit/vite';

const isBuild = process.env.BUILD === "true";

/** @type {import('vite').UserConfig} */
const config = {
    server: {
        port: 3000
    },
    ssr: {
        noExternal: [],
    },
    optimizeDeps: {
        exclude: ['libsodium-wrappers', 'libsodium']
    },
	plugins: [sveltekit()],
    resolve: {
        alias: {
            $store: resolve('./src/stores'),
            $com: resolve('./src/com'),
            $gql: resolve('./src/gql'),
            $types: resolve('./src/types'),
            'path': 'path-browserify',
            'fs': 'path-browserify',
        }
    },
};

if (isBuild) {
    config.ssr.noExternal.push('libsodium-wrappers')
}

export default config;
