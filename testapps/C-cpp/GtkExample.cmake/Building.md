## Building this project
to build this project you need pkg-config, cmake and gtk4

```
# Generate and build
cmake  -B build && cmake --build build

# Run 
./build/main[.exe]
```