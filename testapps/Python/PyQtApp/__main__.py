import sys
from PyQt5.QtWidgets import *

import sys

class App(QWidget):
    def __init__(self):
        super().__init__()
        self.btn = QPushButton("Click Me")
        self.title = 'PyQt5 simple window - pythonspot.com'
        self.left = 10
        self.top = 10
        self.width = 640
        self.height = 480
        self.initUI()
        
    def initUI(self):
        self.btn.clicked.connect(self.button_pressed)
        self.btn.setGeometry(200, 150, 100, 30)
        self.clicked = 0
    
        mainLayout = QVBoxLayout()
        mainLayout.addWidget(self.btn)
        
        self.setLayout(mainLayout)

        self.setWindowTitle("Form Layout - pythonspot.com")
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.show()

    def button_pressed(self):
        self.clicked += 1
        self.btn.setText(f"You Clicked {self.clicked} time(s)")
    
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
