# Load Gtk
import gi
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk


class App():
    def __init__(self):
        self.clicks = 0
        self.app = Gtk.Application(application_id='org.higenku.GtkExampleApplication')
        self.app.connect('activate', self.onActivate)

        # Run the application
        self.app.run(None)
    
    def onActivate(self, app):
        self.win = Gtk.ApplicationWindow(application=app)
        self.btn = Gtk.Button(label='Hello, World!')

        self.btn.connect('clicked', self.buttonClicked)

        self.win.set_size_request(1000, 900)
        self.win.set_child(self.btn)
        self.win.present()    

    def buttonClicked(self, widget):
        self.clicks += 1
        widget.set_label(f"Button Pressed for the {self.clicks} time(s)")  
        print(f"Button Pressed for the {self.clicks} time(s)")



App()
