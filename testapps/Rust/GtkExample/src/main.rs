use gtk4::prelude::*;
use gtk4::*;

static mut CLICKS: u32 = 0;

fn main() {
    
    let application = Application::new(Some("org.higenku.RustGtkExampleApplication"), Default::default());
    application.connect_activate(build_ui);
    application.run();
}

fn build_ui(application: &Application) {

    let window = ApplicationWindow::new(application);

    window.set_title(Some("Higenku Example Application"));
    window.set_default_size(350, 70);

    let button = Button::with_label("Click me!");
    button.connect_clicked(|btn| {
        unsafe { CLICKS += 1; }

        let clicks = unsafe { CLICKS };
        btn.set_label(format!("you clicked {} time(s)", clicks).as_str());
    });
    window.set_child(Some(&button));

    window.show();
}