# C# Applications
This repository is dedicated to show how to publish various types of c# applications to Higenku

## Applications
- HelloWolrd:     A simple Console App
- GtkExample:     A simple Gtk App
- Blazor Server:  A simple Blazor Server Example using Webview
- Blazor Pwa:	  A simple Blazor Pwa Using Pwa Packager
