# Vala Examples
- simpleapp: Simple Console app
- gtkapp: Simple Gtk App 

## Building
consider `${ProjName}` the folder that the project is. You need meson, ninja, glib and gtk to build these projects
```
cd ${ProjName}

meson setup build
meson compile -C build
```