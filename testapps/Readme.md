# Higenku Test Applications

The languages we have test applications are:
- C++ / C in C-cpp
- C# in Csharp
- Go in Go
- Java in Java
- JavaScript and TypeScript in JavascriptTypescript
- Pwa in Pwa
- Python in Python
- Rust in Rust
- Vala in Vala

## License
This project is license in both [Apache v2](LICENSE.APACHE) and [MIT](LICENSE.MIT) Licenses.
