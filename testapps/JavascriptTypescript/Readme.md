# Typescript and Javascript Applications
This repository is dedicated to show how to publish various types of Javascript and Typescript applications to Higenku

## Applications
- HelloWolrd.js:               A simple Console App using javascript
- HelloWolrd.ts:               A simple Console App using typescript
- Gtk.js:                      A simple Gtk App using javscript
- Qt.js:                       A simple Gtk App using typescript
- Svelte.pwa:                  A simple Pwa App using typescript and svelte
- Angular.pwa:                 A simple Pwa app using typescript and angular
- Vue.pwa:                     A simple Pwa app using typescript and vue
- React.pwa:                   A simple Pwa app using typescript and react

- Svelte.ele:                  A simple Electron App using typescript and svelte
- Angular.ele:                 A simple Electron app using typescript and angular
- Vue.ele:                     A simple Electron app using typescript and vue
- React.ele:                   A simple Electron app using typescript and react
