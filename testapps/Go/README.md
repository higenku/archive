# Go Applications
This repository is dedicated to show how to publish various types of Go applications to Higenku

## Applications 
- helloWolrd:               A simple Console App
- gtkExample:               A simple Gtk App
- qtExample:                A simple Qt App
- wailsExample:                 A simple Wails App
