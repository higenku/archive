# Higenku Packer (XPacker) [![pipeline status](https://gitlab.com/higenku/tools/packer/badges/dev/pipeline.svg)](https://gitlab.com/higenku/tools/packer/-/commits/dev) [![Docker Repository on Quay](https://quay.io/repository/higenku/t_packer/status?token=d4b67998-438e-4ec3-9700-546c9a7fbdc1 "Docker Repository on Quay")](https://quay.io/repository/higenku/t_packer)
App Packer for Higenku Ecossystem.

[Documentation](https://docs.higenku.org/tools/packer/)

## Table of contents
[toc]

## Introduction
Hello there,
Here is all the source code related to the Packer Application, which is the base application to all applications that are in the [Store](https://store.higenku.org)

## Features
- Multiple Environments, you can use multiple languages, compilers and tools together with this tool
- Support for Container environments
- Extensions Support

## Feedback
- You can start a duscussion on the [Feedback Cattegory](https://community.higenku.org/t/feedback) or in our [Discord/Matrix](https://higenku.org/contact)

## Contributing
See the [contributing guide](https://gitlab.com/higenku/legal/-/blob/dev/Contributing.md)

## Sponsors and Acknowledgments
See the [main site](https://higenku.org/acknowledgments)
