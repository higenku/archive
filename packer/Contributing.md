# Contributing guide
Like the other projects, this contributing guide is just a supplementary of the main one present at [Contributing guide](https://higenku.org/legal/contributing)

## Code Style
On rust files, please follow the [Rust coding style](https://github.com/rust-dev-tools/fmt-rfcs/blob/master/guide/guide.md)

## creating a new extension
New extension are always welcome to support new languages, build systems and ways to make developers life easier.

Each extension should have the following structure:
- Must have a package name that starts with `libxpacker_ext_` and a name itself. For example, dotnet has the package name `libxpacker_ext_dotnet` but the name itself is dotnet.
- They should be defined as packages in the `extensions/` and should have the directory name equivalent to the extension name. For example `extensions/dotnet`.
- Each extension should contain some code in the `lib.rs` that follows:
```rs
pub struct NAMEPacker;

impl NAMEPacker {
    pub fn new() -> Self {
        NAMEPacker {}
    }
}

impl Packer for NAMEPacker {
    fn name(&self) -> String {
        String::from("NAME")
    }

    fn version(&self) -> String {
        env!("CARGO_PKG_VERSION").to_string()
    }

    fn pack(&self, _: &String, _: &PkgSpec, _: &Arguments) -> Result<(), (std::string::String, std::option::Option<std::string::String>)> {
    // do the packer extension packing
        Ok(())
    }

    fn generate(&self, dir: &String, _: &mut Arguments, spec: PkgSpec) -> Result<PkgSpec, (std::string::String, std::option::Option<std::string::String>)> {
        let mut spec = spec;
        spec.package.name = "NAME".to_string();

        log::info!("Generation of Specification Successful, see more at {}", Path::new(dir).join("pkg.yml").as_path().to_str().unwrap());
        Ok(spec)
    }

    fn prefix(&self) -> Vec<String> {
        vec![
            "NAME".to_string()
        ]
    }
}

declare_packer!(NAMEPacker, NAMEPacker::new);
```
As you can see, the `declare_packer!` macro is special because it allows the packer to understand how to create an instance of your packer. There is no dependency injection.

A simple example of a packer extension can be found [here](https://gitlab.com/higenku/tools/packer/-/tree/dev/Source/extensions/custom), and a complex one can be found [here](https://gitlab.com/higenku/tools/packer/-/tree/dev/Source/extensions/rust).
