#+++++++++++++
# Installation
#+++++++++++++

pre_update() {
    echo "Updating..."
}

pos_update() {
    echo "Updated"
}

pre_install() {
    echo "installing..."
}

pos_install() {
    echo "installed"
}

#+++++++++++++
# Extensions
#+++++++++++++
pre_ext_update() {
    echo "Extension Updating..."
}

pos_ext_update() {
    echo "Extension Updated"
}

pre_ext_install() {
    echo "Extension installing..."
}

pos_ext_install() {
    echo "Extension installed"
}
