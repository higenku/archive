package main

import (
	"fmt"
)

func main() {
	fmt.Println("Hello world")

	var app string = "hello world"

	fmt.Println("I am ", app, "world")
}
