use glob::glob;
use sha2::Digest;
use std::{collections::HashMap, fs::File, io::{Write}, path::Path};
use serde::{Deserialize, Serialize};
use crate::{log, Runtime};
use super::pack::PkgSpec;

#[derive(Debug, Serialize, Deserialize)]
pub struct Manifest {
    pub id: String,
    pub version: String,
    pub dependencies: Option<Vec<String>>,
    pub runtime: Runtime,
    pub xpkg: String,
    pub filehashes: HashMap<String, String>
}

impl Manifest {
    pub fn new(spec: PkgSpec) -> Result<Self, String> {
        let mut man = Manifest {
            id: spec.id,
            xpkg: spec.xpkg,
            filehashes: HashMap::new(),
            version: spec.version,
            dependencies: spec.dependencies,
            runtime: spec.runtime,
        };
        
        let files = match glob(".xpkg/*.xpkg") {
            Ok(e) => e,
            Err(e) => return Err(e.to_string()),
        }; 

        for file in files {
            if let Ok(file) = file {
                let filename = match file.as_path().to_str() {
                    Some(e) => e,
                    None => return Err(format!("Couldn't get filename")),
                };

                let input = Path::new(filename);
                let input = match std::fs::read(input) {
                    Ok(e) => e,
                    Err(e) => {
                        log::error!("Error while reading file {}", input.to_string_lossy());
                        return Err(e.to_string());
                    }
                };
                let mut hasher = sha2::Sha256::default();
                hasher.update(input);
                let hash = hasher.finalize().to_vec();


                man.filehashes.insert(filename.replace(".xpkg/", "").to_string(), format!("sha256:{}", base64::encode_config(hash, base64::URL_SAFE)));

            }
        }

        Ok(man)
    }

    pub fn write(&self) -> Result<(), String>{
        let mut file = match File::create(".xpkg/manifest.json") {
            Ok(e) => e,
            Err(e) => return Err(e.to_string()),
        };
        let contents = match serde_json::to_string_pretty(self) {
            Ok(e) => e,
            Err(e) => return Err(format!("{:#?}", e)),
        };

        match file.write_all(contents.as_bytes()) {
            Ok(_) => return Ok(()),
            Err(e) => return Err(e.to_string()),
        };
    }
}