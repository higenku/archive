use serde::{Serialize, Deserialize};
use std::{fs, io::Write};
use crate::log;
use super::pack::{Action, PkgSpec, Runtime};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct PkgManifest {
    pub id: String,
    pub xpkg: String,

    pub dependencies: Vec<String>,
    pub install_instructions: Vec<String>,
    pub runtime: Runtime,
    pub actions: Vec<Action>,
    pub files: Vec<String>,
}

impl PkgManifest {
    pub fn from_pkgspec(spec: &PkgSpec) -> Result<Self, String> {
        let spec = spec.clone();
        let man = PkgManifest {
            id: spec.id,
            xpkg: spec.xpkg,

            dependencies: spec.dependencies.unwrap_or(vec![]),
            install_instructions: spec.install_instructions.unwrap_or(vec![]),
            runtime: spec.runtime,
            actions: spec.actions.unwrap_or(vec![]),
            files: vec!["manifest.json".to_owned()],
        };

        Ok(man)
    }

    pub fn write(self, path: String) -> Result<(), String> {
        let contents = match serde_json::to_string_pretty(&self) {
            Err(e) => {
                log::error!("Error while writing manifest file, parsing manifest error");
                return Err(e.to_string());
            }
            Ok(s) => s,
        };

        let mut file = match std::fs::File::create(path) {
            Ok(e) => e,
            Err(e) => return Err(e.to_string()),
        };

        match file.write_all(contents.as_bytes()) {
            Ok(_) => {},
            Err(e) => return Err(e.to_string()),
        };
        log::debug!("Finished writing manifest");
        Ok(())
    }

    fn getfiles_fromdir(&self, dir: String) -> Vec<String> {
        let mut res: Vec<String> = vec![];
        
        let files = match fs::read_dir(dir) {
            Ok(e) => e,
            Err(e) => {
                log::error!("An Error Occured while generating manifest.files");
                log::debug!("{}", e);
                return vec![];
            }
        };

        for file in files {
            if let Ok(file) = file {
                let path = file.path();
                if path.is_dir() {
                    res.extend(self.getfiles_fromdir(file.path().to_string_lossy().to_string()));
                }
                else {
                    let file = file.path().to_string_lossy().to_string();
                    res.push(file);
                }
            }
            else if let Err(e) = file {
                log::error!("An Error Occured while generating manifest.files");
                log::debug!("{}", e);
                return vec![];
            }
        }
        res
    }

    pub fn getfiles<S: AsRef<str>>(&mut self, dir: S) {
        let dir = dir.as_ref().to_string();
        let mut root = dir.clone();
        if !root.ends_with("/") {
            root = format!("{}/", &root);
        }

        let files = self.getfiles_fromdir(dir);
    
        // log::error!("Length {}", files.len());
        for file in files {
            self.files.push(file.replace(&root, ""));
        }
    }
}