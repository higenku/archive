use std::{collections::HashMap, path::Path, vec};

use serde::{Deserialize, Serialize};

use crate::log;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct PkgSpec {
    pub id: String, 
    pub version: String, 
    pub repo: String, 
    pub license: Option<String>, 
    
    pub package: Package, 
    
    pub runtime: Runtime, 
    pub xpkg: String,
    
    pub dependencies: Option<Vec<String>>, 
    pub install_instructions: Option<Vec<String>>, 
    
    pub actions: Option<Vec<Action>>, 
    pub targets: Vec<Target>,
    pub scripts: Option<String>,
    pub meta: Option<HashMap<String, String>>,

    pub include_files: Option<Vec<String>>,
}

impl PkgSpec {
    pub fn write(&self, dir: &String) {
        if let Some(path) = Path::new(&dir).join("pkg.yml").as_path().to_str() {
            if let Ok(contents) = serde_yaml::to_string(self) {
                if let Err(e) = std::fs::write(path, contents) {
                    log::error!("Failed to write data to {}", &path);
                    log::debug!("{:#?}", e);
                }
            }
        }
        else {
            log::error!("Invalid path");
        }
    }

    // Loads the spec from the file path provided
    pub fn load<S: AsRef<str>>(file_path: S) -> Result<Self, String> {
        let file_path = file_path.as_ref().to_owned();
        let contests = match std::fs::read_to_string(&file_path) {
            Err(_) => return Err(format!("Error while reading file {}", file_path)),
            Ok(e) => e,
        };
        let spec = match serde_yaml::from_str::<PkgSpec>(contests.as_str()) {
            Err(e) => {
                return Err(format!("{:#?}", e));
            },
            Ok(e) => e,
        };

        Ok(spec)
    }

}

#[derive(Debug, PartialEq, Serialize, Deserialize, Clone)]
pub struct Runtime {
    pub name: String,
    pub version: String,
}

impl Runtime {
    pub fn new<S: AsRef<str>>(name: S, version: S) -> Self {
        let name = name.as_ref().to_string();
        let version = version.as_ref().to_string();
        Runtime { name, version }
    }
}

#[derive(Debug, PartialEq, Serialize, Deserialize, Clone)]
pub struct Action {
    pub name: String,
    pub args: String,
}
impl Action {
    pub fn new<S: AsRef<str>>(name: S, args: S) -> Self {
        let name = name.as_ref().to_string();
        let args = args.as_ref().to_string();
        Action { name, args }
    }
}

#[derive(Debug, PartialEq, Serialize, Deserialize, Clone)]
pub struct Target {
    pub sys: String,
    pub dir: String,
}

impl Target {
    pub fn new<S: AsRef<str>>(dir: S, sys: S) -> Self {
        let dir = dir.as_ref().to_string();
        let sys = sys.as_ref().to_string();
        Target { dir, sys }
    }
}

#[derive(Debug, PartialEq, Serialize, Deserialize, Clone)]
pub struct Package {
    pub name: String,
    pub entry: Option<String>,
    pub args: HashMap<String, String>,
}

impl PkgSpec {
    pub fn default(include_scripts: bool) -> Self {
        let mut spec = PkgSpec {
            id: "company.myapp".to_string(),
            version: "1.0.0".to_string(),
            repo: "https://gitlab.com/company/app".to_string(),
            license: Some("LICENSE".to_string()),
            runtime: Runtime::new("asm", "1.0.0"),
            xpkg: "app".to_string(),
            dependencies: Some(vec![]),
            install_instructions: Some(vec![]),
            // install_instructions: vec![
            //     "create desktop_shortcut".to_string(),
            //     "create menu_shortcut".to_string(),
            //     "set extension_folder extensions/".to_string(),
            //     "set autostrart true".to_string(),
            // ],
            package: Package {
                name: String::new(),
                entry: Some(String::new()),
                args: HashMap::new(),
            },
            actions: Some(vec![]),
            targets: vec![
                Target::new(".xpkg/linux", "linux_amd64"), 
                Target::new(".xpkg/linux_aarch64", "linux_aarch64"),

                Target::new(".xpkg/macos", "macos_amd64"), 
                Target::new(".xpkg/macos_aarch64", "macos_aarch64"),
                
                Target::new(".xpkg/win", "win_amd64"),
            ],
            scripts: Some(String::new()),
            meta:  Some(HashMap::new()),
            include_files: Some(vec![]),
        };

        if include_scripts {
            spec.scripts = Some("scripts.sh".to_string());
        }

        spec
    }
}