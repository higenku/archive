use std::ops::Index;

#[derive(Debug)]
pub struct Arguments {
    args: Vec<String>
}

impl Arguments {
    pub fn new(args: Vec<String>) -> Self {
        Self { args: args[1..].to_vec() }
    }

    pub fn get_next(&mut self) -> Option<String> {
        if self.args.len() == 0 {
            return None;
        }

        let ele = self.args.remove(0);
        Some(ele)
    }

    pub fn get_idx<S: AsRef<str>>(&self, arg: S) -> Option<usize> {
        let arg = &arg.as_ref().to_owned();
        if self.contains(arg) {
            Some(self.args.iter().enumerate().find(|s| s.1.eq(arg)).unwrap().0)
        }
        else {
            None
        }
    }

    pub fn remove<S: AsRef<str>>(&mut self, arg: S) -> Result<(), ()> {
        if self.contains(&arg) {
            if self.remove_idx(self.get_idx(arg).unwrap()).is_err() {
                return Err(());
            }
            Ok(())
        }
        else {
            Err(())
        }
    }

    pub fn remove_idx(&mut self, idx: usize) -> Result<(), ()> {
        if let Some(_) = self.args.get(idx) {
            self.args.remove(idx);
            Ok(())
        }
        else {
            Err(())
        }
    }

    pub fn get_next_to<S: AsRef<str>>(&mut self, arg: S) -> Option<String> {
        let arg = &arg.as_ref().to_owned();
        if self.args.contains(arg) {
            let idx = self.args.iter().position(|e| e.eq(arg)).expect("Problem found");
            if self.args.len()-1 ==  idx {
                return None;
            }
            else {
                return Some(self.args.index(idx + 1).clone());
            }
        }
        else {
            None
        }
    }

    pub fn contains<S: AsRef<str>>(&self, arg: S) -> bool {
        self.args.contains(&arg.as_ref().to_string())
    }

    pub fn contains_idx(&self, idx: usize) -> bool {
        self.args.len() > idx
    }
}

#[cfg(test)]
mod test {
    use crate::argument_parser::Arguments;

    #[test]
    pub fn should_create_arguments_and_test_functions() {
        let args = vec!["pack".to_string(), "-C".to_string(), "../../app".to_string()];
        let mut args = Arguments::new(args);
        
        let arg1 = "pack".to_string();
        let arg2 = "-C".to_string();
        let arg3 = "../../app".to_string();
        assert!(!args.contains_idx(7));

        assert!(args.contains_idx(0));
        assert!(args.contains_idx(1));
        assert!(args.contains_idx(2));
        assert!(!args.contains_idx(3));


        assert_eq!(args.get_idx(&arg1), Some(0));
        assert_eq!(args.get_idx(&arg2), Some(1));
        assert_eq!(args.get_idx(&arg3), Some(2));

        assert!(args.contains(&arg1));
        assert!(args.contains(&arg2));
        assert!(args.contains(&arg3));

        assert_eq!(args.get_next_to(&arg1), Some(arg2.clone()));
        assert_eq!(args.get_next_to(&arg2), Some(arg3.clone()));
        assert_eq!(args.get_next_to(&arg3), None);

        assert!(args.remove("Non Existing Item").is_err());
        assert!(args.remove(arg1).is_ok());
        assert!(args.remove_idx(0).is_ok());

        assert_eq!(args.get_next(), Some(arg3));
        assert_eq!(args.get_next(), None);
    }

}