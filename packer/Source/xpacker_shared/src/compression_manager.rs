use std::{fs::{self, File}, io::{Cursor, Read, Write}, path::Path, ffi::OsStr};
use tar::{Archive, Builder};
use zip::{write::FileOptions, ZipArchive};
// use flate2::Compression;
// use flate2::{
//     write::{ZlibEncoder},
//     read::ZlibDecoder
// };
use zstd::{Encoder, Decoder};
use crate::log;

const COMPRESSION_LEVEL: i32 = 4;
pub struct CompressionManager;

impl CompressionManager {
    /// decompresses the xpkg into the desired output directory
    pub fn decompress(filepath: &str, outdir: &str) {
        let file = match fs::read(filepath) {
            Ok(e) => e,
            Err(_) => {
                log::error!("File not found {}", filepath);
                return;
            }
        };

        let contents: &[u8] = file.as_ref();
        let mut decoder = match Decoder::new(contents) {
            Ok(e) => e,
            Err(e) => {
                log::error!("Error decompressing stream");
                log::error!("{:#?}", e);
                return;
            }
        }; 
        
        let mut buf = Vec::new();
        match  decoder.read_to_end(&mut buf) {
            Ok(_) => {},
            Err(e) => {
                log::error!("Error decompressing stream");
                log::error!("{:#?}", e);
                return;
            }
        };

        let mut arc = Archive::new(Cursor::new(buf));
        
        if let Err(e) = arc.unpack(outdir) {
            log::error!("Couldn't extract file {} to {}", filepath, outdir);
            log::debug!("{:#?}", e);
        }

    }

    /// addes the contents of folder to the outfile file and replace the prefix of folder with root
    pub fn compress_folder(folder: &str, root: &mut str, outfile: &str, target: &str) -> Result<(), String> {
        let mut root = root.to_string();
        log::debug!("packing {} with root {} into {}", folder, root, outfile);
        if !Path::new(".xpkg").exists() {
            match fs::create_dir_all(".xpkg") {
                Ok(_) => {},
                Err(e) => return Err(e.to_string()),
            }
        }
        
        // removes the file if it exists
        if Path::new(outfile).exists() {
            match fs::remove_file(outfile) {
                Ok(_) => {},
                Err(e) => return Err(e.to_string()),
            };
        }

        // normalize directories
        if !root.ends_with("/") {
            root.push('/');
        }
        
        let root = root.to_owned();
        log::debug!("Compressing Tar");

        // Tar Processing
        let tmp_filepath = format!("{}.tmp", &outfile);
        let total;
        {
            let tmpfile = match File::create(tmp_filepath.as_str()) {
                Ok(e) => e,
                Err(e) => return Err(e.to_string()),
            };

            let mut arc = Builder::new(tmpfile);
            
            let (files, dirs) = CompressionManager::read_dir_recursive(folder);
            total = dirs.len() + files.len() + 3;
            let mut finished = 0 as usize; 
            
            for dir in dirs {
                match arc.append_path_with_name(&dir, &dir.replace(&root, "")) {
                    Ok(_) => {},
                    Err(e) => return Err(e.to_string()),
                };
                
                finished += 1;
                let filename = Path::new(&dir).file_stem().unwrap_or(OsStr::new("File")).to_string_lossy();
                log::prog(finished as u32, total as u32, format!("Added {}", filename));
            } 
            
            for file_path in files {
                let mut file = match File::open(&file_path) {
                    Ok(e) => e,
                    Err(e) => return Err(e.to_string()),
                };
                
                match arc.append_file(&file_path.replace(&root, ""), &mut file) {
                    Ok(e) => e,
                    Err(e) => return Err(e.to_string()),
                };
                
                finished += 1;
                let filename = Path::new(&file_path).file_stem().unwrap_or(OsStr::new("File")).to_string_lossy();
                log::prog(finished as u32, total as u32, format!("Added {} {}/{}", filename, finished, total));
            }
            log::prog((finished + 1) as u32, total as u32, format!("{}", target));
            match arc.finish() {
                Ok(_) => {},
                Err(e) => return Err(e.to_string()),
            };
        }
        
        let mut file = match File::create(outfile) {
            Ok(e) => e,
            Err(e) => return Err(e.to_string()),
        };
        
        // ! Final Compression
        log::prog((total - 1) as u32, total as u32, "Final Compression, final stage!".to_string());
        let contents = fs::read(&tmp_filepath).expect("Couldn't read temporary file");
        let contents: &[u8] = contents.as_ref();
        
        let mut compressor = match Encoder::new(Vec::new(), COMPRESSION_LEVEL) {
            Ok(e) => e,
            Err(e) => {
                log::error!("Error Occured while compressing to zstd");
                return Err(e.to_string());
            }
        };
        match compressor.write_all(contents) {
            Ok(_) => {}
            Err(e) => {
                log::error!("Error Occured while compressing to zstd");
                return Err(e.to_string());
            }
        };

        let contents = match compressor.finish() {
            Ok(e) => e,
            Err(e) => {
                log::error!("Error Occured while compressing to zlib");
                return Err(e.to_string());
            },
        };

        log::prog_done(format!("Packed {}", &target));

        if let Err(e) = file.write_all(contents.as_ref()) {
            log::error!("Failed writing package {}", &outfile);
            return Err(e.to_string());
        }

        log::info!("Removing temporary files");
        fs::remove_file(&tmp_filepath).expect("Couldn't remove temporary file");
        log::debug!("Finished removing files");
        Ok(())
    }

    fn read_dir_recursive(folder: &str) -> (Vec<String>, Vec<String>) {
        let mut dirs = Vec::<String>::new();
        let mut files = Vec::<String>::new();

        for i in fs::read_dir(folder).expect(format!("Folder doesn't allow reading or doesn't exists {}", folder).as_str()) {
            if let Ok(entry) = i {
                let path = entry.path();
                let string_path = path.to_str().unwrap().to_string();
                if path.is_dir() {
                    log::debug!("Adding Directory {}", &string_path);
                    dirs.push(string_path.clone());   
                    let (_files, _dirs) = CompressionManager::read_dir_recursive(&string_path);
                    for file in _files { files.push(file)}
                    for dir in _dirs { dirs.push(dir) }
                }
                else if path.is_file() {
                    log::debug!("Adding File {}", &path.to_str().unwrap());
                    files.push(string_path)
                }
            }
        }

        (files, dirs)
    }

    pub fn compress_to_zip(folder: &str, outfile: &str) -> Result<(), String> {

        log::prog(0, 100, "Starting to compress to zip".to_string());
        use zip::{CompressionMethod, ZipWriter};

        let curdir = std::env::current_dir();
        if let Err(e) = curdir {
            return Err(format!("Failed to get current directory: {:?}", e))
        }
        let curdir = curdir.unwrap();
        let curdir = curdir.to_string_lossy().to_string();

        let outfile = Path::new(&curdir).join(outfile);
        let outfile = outfile.to_string_lossy().to_string();

        if let Err(e) = std::env::set_current_dir(folder) {
            return Err(format!("Failed to change directory: {:?}", e));
        }

        let (files, _) = CompressionManager::read_dir_recursive("."); 

        if Path::new(&outfile).exists() {
            if let Err(e) = std::fs::remove_file(&outfile) {
                return Err(e.to_string());
            }
        }

        let file = File::create(&outfile);

        if let Err(e) = file {
            return Err(e.to_string());
        }

        let file = file.unwrap();

        let mut writer = ZipWriter::new(file);

        let options = FileOptions::default()
            .compression_method(CompressionMethod::Deflated)
            .large_file(true)
            .compression_level(Some(7));

        // for dir in &dirs { 
            
        // }

        let mut buffer = Vec::<u8>::new();

        let mut count = 0;
        for file in &files {
            let path = file.to_owned();

            log::prog(count, files.len() as u32, format!("Compressing: {:?}", Path::new(&path).file_name()));
            
            if let Err (e) = writer.start_file(&path, options) {
                return Err(format!("Failed to start a new file: {:?}", e));
            }
            let mut f = File::open(file).unwrap();

            if let Err(e) = f.read_to_end(&mut buffer){ 
                return Err(format!("Failed to read file: {:?}", e));
            }
            if let Err(e) = writer.write_all(&buffer) {
                return Err(format!("Failed to write to zip: {:?}", e));
            }

            // reset buffer
            buffer.clear();
            count += 1;
        }
        log::prog_done(format!("Finshed to compress to zip: {}", outfile));

        // closes the buffer
        if let Err(e) = writer.finish() {
            return Err(format!("Error closing zip: {:?}", e));
        }

        if let Err(e) = std::env::set_current_dir(&curdir) {
            return Err(format!("Failed to change directory: {:?}", e));
        }
        
        Ok(())
    }

    /// decompress the {archive} into the {dir}
    pub fn decompress_zip(archive: &str, dir: &str) -> Result<(), String> {
        log::prog(0, 100, format!("starting to decompress file {}", archive));

        if !Path::new(&archive).exists() {
                return Err(format!("File {archive} does not exist"));
        }

        let file = File::open(&archive);

        if let Err(e) = file {
            return Err(e.to_string());
        }
        let file = file.unwrap();

        let arc = ZipArchive::new(file);

        if let Err(e) = arc {
            return Err(e.to_string());
        }
        let mut arc = arc.unwrap();



        let count = arc.len() as u32;
        for i in 0..arc.len() {
            if let Ok(mut entry) = arc.by_index(i) {
                log::prog(i as u32, count, format!("Decompressing {}", entry.name()));

                let path = Path::new(dir).join(entry.name());

                if entry.is_dir() {
                    if let Err(e) = fs::create_dir_all(&path) {
                        return Err(format!("Failed to create dir: {}\n{e:?}", path.to_string_lossy()))
                    }
                }
                else if entry.is_file() {
                    if let Err(e) = fs::create_dir_all(path.parent().unwrap()) {
                        return Err(format!("Failed to create dir: {:?},\n{e:?}", path.parent()))
                    }

                    let file = File::create(&path);
                    if let Err(e) = file {
                        return Err(format!("Failed to create file: {},\n{e:?}", path.to_string_lossy()))
                    }
                    let mut file = file.unwrap();
                    
                    if let Err(e) = std::io::copy(&mut entry, &mut file) {
                        return Err(format!("Failed to write file: {},\n{e:?}", path.to_string_lossy()))
                    }
                    
                }

                // let path = Path::new(dir).join(file.)
                // File::create()
            }
        }
        
        log::prog_done(format!("Finished Decompressing into {}", &dir));


        Ok(())
    }
}