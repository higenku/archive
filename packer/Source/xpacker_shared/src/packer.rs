use crate::{Arguments, log, models::pack::PkgSpec};
use std::{any::Any, env::consts, fs};
use libloading::{Library, Symbol};

#[macro_export]
macro_rules! declare_packer {
    ($plugin_type:ty, $constructor:path) => {
        #[no_mangle]
        pub extern "C" fn __xpacker__internals__create_plugin() -> *mut dyn $crate::Packer {
            // make sure the constructor is the correct type.
            let constructor: fn() -> $plugin_type = $constructor;

            let object = constructor();
            let boxed: Box<dyn $crate::Packer> = Box::new(object);
            Box::into_raw(boxed)
        }
    };
}

pub type PackerResult =  Result<(), (String, Option<String>)>;

// Likely to change
pub trait Packer: Any + Send + Sync {
    fn name(&self) -> String;
    fn version(&self) -> String;
    fn pack(&self, dir: &String, spec: &PkgSpec, args: &Arguments) -> PackerResult;
    fn generate(&self, dir: &String, args: &mut Arguments, initial_spec: PkgSpec) -> Result<PkgSpec, (String, Option<String>)>;
    fn prefix(&self) -> Vec<String>;
}


pub struct PackerManager {
    pub packers: Vec<Box<dyn Packer>>,
    pub loaded_libraries: Vec<Library>,
}

impl Drop for PackerManager {
    fn drop(&mut self) {
        if !self.packers.is_empty() || !self.loaded_libraries.is_empty() {
            self.unload();
        }
        log::debug!("Unloading Packer Manager");
    }
}

impl PackerManager {
    pub fn new() -> Self {
        PackerManager {
            loaded_libraries: Vec::new(),
            packers: Vec::new(),
        }
    }

    pub fn get_packer(&mut self, ty: &String) -> Option<&Box<dyn Packer>> {
        let idx = self.packers.iter().position(|e| e.prefix().contains(ty));
        if let Some(idx) = idx {
            Some(&self.packers[idx])
        }
        else {
            None
        }
    }

    pub fn load(&mut self) -> Result<(), ()> {
        let packer_extension_path = std::env::current_exe().unwrap();
        let packer_extension_path = packer_extension_path.as_path().parent().expect("Extension Path doens't exist");

        log::debug!("Loading Libraries from {}", &packer_extension_path.display());
        for entry in fs::read_dir(packer_extension_path).unwrap() {
            if let Ok(entry) = entry {
                let path = entry.path();
                if path.is_dir() {
                    continue;
                }
                
                if let Some(ext) = path.extension() {
                    let filename = match path.file_name() {
                        Some(e) => e,
                        None => {
                            log::error!("Failed to initialize the program");
                            return Err(());
                        }
                    }; 

                    let filename = match filename.to_str() {
                        Some(e) => e,
                        None => {
                            log::error!("Failed to initialize the program, conversion error!");
                            return Err(());
                        }
                    };
                    
                    if ext == consts::DLL_EXTENSION && filename.contains("xpacker_ext_") {
                        log::debug!("Loading {}", filename);
                        unsafe {
                            type PackerCreate = unsafe fn() -> *mut dyn Packer;
                            let lib = Library::new(path.as_os_str()).expect(format!("Unable to load library {}", filename).as_str());

                            // We need to keep the lib around otherwise our plugin's pointer will
                            // point to garbage. this makes sure the lib doesn't end up getting dealocated.
                            self.loaded_libraries.push(lib);
                    
                            let lib = self.loaded_libraries.last().unwrap();
                    
                            let constructor: Symbol<PackerCreate> = match lib.get(b"__xpacker__internals__create_plugin") {
                                Ok(e ) => e,
                                Err(e) => {
                                    log::error!("Error occured while loading extension, is the bootstrapper defined");
                                    log::debug!("{:#?}", e);
                                    return Err(());
                                }
                            };
                            let boxed_raw = constructor();
                    
                            let plugin = Box::from_raw(boxed_raw);
                            log::debug!("Loaded plugin: {}", plugin.name());
                            
                            self.packers.push(plugin);
                        };

                    }
                }
            }
        }
        
        Ok(())
    }

    pub fn unload(&self) {
        log::debug!("Unloading packers");

        for packer in &self.packers {
            // log::debug!("Firing on_plugin_unload for {}", packer.name());
            drop(packer)
        }

        for lib in &self.loaded_libraries {
            drop(lib);
        }
    }

}