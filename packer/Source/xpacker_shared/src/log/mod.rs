#[macro_use]
pub mod macros;
pub use crate::{error, info, warn, trace, debug};

use std::{fmt::Display, io::{stdout}, process::exit, sync::{Mutex}};

use crossterm::{cursor::{MoveTo, Show, position}, execute, style::{Print, Stylize}, terminal::{size, Clear, ClearType}};

// pub use macros::{};
// pub use log::{info, error, warn, trace, debug, LevelFilter, Level};
use lazy_static::lazy_static;
lazy_static! {
    static ref LAST_LOGLEVEL: Mutex<String> = Mutex::new(String::from("NONE"));
    static ref MAX_LEVEL: Mutex<Level> = Mutex::new(Level::Debug);
    pub static ref LOGGER: Mutex<Logger> = Mutex::new(Logger::new());
}   


// check if it is a progress line, if yes, clears it
fn check_line(level: &String) {
    if level.eq("PROGRESS") {
        let (_, row) = position().unwrap_or((50, 50));

        execute!(
            stdout(), 
            Clear(ClearType::CurrentLine), 
            MoveTo(0, row), 
        ).expect("Error executing terminal command");
    }
}

pub fn done() {
    execute!(stdout(), Show).expect("Error executing terminal command");
}

#[derive(PartialEq)]
pub enum Level {
    Trace = 0,
    Debug,
    Info,
    Warn,
    Error,
}

impl Display for Level {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Level::Trace => write!(f, "Trace"),
            Level::Debug => write!(f, "Debug"),
            Level::Info =>  write!(f, "Info"),
            Level::Warn =>  write!(f, "Warn"),
            Level::Error => write!(f, "Error"),
        }
    }
}

pub struct Logger;

impl Logger {

    pub fn new() -> Self {
        Logger {}
    }

    pub fn log(&self, level: Level, message: String) {
        let max_level = match MAX_LEVEL.lock() {
            Ok(e) => e,
            Err(e) => {
                println!("Error initializing logger!");
                println!("{:#?}", e);
                exit(-1);
            }
        };
        
        if *max_level == Level::Debug {
            let mut last_level = LAST_LOGLEVEL.lock().unwrap();
            
            // clear the line if necessary
            check_line(&*last_level);
            *last_level = format!("{}", level);
            
            let message = match level {
                Level::Debug    => format!("   {} {}", "Debu".magenta(),    message),
                Level::Error    => format!("   {} {}", "Erro".red(),        message),
                Level::Info     => format!("   {} {}", "Info".blue(),       message),
                Level::Trace    => format!("   {} {}", "Trac".dark_grey(),  message),
                Level::Warn     => format!("   {} {}", "Warn".yellow(),     message),
            };

            println!("{}", message);
        }

        else if (*max_level) == Level::Info {
            let mut last_level = LAST_LOGLEVEL.lock().unwrap();
            
            // clear the line if necessary
            check_line(&*last_level);
            *last_level = format!("{}", level);
            
            let (message, enable) = match level {
                Level::Error    => (format!("   {} {}", "Erro".red(),        message), true),
                Level::Info     => (format!("   {} {}", "Info".blue(),       message), true),
                Level::Trace    => (format!("   {} {}", "Trac".dark_grey(),  message), true),
                Level::Warn     => (format!("   {} {}", "Warn".yellow(),     message), true),
                _ => ("".to_string(), false)
            };

            if enable {
                println!("{}", message);
            }
        }
    }
}

pub fn init(verbose: bool) {
    let mut level = match MAX_LEVEL.lock() {
        Ok(e) => e,
        Err(e) => {
            println!("Error initializing logger!");
            println!("{:#?}", e);
            exit(-1);
        }
    };

    if verbose {
        *level = Level::Debug;
    }
    else {
        *level = Level::Info;
    }

    let mut logger = match LOGGER.lock() {
        Ok(e) => e,
        Err(e) => {
            println!("Error initializing logger! Lock failed");
            println!("{:#?}", e);
            exit(-1);
        }
    };

    *logger = Logger::new();
}

pub fn prog_done(message: String) {
    let mut level = LAST_LOGLEVEL.lock().unwrap();
    
    // clears the line if necessary
    check_line(&level);

    *level = "PROGRESS_DONE".to_string();

    println!("   {} {}!", "Prog".green(), message);
}

pub fn prog(remaning: u32, total: u32, message: String) {
    let mut percentege = remaning as f64 / total as f64;
    let mut level = LAST_LOGLEVEL.lock().unwrap();
    
    if remaning == total {
        // done 
        prog_done(message);
        return;
    }
    
    // clears the line if necessary
    check_line(&level);
    let ci =  match std::env::var("CI") {
        Ok(e) => e,
        Err(_) => String::new(),
    };

    if !ci.is_empty() {
        println!("   {} {}", format!("Prog [{}/{}]", remaning, total).green(), message);
        *level = "PROGRESS_CI".to_string();
        return;
    }
    else {
        let (width, _) = size().unwrap_or((50, 50));
        let width = width as f64 / 3.0;
        percentege *= width;// ajust the percente to the size
        let bar = {
            let width = width as i32;
            let percentege = percentege as i32;
            let mut _bar = String::new();
            for i in 0..(width+1) {
                if i < percentege {
                    _bar += "█";
                }
                else {
                    _bar += "░";
                }
            }
            _bar
        };

        // print to screen
        *level = "PROGRESS".to_string();
        execute!(stdout(), Print(format!("   {} {}|{}", "Prog".green(), bar.green(), message))).expect("Error executing terminal command");
    }
    
}


pub fn h(e: String) {
    crate::error!("Hello {}", e)
}