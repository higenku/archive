use std::{env};
use xpacker_shared::{Arguments, log};
mod commands;
mod extension;
mod pack;
mod registry;


fn main() {
    println!("Higenku Packing Engine v{}", env!("CARGO_PKG_VERSION"));
    println!("Copyright to Higenku Developers and Contributors");
    println!("Source code available at https://gitlab.com/higenku/tools/packer/\n");

    let args: Vec<String> = env::args().collect(); 
    let mut args = Arguments::new(args);
    // let _exe = args.get_next().expect("Executable not found");


    // verbose output
    log::init(args.contains("-V"));

    // change the current working directory
    if args.contains("-C") {
        let newcur = args.get_next_to("-C").expect("When using -C, it should be formatted as -C [Directory]");
        std::env::set_current_dir(newcur.clone()).expect("Failed to change directory, are you sure it exists?");
        let remove_arg = args.get_idx("-C").unwrap();
        let next_arg = args.get_idx(&newcur).unwrap();
    
        args.remove_idx(remove_arg).unwrap();
        args.remove_idx(next_arg -1).unwrap();
    }


    let cmd = args.get_next();
    if cmd == None {
        log::error!("Unrecognized Command \"\", use -h for help");
        return;
    }

    let cmd = cmd.expect("Program corrupted");
    match cmd.as_str() {
        "pack"      | "p" => commands::pack(args), 
        "gen"       | "g" => commands::gen(args), 
        "unpack"    | "u" => commands::unpack(args),
        "ext"       | "e" => commands::ext(args),
        "reg"       | "r" => commands::reg(args),
        "clean"           => commands::clean(),

        "--help"    | "-h" => commands::help(), 
        "help"      | "h" => commands::help(), 

        _ => log::error!("Unrecognized Command \"{}\", use -h for help", cmd),
    }
}
