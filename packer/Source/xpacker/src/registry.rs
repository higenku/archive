use std::{path::Path, fs::File, io::Read, str::FromStr};
use regex::Regex;
use serde_json::{json, Value};
use sha2::Digest;
use xpacker_shared::{Arguments, log, PkgSpec};

pub fn gitlab(args: Arguments, spec: PkgSpec) {
    let mut args = args;
    let token = args.get_next_to("-a");
    let api = args.get_next_to("-api").unwrap_or("gitlab.com".to_owned());
    
    if let Some(token) = token {
        if !Path::new(".xpkg").exists() { log::error!("The application needs to be packed first"); return; }
            
        let proj_path = match url::Url::parse(&spec.repo){
            Ok(e) => e,
            Err(e) => {
                log::error!("invalid url {}", spec.repo);
                log::error!("{:#?}", e);
                return;
            },
        };
        let proj_path = proj_path.path()[1..].to_string();
        let proj_path = proj_path.replace("/", "%2F");
        
        for target in &spec.targets {
            log::info!("Pushing Target: {}", target.sys);
            let mut file = match File::open(format!(".xpkg/{}__{}__{}.xpkg", spec.id, target.sys, spec.version)) {
                Ok(e) => e,
                Err(e) => {
                    log::error!("It looks that there is no file for the target {}, did the packing of your app occur correctly?", target.sys);
                    log::error!("{:#?}", e);
                    return;
                },
            };
            let mut data = Vec::new();
            match file.read_to_end(&mut data) {
                Ok(_) => {},
                Err(e) => {
                    log::error!("An error occured while processing target {}", target.sys);
                    log::error!("{:#?}", e);
                    return;
                },
            };
            // Pushing to gitlab :)
            let url= format!("https://{}/api/v4/projects/{}/packages/generic/{}/{}/{}", api, proj_path, spec.id, spec.version, format!("{}__{}__{}.xpkg", spec.id, target.sys, spec.version));

            log::debug!("Using Url {}", url);
            match ureq::put(&url)
                .set("PRIVATE-TOKEN", &token)
                .send_bytes(data.as_ref()) 
            {
                Ok(e) => log::debug!("{:#?}", e),
                Err(e) => {
                    log::error!("An error occured while pushing target {}", target.sys);
                    log::error!("{:#?}", e);
                    return;
                },
            };
        }

        log::info!("Pushing manifest");
        let mut file = match File::open(".xpkg/manifest.json") {
            Ok(e) => e,
            Err(e) => {
                log::error!("Error Loading .xpkg/manifest.json");
                log::error!("{}", e);
                return;
            },
        };

        // Manifest

        let mut data = String::new();
        match file.read_to_string(&mut data) {
            Ok(_) => {},
            Err(e) => {
                log::error!("An error occured while processing manifest");
                log::error!("{:#?}", e);
                return;
            },
        };

        let url= format!("https://{}/api/v4/projects/{}/packages/generic/{}/{}/{}", api, proj_path, spec.id, spec.version, "manifest.json");
        log::debug!("using url {}", url);
        
        // Pushing manifest
        match ureq::put(&url)
            .set("PRIVATE-TOKEN", &token)
            .send_string(&data) 
        {
            Ok(e) => log::debug!("{:#?}", e),
            Err(e) => {
                log::error!("An error occured while pushing manifest");
                log::error!("{:#?}", e);
                return;
            },
        };

    } else { log::error!("A token is needed to push applications \n\tUse -a [TOKEB]")}
}



pub fn oci(args: Arguments, spec: PkgSpec) {

    fn get_token(realm: &str, service: &str, repository: &str, basic_auth: &str) -> Option<String> {
        let url = format!("{}?service={}&scope=repository:{}:pull,push", realm, service, repository);
        let res = match ureq::get(&url)
            .set("Authorization", &basic_auth)
            .call() {
                Ok(e) => e,
                Err(e) => {
                    log::error!("Failed to get token");
                    log::debug!("{:?}", e);
                    return None;
                },
            };
        
        if !res.has("Content-Length") {
            log::error!("Failed to get Content-Length");
            return None;
        }
        let len: usize = res.header("Content-Length")
            .unwrap()
            .parse().unwrap();
        
        let mut bytes: Vec<u8> = Vec::with_capacity(len);
        res.into_reader()
            .take(10_000_000)
            .read_to_end(&mut bytes).unwrap();
        
        let json = match String::from_utf8(bytes) {
            Ok(e) => e,
            Err(_) => {
                log::error!("Failed to get content");
                return None;
            },
        };
        let value = match serde_json::Value::from_str(&json) {
            Ok(e) => e,
            Err(_) => {
                log::error!("Failed to parse the response: {}", json);
                log::debug!("DO NOT SHARE:\n {}", json);
                return None;
            },
        };
        
        let value = match value["token"].as_str() {
            Some(e) => e.to_string(),
            None => {
                log::error!("Failed to get token from response");
                log::debug!("DO NOT SHARE:\n {:?}", value);
                return None;
            },
        };

        log::debug!("Got token: {}", &value);
        Some(value)
    }

    fn push_manifest(api: &str, name: &str, manifest: &str, token: &str, version: &str) {
        let url = format!("https://{}/v2/{}/manifests/{}", api, name, version);
        log::debug!("using url {}", url);
        match ureq::put(&url)
            .set("Authorization", &format!("Bearer {}", token))
            .set("Content-Type", "application/vnd.oci.image.manifest.v1+json")
            .send_string(&format!("{}", manifest)) 
        {
            Ok(_) => {
                log::info!("Blob Pushed")
            },
            Err(_) => {
                log::error!("Failed to push manifest to registry");
                std::process::exit(1);
            },
        };
    }
    
    fn push_blob(spec: &PkgSpec, api: &str, name: &str, target: &str, token: &str, manifest: &mut Value) {
        let mut file = match File::open(format!(".xpkg/{}__{}__{}.xpkg", spec.id, target, spec.version)) {
            Ok(e) => e,
            Err(e) => {
                log::error!("It looks that there is no file for the target {}, did the packing of your app occur correctly?", target);
                log::error!("{:#?}", e);
                return;
            },
        };

        let filesize = file.metadata().expect("Unknow system behaviour, start again").len();

        let mut buf = Vec::new();
        match file.read_to_end(&mut buf) {
            Ok(_) => {},
            Err(e) => {
                log::error!("Failed to read file");
                log::error!("{:?}", e);
                std::process::exit(1);
            },
        };

        let mut hash = sha2::Sha256::new();
        hash.update(&buf);
        let hash = hash.finalize().to_vec();
        let hash = hex::encode(hash);

        let url = format!("https://{}/v2/{}/blobs/uploads/?digest=sha256:{}", api, name, hash);
        match ureq::post(&url)
            .set("Authorization", &format!("Bearer {}", token))
            .send_bytes(&buf) 
        {
            Ok(_) => log::info!("Pushed {} successfully", target),
            Err(e) => {
                log::error!("Error Pushing {}", target);
                log::debug!("{}", e);
                return;
            },
        };
        
        // Config
        manifest["layers"].as_array_mut().unwrap().push(json!({
            "mediaType": "application/higenku.pkg.v1",
            "size": filesize,
            "digest": format!("sha256:{}", &hash)
        }));

        manifest["annotations"].as_object_mut().unwrap().insert(format!("org.higenku.{}", target), Value::String(hash));
    }


    let mut args = args;
    let api = args.get_next_to("-api").unwrap_or("ghcr.io".to_string());
    let basic_auth = args.get_next_to("-a").unwrap_or("".to_string());
    let basic_auth = format!("Basic {}", basic_auth);

    let name = args.get_next_to("-n").unwrap_or(spec.id.replace(".", "/"));


    log::debug!("Api: {}", api);
    log::debug!("Auth: {}", basic_auth);
    log::debug!("name: {}", name);
    // get auth token :)

    let url = format!("https://{}/v2/", api);
    log::debug!("Using url: {}", url);
    let (realm, service) = match ureq::get(&url).call() {
        Ok(_) => {
            log::error!("Error getting www-athenticate");
            return
        },
        Err(e) => {
            let res = match e.into_response() {
                Some(e) => e,
                None =>  {
                    log::debug!("Error in Response");
                    log::error!("Error getting www-athenticate");
                    return;
                },
            };
            let header = match res.header("WWW-Authenticate") {
                Some(e) => e.to_string(),
                None =>  {
                    log::debug!("Error in Header");
                    log::error!("Error getting www-athenticate");
                    return;
                },
            };

            log::debug!("Www-Authenticate: {}", &header);

            let reg = Regex::new("realm=\"(?P<realm>.*)\",service=\"(?P<service>.*)\"").unwrap();
            let matches = reg.captures(&header).unwrap();
            
            let realm   = matches.name("realm").unwrap().as_str();
            let service = matches.name("service").unwrap().as_str();

            log::debug!("{}", realm);
            log::debug!("{}", service);

            (realm.to_string(), service.to_string())
        },
    };

    let mut manifest = json!({
        "schemaVersion": 2 as i32,
        "layers": [],
        "annotations": {}
    });

    if let Some(token) = get_token(&realm, &service, &name, &basic_auth) {
        for target in &spec.targets {
            log::info!("Pushing {}", target.sys);
            push_blob(&spec, &api, &name, &target.sys, &token, &mut manifest);
        }

        // Package Manifest
        let pkgmanifest = match std::fs::read_to_string(".xpkg/manifest.json") {
            Ok(e) => e,
            Err(_) => {
                log::error!("Failed to read .xpkg/manifest.json");
                return
            },
        };
        manifest["annotations"].as_object_mut().unwrap().insert("org.higenku.manifest".to_string(), Value::String(pkgmanifest));

        push_manifest(&api, &name, &format!("{}", manifest), &token, "latest");
        push_manifest(&api, &name, &format!("{}", manifest), &token, &spec.version);
        log::info!("Pushed Manifest");
    }
    else {
        log::error!("Failed to get token, thus cannot create package");
        return
    }
    
}