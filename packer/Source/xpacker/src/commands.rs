
use std::{path::Path};

use crate::{
    pack,
    extension
};
use xpacker_shared::{Arguments, CompressionManager, PackerManager, log, PkgSpec};


/// help command
pub fn help() {
    println!("All Commands:");
    println!("  gen, g      for generating pkg.yml");
    println!("  pack, p     for packing applications");
    println!("  unpack, u   for unpacking final archives");
    println!("  reg         for pushing to registries (OCI and Gitlab)");
    println!("  ext, e      for extension commands");
    println!("  clean       for cleaning the project build");
    println!("  --help, -h  for help command");
    println!("  -V          for verbose output");
    println!("  -C          set the current directory");
    println!();
    println!("Usage: ");
    println!("  xpacker [COMMAND] [OPTIONS]");
    println!("  xpacker -C [DIRECTORY] [COMMAND] [OPTIONS]");
}

pub fn gen(args: Arguments) {
    let mut args = args;

    let mut manager = PackerManager::new();
    match manager.load() {
        Ok(_) => {},
        Err(_) => {
            log::error!("Error Loading packers");
            return;
        }
    };

    if let Some(genname)  = args.get_next() {
        if let Some(generator) = manager.get_packer(&genname) {
            let curdir = std::env::current_dir().unwrap().as_path().to_str().unwrap().to_string();
            let spec = match PkgSpec::load("pkg.yml") {
                Ok(e) => {
                    log::warn!("Spec already Defined, we will be talking existing configuration as base and then work on it");
                    e
                }
                Err(_) => PkgSpec::default(args.contains("-iscripts")),
            };

            match generator.generate(&curdir, &mut args, spec) {
                Ok(spec) => {
                    log::info!("pkg.yml generation was successful, writing file");
                    spec.write(&"./".to_string());
                },
                Err ((msg, err)) => {
                    log::error!("{}", msg);
                    
                    if let Some(err) = err {
                        log::error!("{}", err.to_string());
                    }
                    return;
                }
            };
        }
        else { log::error!("Generator not found {} ", genname); }
    }
    else { log::error!("No generator specified"); }
}


/// pack command
pub fn pack(args: Arguments) {
    pack::pack(args);
}

pub fn clean() {
    if Path::new(".xpkg").exists() {
        if let Err(e) = std::fs::remove_dir_all(Path::new(".xpkg")) {
            log::error!("Unable to clean '.xpkg' directory");
            log::error!("{:#?}", e);
        }
    }
    else {
        log::info!("Cleaned the '.xpkg' folder");
    }
}

/// unpack command
pub fn unpack(args: Arguments) {
    let mut args = args;
    while let Some(file) = args.get_next() {
        if file == "--help" || file == "-h" {
            println!("Utility to unpack xpkgs (company.appName__system__version.xpkg):");
            println!("  Use this functionality to unpack xpkgs that were packed with the packer");
            println!("Usage: ");
            println!("  xpacker u [...files]");
            println!("  xpacker -C [DIRECTORY] u [...files]");
            return;
        }

        log::info!("Unpacking {}", file);
        let outfile = file.replace(".xpkg/", "").replace(".xpkg", "");
        CompressionManager::decompress(&file, &outfile);
    }
    
}

/// extension command
pub fn ext(args: Arguments) {
    let mut args = args;

    let mut mgr = PackerManager::new();
    match mgr.load() {
        Ok(_) => {},
        Err(_) => {
            log::error!("Error Loading packers");
            return;
        }
    };

    match args.get_next() {
        Some(arg) => {
            match arg.as_str() {
                "ls" | "list" => extension::list(mgr),
                _ => log::error!("command not found"),
            }
        },
        None => {
            log::error!("You must specify another option for extensions command")
        }
    }
}

/// Registry Integration
pub fn reg(args: Arguments) {
    let mut args = args;

    let cmd = args.get_next();
    if let Some(cmd) = cmd {
        match cmd.as_str() {
            "push" | "p" => {

                let registry = match args.get_next_to("-t") {
                    Some(e) => e,
                    None => {
                        log::error!("Must Contain a registry type! How are we supposed to know where you want to put it");
                        return;
                    }
                };
                let spec = match PkgSpec::load("pkg.yml") {
                    Ok(e) => e,
                    Err(e) => {
                        log::error!("Error Loading pkg.yml");
                        log::error!("{}", e);
                        return;
                    },
                };
    

                if registry == "gitlab".to_string() {
                    crate::registry::gitlab(args, spec);
                }
                
                else if registry == "oci".to_string() {
                    crate::registry::oci(args, spec);
                }

                else {
                    log::error!("Target not supported, use gitlab, oci");
                }
            }
            "--help" | "-h" => {
                println!("Utility to push to gitlab package registry");
                println!("  Use this functionality to push you packages to gitlab registry");
                println!("  -a   [TOKEN]  for providing the access token to the registry");
                println!("  -api [URL]    for providing the url of the registry");
                println!("  -n   [NAME]   for a custom name prefix (only OCI)");
                println!("  -t   [TYPE]   for a specifying the registry type:");
                println!("                   can only be: gitlab oci");

                println!("\nUsage: ");
                println!("  xpacker reg p -a $GITLAB_TOKEN");
                println!("  xpacker reg p -a $GITLAB_TOKEN -api gitlab.example.com");
                println!("  xpacker -C [DIRECTORY] reg p -a $GITLAB_TOKEN");
                return;
            }
            _ => log::error!("command not found")
        }
    }
    else {
        log::error!("You must specify another option for regigistry command")
    }
}