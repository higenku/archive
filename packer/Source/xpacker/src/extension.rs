use xpacker_shared::PackerManager;

pub fn list(mgr: PackerManager) {
    println!("Packers: ");
    for i in &mgr.packers {
        println!("{} v{}", i.name(), i.version());
    }
}