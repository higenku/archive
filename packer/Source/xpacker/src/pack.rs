use std::{fs, path::Path};
use xpacker_shared::{Arguments, CompressionManager, PackerManager, log, Manifest, PkgSpec, PkgManifest};

pub fn pack(args: Arguments) {

    let mut manager = PackerManager::new();
    match manager.load() {
        Err(_) => {
            return;
        }
        Ok(_) => {},
    };
    
    let curdir = std::env::current_dir().unwrap();
    let curdir = Path::new(&curdir);

    let spec = curdir.join("pkg.yml");
    let spec = spec.as_path();

    let spec = match PkgSpec::load(spec.to_str().unwrap()) {
        Err(e) => {
            log::error!("Error Loading pkg.yml");
            log::error!("{}", e);
            return;
        },
        Ok(spec) => spec,
    };

    let packer = match manager.get_packer(&spec.package.name) {
        None => {
            log::error!("packer not found {}", &spec.package.name);
            return;
        }
        Some(e) => e
    };

    for target in &spec.targets {
        if !Path::new(&target.dir).exists() {
            if let Err(e) = std::fs::create_dir_all(&target.dir) {
                log::error!("Failed to create directory .xpkg, maybe the application doesn't have enought permissions");
                log::error!("{:#?}", e);
                return;
            }
        }
    }

    let curdir = match curdir.to_str() {
        Some(e) => e,
        None => {
            log::error!("error Getting file \"pkg.yml\"");
            return;
        }
    };
    
    match packer.pack(&curdir.to_string(), &spec, &args) {
        Ok(_) => {},
        Err ((msg, err)) => {
            log::error!("{}", msg);
            
            if let Some(err) = err {
                log::error!("{}", err.to_string());
            }
            return;
        }

    };
    // Final Touches
    for target in &spec.targets {

        let scripts = &spec.scripts;
        if let Some(scripts_path) = scripts {
            log::debug!("Writing scripts");
            let contents = match fs::read_to_string(scripts_path) {
                Ok(e) => e,
                Err(e) => {
                    log::error!("Erro while reading script '{}'", scripts_path);
                    log::error!("{:#?}", e);
                    return;
                }
            };

            if !Path::new(&format!("{}/scripts.sh", target.dir)).exists() {
                if let Err(e) = fs::create_dir_all(format!("{}/scripts", target.dir)) {
                    log::error!("Error creating {}/scripts", target.dir);
                    log::error!("{:#?}", e);
                    return;
                }
            }

            if let Err(e) = fs::write(format!("{}/scripts.sh", target.dir), contents) {
                log::error!("Erro while writing script {}", scripts_path);
                log::error!("{:#?}", e);
                return;
            }
        }

        
        if let Some(include_files) = &spec.include_files {
            log::info!("Copying include files");
            for pat in include_files {
                let files = match glob::glob(pat) {
                    Ok(e) => e,
                    Err(e) => {
                        log::error!("Error to get files with pattern \"{}\"", pat);
                        log::error!("{:#?}", e);
                        return
                    }
                };
    
                for file in files {
                    if let Ok(file) = file {
                        let fname = file.as_path().to_string_lossy().to_string();
                        let fpath = Path::new(&fname);
                        let outpath = format!(".xpkg/{}/app/{}", target.sys, fname);
                        let dirname = match Path::new(&outpath).parent() {
                            Some(e) => e,
                            None => {
                                log::error!("error occured while copying file \"{}\"", fname);
                                log::error!("Coulnd't get parrent");
                                return;
                            }
                        };
    
                        if !dirname.exists() {
                            match fs::create_dir_all(dirname) {
                                Ok(_) => {},
                                Err(e) => {
                                    log::error!("error while creating directory \"{}\"", dirname.display());
                                    log::debug!("{:#?}", e);
                                    return;
                                }
                            };
                        }
    
                        if fpath.is_dir() {
                            match fs::create_dir_all(fpath) {
                                Ok(_) => {},
                                Err(e) => {
                                    log::error!("error while creating directory \"{}\"", dirname.display());
                                    log::error!("{:#?}", e);
                                    return;
                                }
                            };
                        }
    
    
                        if let Err(e) = fs::copy(&fname, outpath) {
                            log::error!("error occured while copying file \"{}\"", fname);
                            log::error!("{:#?}", e);
                            return;
                        }
                    }
                }            
            } // end copy files
        }

        log::debug!("writting manifest");

        let mut manifest = match PkgManifest::from_pkgspec(&spec){
            Ok(e) => e,
            Err(e) => {
                log::error!("An Error Occured while getting manifest");
                log::error!("{}", e);
                return;
            }
        };

        manifest.getfiles(target.dir.clone());

        match manifest.write(format!("{}/manifest.json", target.dir)) {
            Ok(_) => {},
            Err(e) => {
                log::error!("An Error Occured while writting manifest.json");
                log::error!("{}", e);
                return;    
            }
        };

        // License
        if let Some(license) = &spec.license {
            if Path::new(license).exists() {
                if let Err(e) = fs::copy(license, Path::new(&target.dir).join("LICENSE")) {
                    log::error!("Error Occured while copying the license file");
                    log::error!("{}", e);
                    return;
                }
            }
        }


        log::info!("Packing {} from {}", target.sys, target.dir);
        let mut root = target.dir.clone();
        let sys = target.sys.clone();
        match CompressionManager::compress_folder(&target.dir, &mut root, &format!(".xpkg/{}__{}__{}.xpkg", &spec.id, target.sys, &spec.version), &sys) {
            Ok(_) => {},
            Err(e) => {
                log::error!("An Error Occured while packing the final package");
                log::error!("{}", e);
                return;
            }
        };
    }

    // Root Manifest
    let manifest = match Manifest::new(spec) {
        Err(e) => {
            log::error!("Error Generating root manifest");
            log::error!("{}", e);
            return;
        }
        Ok(e) => e,
    };
    
    match manifest.write() {
        Ok(_) => {},
        Err(e) => {
            log::error!("Error writting root manifest");
            log::error!("{}", e);
            return;        
        }
    };

}