use xpacker_shared::{Packer, declare_packer, log, PkgSpec, Arguments};
use std::{path::Path, process::{Command, exit, Stdio}};


mod utils;
use utils::{build_proj, get_toolchain};

use crate::utils::copy_files;

pub struct RustPacker;

impl RustPacker {
    pub fn new() -> Self {
        RustPacker {}
    }
}

impl Packer for RustPacker {
    fn name(&self) -> String {
        "rust".to_string()
    }

    fn version(&self) -> String {
        env!("CARGO_PKG_VERSION").to_string()
    }

    fn pack(&self, _: &String, spec: &PkgSpec, _: &Arguments) -> Result<(), (std::string::String, std::option::Option<std::string::String>)> {
        // check rustc version
        let mut spec = spec.clone();
        let runtime_version = match Command::new("rustc").arg("-V").stdout(Stdio::piped()).spawn() {
            Ok(e ) => {
                let out = match e.wait_with_output() {
                    Ok(e) => e,
                    Err(_) => {
                        log::error!("Error running \"rustc -V\"");
                        log::warn!("Skipping Rust version check");
                        return Err((format!("No runtime version found"), None));
                    }
                };
                out
            },
            Err(_) => {
                log::error!("Error running \"rustc -V\"");
                log::warn!("Skipping Rust version check");
                return Err((format!("No runtime version found"), None));
            }
        };

        let runtime_version: String = match String::from_utf8(runtime_version.stdout) {
            Ok(e ) => {
                let e: Vec<&str> = e.split(" ").collect();
                e[1].to_string()
            },
            Err(e) => {
                log::debug!("Error running \"rustc -V\"");
                return Err( 
                    ("Error getting rust version".to_string(), Some(e.to_string()))
                );        
            }
        };

        
        if let Some(dependencies) = &mut spec.dependencies {
            let mut pos = 0;
            let mut needs_update = false;
            for dependency in dependencies.into_iter() {
                if dependency.starts_with("ruststd:") {
                    let version = dependency.replace("ruststd:", "");
                    if version != runtime_version {
                        log::warn!("The rustd dependency has a different version than the one you are using, we are changing it in your pkg.yml");
                        log::warn!("The latest version found is {} and the one you defined is {}", runtime_version, version);
                        needs_update = true;
                        // dependencies.remove(pos);
                        break;
                    }
                }

                pos += 1;
            };

            if needs_update {
                dependencies.remove(pos);
                dependencies.insert(pos, format!("ruststd:{}", runtime_version));
                spec.write(&format!("."));
                log::info!("pkg.yml update successfully");
            }
            
        }
        else {
            log::error!("No ruststd dependency found, you need to run the generator again or look at the dependency yourself")
        }


        let extra_envs = if spec.package.args.get("env").is_some() { spec.package.args["env"].clone()  } else {format!(" ")};

        let mut targets = Vec::new();
        for target in &spec.targets {
            
            let flags = match target.sys.as_str() {

                "linux_amd64" => spec.package.args.get("linux_flags").unwrap_or(&"".to_string()).clone(),
                "macos_amd64" => spec.package.args.get("macos_flags").unwrap_or(&"".to_string()).clone(),    
                "win_amd64" => spec.package.args.get("win_flags").unwrap_or(&"".to_string()).clone(),
                                
                "linux_aarch64" => spec.package.args.get("linux_flags_aarch64").unwrap_or(&"".to_string()).clone(),
                "macos_aarch64" => spec.package.args.get("macos_flags_aarch64").unwrap_or(&"".to_string()).clone(),
                _ => {
                    log::error!("System not found");
                    exit(-1);
                }
            };
            
            targets.push((target.clone(), flags));
        }
        if spec.package.entry.is_none() || spec.package.entry.as_ref().unwrap_or(&"".to_string()) == "#YOU NEED TO PUT THE EXECUTABLE NAME HERE" {
            log::error!("Error Getting package.entry, is it defined?");
            return Err((format!("package.entry was either null or couldn't be deserialized by our app"), None));
        }

        let mut script = format!("");
        let compilers = format!(" {} {} {} {} {} {} {} {} {} {} ",
            // Aarch64 Macos
            "CC_aarch64_apple_darwin=o64h-clang",        
            "AR_aarch64_apple_darwin=aarch64-apple-darwin20.4-ar",
            
            // x86_64 Macos
            "CC_x86_64_apple_darwin=o64-clang",        
            "AR_x86_64_apple_darwin=x86_64-apple-darwin20.4-ar",

            // x86_64 Windows
            "CC_x86_64_pc_windows_gnu=x86_64-w64-mingw32-gcc",
            "AR_x86_64_pc_windows_gnu=x86_64-w64-mingw32-gcc-ar",
            
            // x86_64 linux
            "CC_x86_64_pc_windows_gnu=clang",
            "AR_x86_64_pc_windows_gnu=ar",

            // Aarch linux
            "CC_x86_64_unknown_linux_gnu=clang",
            "AR_x86_64_unknown_linux_gnu=aarch64-linux-gnu-ar ",
        );
        let base_path = format!(".xpkg/tmp");
        
        for (target, flags) in &targets {
            let toolchain= match get_toolchain(&target.sys) {
                Some(e) => e,
                None => {
                    log::error!("Target not found {:?}", target);
                    exit(-1);
                }
            };
            // add the target
            script.push_str(&format!("echo \"Building for {}\"\n", toolchain));
            script.push_str(&format!("rustup target add {} \n", toolchain));

            // CARGO_BUILD_RUSTFLAGS RUSTFLAGS
            script.push_str(&format!("RUSTFLAGS=\"-C prefer-dynamic {}\" ", flags));    
            script.push_str(&format!("CARGO_TARGET_DIR=\"{}\" ", base_path));
            script.push_str(&extra_envs);
            script.push_str(&compilers);
            // script.push_str(&format!(" __CARGO_DEFAULT_LIB_METADATA=0 cargo build --release --target {} || exit -1\n\n", toolchain));
            script.push_str(&format!(" cargo build --release --target {} || exit -1\n\n", toolchain));
        }

        script.push_str("echo 'DONE'");
        let container_runtime = 
            if spec.package.args.get("container_runtime").is_some() { spec.package.args["container_runtime"].clone() }
            else {format!("podman")};

        // builds the poject
        build_proj(script, container_runtime);

        // copy all files
        log::info!("Copying files");
        for (target, _) in &targets {
            let toolchain = get_toolchain(&target.sys).unwrap(); // we already got it once so we don't need the match statement again :)
            
            log::debug!("Removing directories");
            std::fs::remove_dir_all(format!(".xpkg/tmp/{}/release/.fingerprint", &toolchain)).expect("Counldn't remove directory");
            std::fs::remove_dir_all(format!(".xpkg/tmp/{}/release/build", &toolchain)).expect("Counldn't remove directory");
            std::fs::remove_dir_all(format!(".xpkg/tmp/{}/release/examples", &toolchain)).expect("Counldn't remove directory");
            std::fs::remove_dir_all(format!(".xpkg/tmp/{}/release/incremental", &toolchain)).expect("Counldn't remove directory");
            log::debug!("Removed directories");

            match target.sys.as_ref() {
                // Linux
                "linux_amd64" | "linux_aarch64" => {
                    copy_files(format!("{}/{}/release", &base_path, &toolchain), |filename| {
                        let path = Path::new(&filename);
                        (!path.is_dir() && path.extension().is_none() && !filename.ends_with(".cargo-lock")) || (filename.ends_with(".so") && !path.is_dir())
                    },  target.dir.clone());

                    copy_files(format!("{}/{}/release/deps", &base_path, &toolchain), |filename| {
                        let path = Path::new(&filename);
                        filename.ends_with(".so") && path.is_file()
                    },  target.dir.clone());
                },
    
                // Macos
                "macos_amd64" | "macos_aarch64" => {
                    copy_files(format!("{}/{}/release", &base_path, &toolchain), |filename| {
                        let path = Path::new(&filename);
                        (!path.is_dir() && path.extension().is_none() && !filename.ends_with(".cargo-lock")) || (filename.ends_with(".dylib") && !path.is_dir())
                    },  target.dir.clone());

                    copy_files(format!("{}/{}/release/deps", &base_path, &toolchain), |filename| {
                        let path = Path::new(&filename);
                        filename.ends_with(".dylib") && path.is_file()
                    },  target.dir.clone());
                },
    
                // Windows
                "win_amd64" => {
                    copy_files(format!("{}/{}/release", &base_path, &toolchain), |filename| {
                        let ext = match Path::new(filename).extension() {
                            Some(e) => e.to_str().unwrap(),
                            None => "NOTAEXTENSION"
                        }; 
                        log::debug!("{}", filename);
                        filename.ends_with(".dll") || ext.eq("exe") || ext.eq("a") 
                    },  target.dir.clone());
    
                },
    
                // No Os
                _ => {
                    log::error!("Unreachable code reached");
                    exit(-1);
                }
            }

            let ext = if target.sys.contains("win") { ".exe" } else { "" };
            
            if let Some(entry) = &spec.package.entry {
                match std::fs::write(format!("{}/app.sh", target.dir), format!("exec {}{} $@", &entry, ext)) {
                    Ok(_) => {
                        log::info!("Wrote {}/app.sh", target.dir);
                    },
                    Err(e) => {
                        log::error!("{:#?}", e);
                        return Err((format!("Error writing {}", format!("{}/app.sh", target.dir)), None));
                    },
                }
            }
            else {
                return Err(("No entry found cannot write app.sh".to_owned(), None));
            }

        }

        

        Ok(())
    }

    fn prefix(&self) -> Vec<String> {
        vec![
            "rust".to_string()
        ]
    }

    fn generate(&self, dir: &String, _: &mut Arguments, spec: PkgSpec) -> Result<PkgSpec, (std::string::String, std::option::Option<std::string::String>)> {
        let mut spec = spec;

        spec.package.name = "rust".to_string();
        spec.package.entry = Some("#YOU NEED TO PUT THE EXECUTABLE NAME HERE".to_string());
        
        spec.package.args.insert("linux_flags".to_string(), "-C linker=cc -C ar=ar".to_string());
        spec.package.args.insert("linux_flags_aarch64".to_string(), "-C linker=aarch64-linux-gnu-gcc -C ar=aarch64-linux-gnu-ar -C link-arg=-L/usr/aarch64-linux-gnu/lib".to_string());
        
        spec.package.args.insert("macos_flags".to_string(), "-C linker=o64-clang -C ar=x86_64-apple-darwin20.4-ar".to_string());
        spec.package.args.insert("macos_flags_aarch64".to_string(), "-C linker=o64h-clang -C ar=aarch64-apple-darwin20.4-ar".to_string());
        
        spec.package.args.insert("win_flags".to_string(), "-C linker=x86_64-w64-mingw32-gcc -C ar=x86_64-w64-mingw32-gcc-ar".to_string());
        
        spec.package.args.insert("env".to_string(), "FOO=BAR BAR=FOO".to_string());
        
        let runtime_version = match Command::new("rustc").arg("-V").stdout(Stdio::piped()).spawn() {
            Ok(e ) => {
                let out = match e.wait_with_output() {
                    Ok(e) => e,
                    Err(e) => {
                        log::debug!("Error running \"rustc -V\"");
                        return Err( 
                            ("Error getting rust version".to_string(), Some(e.to_string()))
                        );        
                    }
                };
                out
            },
            Err(e) => {
                log::debug!("Error running \"rustc -V\"");
                return Err( 
                    ("Error getting rust version".to_string(), Some(e.to_string()))
                );
            }
        };

        let runtime_version: String = match String::from_utf8(runtime_version.stdout) {
            Ok(e ) => {
                let e: Vec<&str> = e.split(" ").collect();
                e[1].to_string()
            },
            Err(e) => {
                log::debug!("Error running \"rustc -V\"");
                return Err( 
                    ("Error getting rust version".to_string(), Some(e.to_string()))
                );        
            }
        };

        log::warn!("Using rust runtime v{}", &runtime_version);

        let rust_depen = format!("ruststd:{}", runtime_version);
        if let Some(dependencies) = &mut spec.dependencies {
            dependencies.push(rust_depen);
        } else {
            spec.dependencies = Some(vec![rust_depen])
        }

        log::info!("Generation of Specification Successful, see more at {}", Path::new(dir).join("pkg.yml").as_path().to_str().unwrap());
        Ok(spec)
    }
}

declare_packer!(RustPacker, RustPacker::new);