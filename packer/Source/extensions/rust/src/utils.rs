use std::{process::{Command, exit}, path::Path};
use xpacker_shared::{log};

pub fn get_toolchain<S: AsRef<str>>(target: S) -> Option<String> {
    let target = target.as_ref();
    match target {
        // amd64 targets
        "linux_amd64" => Some("x86_64-unknown-linux-gnu".to_string()),
        "macos_amd64" => Some("x86_64-apple-darwin".to_string()),
        "win_amd64" => Some("x86_64-pc-windows-gnu".to_string()),
        
        // aarch64 targets
        "linux_aarch64" => Some("aarch64-unknown-linux-gnu".to_string()),
        "macos_aarch64" => Some("aarch64-apple-darwin".to_string()),
        _ => None
    }
}


pub fn copy_files<S: AsRef<str>>(dir: S, filter: fn(&String) -> bool, outdir: S) {
    let dir = dir.as_ref().to_string();
    let outdir = outdir.as_ref().to_string();

    let entries = match std::fs::read_dir(&dir) {
        Err(e) => {
            log::error!("Error reading {} ", dir);
            log::debug!("{:#?} ", e);
            exit(-1);
        }
        Ok(e) => e,
    };

    for entry in entries {
        if let Ok(entry) = entry {
            let filename = match entry.file_name().to_str() {
                Some(e) => e.to_string(),
                None => {
                    log::error!("Error reading file {:#?}", entry);
                    exit(-1);
                }
            };
            if Path::new(&filename).is_dir() {
                log::debug!("Copying directory {} to {}", &filename, format!("{}/{}", &filename, dir));
                copy_files(&format!("{}/{}", &dir, filename), filter, &format!("{}/{}", &outdir, &filename));
                continue;
            }

            if !Path::new(&format!("{}/app", &outdir)).exists() {
                if std::fs::create_dir_all(format!("{}/app", &outdir)).is_err() {
                    log::error!("Error creating output directory");
                    exit(-1);
                }
            }

            if filter(&format!("{}/{}", dir, filename)) {
                log::debug!("Copying file {} to {}", &filename, &outdir);
                if let Err(e) = std::fs::copy(format!("{}/{}", &dir, &filename), format!("{}/app/{}", &outdir, &filename)) {
                    log::error!("Error Copying file {}", format!("{}/{}", &dir, &filename));
                    log::debug!("{:#?}", e);
                    exit(-1);
                }
            }
        }
        else {
            log::warn!("Error reading entry, your pogram may be corrupted");
            log::warn!("This warning doesn't stop the program");
        }
    }
}

pub fn build_proj<S: AsRef<str>>(script: S, container_runtime: String) {
    let mut script = script.as_ref().to_string();
    let is_inside_container = match std::env::var("__XPACKER_INSIDE_CONTAINER") {
        Ok(e) => !e.is_empty(),
        Err(_) => false,
    };

    let mut cmd;
    if is_inside_container {
        log::warn!("Detected Container runtime");
        cmd = Command::new("sh");
        cmd.arg("-c");
    } 
    else {
        log::info!("Using Container runtime {}", container_runtime);
        cmd = Command::new(container_runtime);

        log::debug!("Current Directory: {}", std::env::current_dir().expect("ERROR GETTING CURRENT DIRECTORY").to_string_lossy());
        cmd.args([
            "run",
            "--rm",
            "-it",
            "-v",
            format!("{}:/source", std::env::current_dir().expect("ERROR GETTING CURRENT DIRECTORY").to_string_lossy()).as_str(),
            "ghcr.io/higenku/bens/rust",
            "sh",
            "-c"
        ]);
        let other = script.clone();
        script = format!("echo \"Building the application with a script from the Higenku Packing Engine :)\"\ncd source && source /root/.cargo/env && \n\n");
        script.push_str(&other);
    }

    log::debug!("Script {}", script);
    cmd.arg(&script);

    let cmd = match cmd.spawn() {
        Ok(cmd) => cmd,
        Err(e) => {
            log::error!("Couldn't start cargo");
            log::debug!("{:#?}", e);
            exit(-1);
        }
    };

    let out = match cmd.wait_with_output() {
        Ok(x) => x,
        Err(e) => {
            log::error!("Couldn't start cargo");
            log::debug!("{:#?}", e);
            exit(-1);
        }
    };

    if out.status.code().unwrap_or(0) != 0 {
        log::error!("Failed to build the project");
        log::debug!("{}", String::from_utf8_lossy(&out.stderr));
        exit(-1);
    }
}