use subprocess::Exec;
use xpacker_shared::log;

pub fn build_with_cmd(script: &String) -> Result<(), String> {
    match Exec::shell(script).join() {
        Ok(e) => {
            if e.success() {
                log::info!("Build Succed");
                return Ok(())
            }
            else {
                log::error!("Build script failed");
                return Err("Build Script failed".to_string())
            }
        }
        Err(e) => {
            log::error!("Error while bootstraping build script");
            return Err(e.to_string());
        }
    }
}