use std::collections::HashMap;

use xpacker_shared::{Packer, PkgSpec, Target, log, declare_packer, Arguments};

use crate::utils::build_with_cmd;
mod utils;

pub struct BuilderPackers;

impl BuilderPackers {
    pub fn new() -> Self {
        BuilderPackers {}
    }
}

impl Packer for BuilderPackers {
    fn name(&self) -> String {
        format!("build")
    }

    fn version(&self) -> String {
        env!("CARGO_PKG_VERSION").to_string()
    }

    fn pack(&self, _: &String, spec: &PkgSpec, _: &xpacker_shared::Arguments) -> Result<(), (std::string::String, std::option::Option<std::string::String>)> {        
        if let Some(script) = spec.package.args.get("buildCmd") {
            if let Err(e) = build_with_cmd(script) {
                return Err((
                    "Error Occured while executing the build command".to_string(),
                    Some(e.to_string()),
                ));
            }
            Ok(())
        }
        else if spec.package.args.iter().position(|e|  {e.0.starts_with("buildCmd")}) != None {
            let no_script = "__XPACKER_SPECIFY###___BUILD_NO_SCRIPT__##ERROR_TRIGGER".to_string(); // to not match any commands on the system
            let buildscrips = vec![
                spec.package.args.get(&"buildCmd_linux_amd64".to_string()).or(Some(&no_script)).unwrap(),
                spec.package.args.get(&"buildCmd_linux_aarch64".to_string()).or(Some(&no_script)).unwrap(),
                spec.package.args.get(&"buildCmd_macos_amd64".to_string()).or(Some(&no_script)).unwrap(),
                spec.package.args.get(&"buildCmd_macos_aarc64".to_string()).or(Some(&no_script)).unwrap(),
                spec.package.args.get(&"buildCmd_win_amd64".to_string()).or(Some(&no_script)).unwrap(),
            ];

            for script in buildscrips {
                if !script.eq(&"NO_SCRIPT".to_string()) {
                    if let Err(e) = build_with_cmd(script) {
                        return Err((
                            "An error occured while executing the build script".to_string(),
                            Some(e.to_string())
                        ));
                    }
                }
            }
            Ok(())
        }
        else {
            log::warn!("If you build your application yourself, please use the custom packer instead");
            return Err((
                "No build script was found".to_string(),
                None
            ))
        }

    }

    fn generate(&self, _: &String, args: &mut Arguments, initial_spec: PkgSpec) -> Result<PkgSpec, (std::string::String, std::option::Option<std::string::String>)> {
        let mut spec = initial_spec;

        spec.package.name = format!("builders");
        spec.package.args = HashMap::new();

        if args.contains("-all") {
            spec.package.args.insert("buildCmd".to_string(),    format!("BUILDER_BUILD_COMMAND_SCRIPT") );   
            spec.targets = vec![
                Target::new("SOME_DIR", "all"),
            ]
        }
        else {
            spec.package.args.insert("buildCmd_linux_amd64".to_string(),    format!("BUILDER_BUILD_SCRIPT") );
            spec.package.args.insert("buildCmd_linux_aarch64".to_string(),  format!("BUILDER_BUILD_SCRIPT") );
            
            spec.package.args.insert("buildCmd_macos_amd64".to_string(),    format!("BUILDER_BUILD_SCRIPT") );
            spec.package.args.insert("buildCmd_macos_aarc64".to_string(),   format!("BUILDER_BUILD_SCRIPT") );
            
            spec.package.args.insert("buildCmd_win_amd64".to_string(),      format!("BUILDER_BUILD_SCRIPT") );
        }

        log::info!("builders generation was successful");
        Ok(spec)
    }

    fn prefix(&self) -> Vec<String> {
        vec![
            format!("build")
        ]
    }
}


declare_packer!(BuilderPackers, BuilderPackers::new);