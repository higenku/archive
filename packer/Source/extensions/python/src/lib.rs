use std::path::Path;

use xpacker_shared::{Packer, PackerResult, declare_packer, log, PkgSpec, Arguments};

const PYTHON_APPSH: &str = r#"#!sh
# Higenku Script Runner for Python Applications
exec PYTHONPATH=lib.zip python app"#;

pub struct PythonPakcer {}

impl PythonPakcer {
    pub fn new() -> Self {
        PythonPakcer {}
    }
}

impl Packer for  PythonPakcer {
    fn name(&self) -> String {
        "python".to_string()
    }

    fn version(&self) -> String {
        todo!()
    }

    fn pack(&self, dir: &String, spec: &xpacker_shared::PkgSpec, args: &xpacker_shared::Arguments) -> PackerResult {
        
        if Path::new("Pipfile").exists() { 
            // generate requirements.txt
        }
        
        if !Path::new("requirements.txt").exists() {
            return Err(("Requires a Pipfile or a requirements.txt".to_string(), None)); 
        }
        

        for target in &spec.targets {
            // Download deps to .xpkg/tmp/{target.sys} 

            // pip download \        
            // --platform {system} --only-binary=:all: \
            // -r ~/{path}/requirements.txt

            // compress deps to {target.dir}/lib.zip

            let mut appsh = PYTHON_APPSH.to_string();

            if let Some(is_zip) = spec.package.args.get("zip") {
                if is_zip == "true" {
                    // pack app to app.zip
                    appsh = format!("{}.zip", appsh);
                }
                else {
                    // pack to app/
                }

                return Ok(())
            }   
            
            // pack app to app/ 

            // write app.sh

            // done!

        }

        Ok(())
    }

    fn generate(&self, _: &String, args: &mut Arguments, initial_spec: PkgSpec) -> Result<PkgSpec, (String, Option<String>)> {
        let mut spec = initial_spec;
        spec.package.name = self.name();
        spec.package.args.insert("zip".to_string(), "false".to_string());

        spec.runtime.name = "python".to_string();
        if !args.contains("--runtime-version") {
            spec.runtime.version = "3".to_string() 
        } 
        else { 
            if let Some(ver) = args.get_next_to("--runtime-version") {
                if ver.starts_with("1") || ver.starts_with("2") {
                    return Err(("Python 1 and 2 are not supported by our build system, therefore cannot be used as a runtime".to_string(), None));
                }

                spec.runtime.version = ver;
            } 
            else {
                log::warn!("No runtime version specified, using 3 as the default");
                spec.runtime.version = "3".to_string();
            }
        };

        Ok(spec)
    }

    fn prefix(&self) -> Vec<String> {
        vec![
            self.name(),
            "py".to_string()
        ]
    }
}

declare_packer!(PythonPakcer, PythonPakcer::new);