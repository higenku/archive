// For building .net applications
use std::{fs, path::Path, process::Command};
use glob::glob;
use xpacker_shared::{PkgSpec, log};

pub fn build_proj(target: &String, output: String, framework: String, spec: &PkgSpec) -> Result<(), String> {
    let target = match target.as_str() {
        "linux_amd64" =>    "linux-x64",
        "linux_aarch64" =>  "linux-arm64",
        
        "win_amd64" =>      "win-x64",
        
        "macos_amd64" =>    "osx-x64",
        "macos_aarch64" => {
            log::warn!("Dotnet 5.0 does not support building for osx arm64, therefore, only x64 builds are allowed by the sdk");
            "osx-arm64"
        },

        "all" => "all",

        e => {
            log::error!("System {} not found", e);
            return Err(format!("System not found {}", e));
        },
    };
    
    let mut cmd = Command::new("dotnet");
    cmd .args([
        "publish",

        // make the command less verbose
        "--nologo",
        "-v",
        "m",

        // optimizations
        "-c",
        "Release",

        // disable Debug objects
        "-p:DebugType=None",

        // for you to specify a different framework
        "--framework",
        framework.as_str(),
        
        // we provide dependencies in the runtime
        "--no-self-contained",
        
        // output
        "-o",
        output.as_str()
    ]);

    if target == "all" {
        log::warn!("We are building this application for all systems")
    }
    else {
        cmd.arg("-r");
        cmd.arg(target);
    }


    if let Ok(cmd) = cmd.spawn() {
        log::info!("building {} {}", framework, target);
        let cmd = cmd.wait_with_output();
        match cmd {
            Ok(out) => {
                if out.status.code().unwrap_or(0) != 0 {
                    log::error!("Error building the project:");
                    return Err(format!("Error occued while building the application"));
                }
            }
            Err(e) => {
                log::error!("Error while building project");
                return Err(e.to_string())
            }
        }
    }
    else {
        log::error!("Failed to build the project");
        log::error!("CMD: {:?}", cmd);
        return Err("Failed to start Build program".to_string());
    }
    
    // removes unecessary files
    let output = Path::new(&output);
    let files = match glob(&output.join("*.deps.json").to_string_lossy()) {
        Ok(e) => e,
        Err(e) => {
            log::error!("an error occured while cleaning up the build results");
            return Err(e.to_string());
        }
    };

    
    for file in files {
        if let Ok(file) = file {
            if let Err(e) = fs::remove_file(&file) {
                match &file.file_name() {
                    Some(fname ) => log::error!("Error removing file {}", &fname.to_string_lossy()),
                    None => log::error!("Error removing file \"Filename not found\" "),
                };
                
                return Err(e.to_string());
            }
        }
        else if let Err(e) = file {
            log::error!("Error removing file {}", &e);
            return Err(e.to_string());
        }
    }

    if spec.package.entry.is_none() {
        log::error!("This packer needs an entry (the name of the executable) to pack the application");
        return Err("".to_string());
    }
    let entry = spec.package.entry.as_ref().unwrap();

    if target == "win-x64" {
        let file = Path::new(output).join(format!("{}.exe", entry));
        if let Err(e) = fs::remove_file(&file) {
            match &file.file_name() {
                Some(fname ) => log::error!("Error removing file {}", &fname.to_string_lossy()),
                None => log::error!("Error removing file \"Filename not found\" "),
            };
            return Err(e.to_string());
        }
    }
    else {
        let file = Path::new(output).join(entry);
        if let Err(e) = fs::remove_file(&file) {
            match &file.file_name() {
                Some(fname ) => log::error!("Error removing file {}", &fname.to_string_lossy()),
                None => log::error!("Error removing file \"Filename not found\" "),
            };
            return Err(e.to_string());
        }
    }
    
    Ok(())
}