mod utils;

use std::{fs, path::Path};

use glob::glob;
use utils::{build_proj};
use xpacker_shared::{Arguments, Packer, declare_packer, log, PkgSpec};

const DOTNET_APPSH: &str = 
r#"#!sh
# Higenku Script Runner for Dotnet Applications
exec dotnet ./app/{appname}.dll"#; 


pub struct NetPacker;

impl NetPacker {
    pub fn new() -> Self {
        NetPacker {}
    }
}

impl Packer for NetPacker {
    fn name(&self) -> String {
        ".net".to_string()
    }

    fn version(&self) -> String {
        env!("CARGO_PKG_VERSION").to_string()
    }

    fn pack(&self, _: &String, spec: &PkgSpec, _:&Arguments) -> Result<(), (std::string::String, std::option::Option<std::string::String>)> {        
        
        for target in &spec.targets {
            match build_proj(
                &target.sys, 
                format!("{}/app", target.dir), 
                spec.package.args.get("framework").unwrap_or(&"net6.0".to_string()).clone(), 
                spec) {
                Ok(_) => {},
                Err(e) => {
                    return Err((
                        ("An Error Occured while building the project".to_string()),
                        Some(e.to_string())
                    ));
                }
            };

            let script;
            if let Some(entry) = &spec.package.entry {
                script = DOTNET_APPSH.replace("{appname}", entry);
            }
            else {
                log::error!("Not Entry option found, cannot write app.sh script");
                return Err(("Entry Not found in pkg.yml".to_string(), None));
            }
            fs::write(format!("{}/app.sh", target.dir), script).expect("Couldn't write dotnet app.sh script");
        }

        Ok(())
    }

    fn prefix(&self) -> Vec<String> {
        vec![ "net".to_string() ]
    }

    fn generate(&self, dir: &String, args: &mut Arguments, spec: PkgSpec) -> Result<PkgSpec, (std::string::String, std::option::Option<std::string::String>)> { 
        let mut spec = spec;
        let mut runtime = format!("6.0");
        if let Some(e) = args.get_next_to("--runtime") {
            runtime = e;
        }
        spec.package.name = "net".to_string();
        spec.package.args.insert("framework".to_string(), "net6.0".to_string());
        
        spec.runtime.name = "dotnet".to_string();
        spec.runtime.version = runtime;

        let mut projname = String::new();        
        match glob("*.csproj") {
            Ok(e) => {
                for i in e {
                    if let Ok(buf) = i {
                        projname = buf.file_name().unwrap().to_string_lossy().to_string();   
                        projname = projname.replace(".csproj", "");
                        break;
                    }
                }
            }
            _ => {log::warn!("Couldn't auto discover project name, please specify it on package.entry in pkg.yml");},
        };
        spec.package.entry = Some(projname);

        log::info!("Generation of Specification Successful, see more at {}", Path::new(dir).join("pkg.yml").as_path().to_str().unwrap());

        Ok(spec)
    }
}

declare_packer!(NetPacker, NetPacker::new);