use std::{path::Path};

use xpacker_shared::{Arguments, Packer, declare_packer, log, PkgSpec};


pub struct CustomPacker;

impl CustomPacker {
    pub fn new() -> Self {
        CustomPacker {}
    }
}

impl Packer for CustomPacker {
    fn name(&self) -> String {
        String::from("custom")
    }

    fn version(&self) -> String {
        env!("CARGO_PKG_VERSION").to_string()
    }

    fn pack(&self, _: &String, _: &PkgSpec, _: &Arguments) -> Result<(), (std::string::String, std::option::Option<std::string::String>)> {
        log::warn!("Be aware that the custom packer only generates the manifest for the project, it doens't package your application");
        Ok(())
    }

    fn generate(&self, dir: &String, _: &mut Arguments, spec: PkgSpec) -> Result<PkgSpec, (std::string::String, std::option::Option<std::string::String>)> {
        let mut spec = spec;
        spec.package.name = "custom".to_string();

        log::info!("Generation of Specification Successful, see more at {}", Path::new(dir).join("pkg.yml").as_path().to_str().unwrap());
        Ok(spec)
    }

    fn prefix(&self) -> Vec<String> {
        vec![
            "custom".to_string()
        ]
    }
}

declare_packer!(CustomPacker, CustomPacker::new);