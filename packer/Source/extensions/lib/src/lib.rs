use xpacker_shared::{Packer, PkgSpec, log, declare_packer, Arguments};

pub struct LibraryPacker;

impl LibraryPacker {
    pub fn new() -> Self {
        LibraryPacker {}
    }
}

impl Packer for LibraryPacker {
    fn name(&self) -> String {
        "lib".to_string()
    }

    fn version(&self) -> String {
        env!("CARGO_PKG_VERSION").to_string()
    }

    fn pack(&self, _: &String, _: &xpacker_shared::PkgSpec, _: &xpacker_shared::Arguments) -> Result<(), (std::string::String, std::option::Option<std::string::String>)> {
        log::warn!("This packer doesn't pack your library, only configures the final package");
        Ok(())
    }

    fn generate(&self, _: &String, _: &mut Arguments, spec: PkgSpec) -> Result<PkgSpec, (std::string::String, std::option::Option<std::string::String>)> {
        let mut spec = spec;
        spec.package.name = "lib".to_string();
        Ok(spec)
    }

    fn prefix(&self) -> Vec<String> {
        vec![
            "lib".to_string(),
        ]
    }
}

declare_packer!(LibraryPacker, LibraryPacker::new);