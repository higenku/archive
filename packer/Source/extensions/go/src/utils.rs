use std::process::Command;

use xpacker_shared::log;

pub fn build_proj(target: &String, output: String, is_cgo: bool, clinker: String, cxxlinker: String) -> Result<(), ()>{
    let mut cmd = Command::new("go");
    cmd.args([
        "build",
        "-o",
        output.as_str(),

        // diable debug info
        "-ldflags",
        "-s -w",
    ]);

    let (os, arch) = match target.as_str() {
        "macos_amd64" => ("darwin", "amd64"),
        "win_amd64" => ("linux",  "amd64"),
        "linux_amd64" => ("darwin", "amd64"),

        "macos_aarch64" => ("darwin", "arm64"),
        "linux_aarch64" => ("linux",  "arm64"),


        _ => ("linux", "amd64"),
    };
    cmd.env("GOS", os);
    cmd.env("GOARCH", arch);

    if is_cgo {
        cmd.env("CGO_ENABLED", "1");
        cmd.env("CXX", cxxlinker);
        cmd.env("CC", clinker);
    }

    let cmd = cmd.spawn();

    if let Ok(cmd) = cmd {
        if let Ok(out) = cmd.wait_with_output() {
            if out.status.code().unwrap_or(0) != 0 {
                log::error!("Failed to build project");
                log::debug!("Status code {}", out.status.code().unwrap());
                Err(())
            }
            else {
                log::info!("Build {} succed", target);
                Ok(())
            }
        }
        else {
            log::error!("Failed to get go output");
            Err(())
        }
    }
    else {
        log::error!("Failed to start the go build command");
        Err(())
    }
}