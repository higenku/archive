use xpacker_shared::{Packer, declare_packer, log, PkgSpec, Arguments};
mod utils;
use utils::build_proj;
pub struct GoPacker;
const GO_APPSH: &str = 
r#"#!sh
# Higenku Script Runner for Go Applications
exec ./app/{appName}"#; 

impl GoPacker {
    pub fn new() -> Self {
        GoPacker {}
    }
}

impl Packer for GoPacker {
    fn name(&self) -> String {
        "go".to_string()
    }

    fn version(&self) -> String {
        env!("CARGO_PKG_VERSION").to_string()
    }

    fn pack(&self, _: &String, spec: &PkgSpec, _: &Arguments) -> Result<(), (std::string::String, std::option::Option<std::string::String>)> {
        for target in &spec.targets {

            if spec.package.entry.is_none() { 
                log::error!("Go packer needs an entry in pkg.yml");
                return Err(("Go packer needs an entry in pkg.yml".to_string(), None));
            }
            let entry = spec.package.entry.as_ref().unwrap();

            let contents = 
                if target.sys == "win_amd64" { GO_APPSH.replace("{appName}", format!("{}.exe", entry).as_str()) } 
                else {GO_APPSH.replace("{appName}", entry)};

            if let Err(e) = std::fs::write(format!("{}/app.sh", target.dir), contents) {
                
                return Err(
                    (format!("Couldn't write the app.sh file for target: {}", target.sys), Some(e.to_string()))
                );
            }

            let enable_cgo = match spec.package.args.get("enable_cgo") {
                Some(e) => {
                    if e == &"true".to_string() { true } 
                    else { false }
                }
                None => false,
            };

            let clinker;
            let cxxlinker;
            let program;

            if target.sys == "win_amd64" {
                clinker = if enable_cgo { 
                    spec.package.args.get("c_win").expect("Failed to read the clinker") 
                } else { "NONE" };
                cxxlinker = if enable_cgo { 
                    spec.package.args.get("cxx_win").expect("Failed to read the clinker") 
                } else { "NONE" };
                program = format!("{}/app/{}.exe", target.dir, entry);
            }

            else if target.sys == "linux_amd64" {
                clinker = if enable_cgo { 
                    spec.package.args.get("c_linux").expect("Failed to read the clinker") 
                } else { "NONE" };
                cxxlinker = if enable_cgo { 
                    spec.package.args.get("cxx_linux").expect("Failed to read the clinker") 
                } else { "NONE" };
                program = format!("{}/app/{}", target.dir, entry);    
            }

            else if target.sys == "macos_amd64" {
                clinker = if enable_cgo { 
                    spec.package.args.get("c_macos").expect("Failed to read the clinker") 
                } else { "NONE" };
                cxxlinker = if enable_cgo { 
                    spec.package.args.get("cxx_macos").expect("Failed to read the clinker") 
                } else { "NONE" };
                program = format!("{}/app/{}", target.dir, entry);    
            }
            
            // aarch64
            else if target.sys == "macos_aarch64" {
                clinker = if enable_cgo { 
                    spec.package.args.get("c_macos_aarch64").expect("Failed to read the clinker") 
                } else { "NONE" };
                cxxlinker = if enable_cgo { 
                    spec.package.args.get("cxx_macos_aarch64").expect("Failed to read the clinker") 
                } else { "NONE" };
                program = format!("{}/app/{}", target.dir, entry); 
            }
            else if target.sys == "linux_aarch64" {
                clinker = if enable_cgo { 
                    spec.package.args.get("c_linux_aarch64").expect("Failed to read the clinker") 
                } else { "NONE" };
                cxxlinker = if enable_cgo { 
                    spec.package.args.get("cxx_linux_aarch64").expect("Failed to read the clinker") 
                } else { "NONE" };
                program = format!("{}/app/{}", target.dir, entry); 
            }


            else {
                return Err(
                    (format!("system not supported {}", target.sys), None)
                );
            }

            if let Err(_) = build_proj(&target.sys, program, enable_cgo, clinker.to_string(), cxxlinker.to_string()) {
                return Err(
                    ("Failed to build the project".to_string(), None)
                );
            }
        }
        Ok(())
    }

    fn generate(&self, _: &String, _: &mut Arguments, spec: PkgSpec) -> Result<PkgSpec, (std::string::String, std::option::Option<std::string::String>)> {
        let mut spec = spec;

        spec.package.name = "go".to_string();
        spec.package.entry = Some("#WRITE THE NAME OF YOUR PROGRAM HERE".to_string());

        spec.package.args.insert("enable_cgo".to_string(), "false".to_string());
        spec.package.args.insert("c_win".to_string(), "x86_64-w64-mingw32-gcc".to_string());
        spec.package.args.insert("c_macos".to_string(), "o64-clang".to_string());
        spec.package.args.insert("c_linux".to_string(), "cc".to_string());


        spec.package.args.insert("cxx_win".to_string(), "x86_64-w64-mingw32-g++".to_string());
        spec.package.args.insert("cxx_macos".to_string(), "o64-clang++".to_string());
        spec.package.args.insert("cxx_linux".to_string(), "g++".to_string());


        // aarch64
        spec.package.args.insert("c_macos_aarch64".to_string(), "o64h-clang".to_string());
        spec.package.args.insert("c_linux_aarch64".to_string(), "aarch64-linux-gnu-gcc".to_string());


        spec.package.args.insert("cxx_macos_aarch64".to_string(), "o64h-clang++".to_string());
        spec.package.args.insert("cxx_linux_aarch64".to_string(), "aarch64-linux-gnu-g++".to_string());

        log::info!("Generation finished");
        Ok(spec)
    }

    fn prefix(&self) -> Vec<String> {
        vec![
            "go".to_string()
        ]
    }
}

declare_packer!(GoPacker, GoPacker::new);