# Plan for development

This file refers to the development process of the Higenku client project.

* 1 Step:
    * Backend Blazor With full control over the backend
    * Frontend in Python with pyqt5 using QtWebEngine for WebInterface
        * Events for all necessary things the backend needs, example:
            * Notifications
            * File Dialogs
            * Creation and Destruction of windows
            * System Tray
        * System based on SignalR events
    
* 2 Step:
    * Support Pyqt5 project for + 2 months
    * Backend and Frontend in C# using Gtk as the UI Toolkit
        * Chromium Content Module | servo | Netsurf webview for web windows
    
* 3 Step:
    * Drop Support for PyQt5 (If we need)
    * Servo webview (when it renders everything)
