using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace HiClient.Backend
{
    public class Backend
    {
        public static void Main()
        {
            System.Console.WriteLine("main method of backend");
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHost(webBuilder =>
                {
                    var contentRoot = AppDomain.CurrentDomain.BaseDirectory + "wwwroot";
                    System.Console.WriteLine("Contnet Root " + contentRoot);
                    if(!Directory.Exists(contentRoot))
                        Directory.CreateDirectory(contentRoot);

                    webBuilder.UseUrls("http://localhost:5000");
                    webBuilder.ConfigureLogging( s => {
                        s.AddConsole();
                        s.SetMinimumLevel(LogLevel.Warning);
                    });
                    webBuilder.UseWebRoot(contentRoot);
                    webBuilder.UseKestrel();
                    webBuilder.UseStartup<Startup>();
                });
    }
}
