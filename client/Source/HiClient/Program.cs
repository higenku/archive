﻿using System;
using System.Threading.Tasks;
using HiClient.Backend;
using SharpWebview;
using SharpWebview.Content;

namespace HiClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var server = Backend.Backend.CreateHostBuilder(args).Build();
            server.StartAsync();
            using var webview = new Webview();
            // using var webview2 = new Webview();
            webview
                .SetTitle("The Hitchhicker")             
                .SetSize(1024, 768, WebviewHint.None)
                .SetSize(800, 600, WebviewHint.Min)
                .Navigate(new UrlContent("http://localhost:5000"));

            // webview2
            //     .SetTitle("The Hitchhicker")             
            //     .SetSize(1024, 768, WebviewHint.None)
            //     .SetSize(800, 600, WebviewHint.Min)
            //     .Navigate(new UrlContent("http://localhost:5000"));

            // Task.Run(() => webview2.Run());
            webview.Run();
            server.Dispose();
            server.StopAsync();
        }
    }
}
