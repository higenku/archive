# Higenku Client
[Plan for development](Plan.md)

## Idea
Client for install and updates apps from Higenku App Repository <br>
[Documentation]() <!--TODO: -->

## Propose
Install, Manage and Update Apps and Runtimes.

## Supported Platforms
* Linux x64
* osx x64
* windows x64

## Future Support
* Android
* PostMarkedOS

# Tip
use /usr/share/applications/mimeapps.list to configure applications alias (.gkapp) [doc](https://help.gnome.org/admin/system-admin-guide/stable/mime-types-application-user.html.en)
