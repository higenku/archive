# bgfx-rs for the XQuark
Rust bindings to [bgfx](https://github.com/bkaradzic/bgfx), a cross-platform, graphics API agnostic, "Bring Your Own Engine/Framework" style rendering library.

This part of the project was altered in order to make it more easier for us to build the quark project.