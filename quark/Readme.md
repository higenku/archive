# Higenku User Interface Framework (XQuark)
This project Includes tools for building cross-platform UI apps

# Libraries
| Name | Purpose | Status | 
| --- | --- | --- |
| xquark_core      | Object Communication, Universal Ptrs (`Ptr`)                   | **Dev** |
| xquark_gui       | windowing system, event handling, bgfx implementation (XRender)| **Dev** | 
| xquark_multimedia| multimedia implementation (png, jpegs, avi, mp4, mp3, ...)     | **Dev** |
| xquark_widgets   | Widgets Implementation                                         | **Dev** |
| xquark_web       | Library with webengine                                         | **Dev** |
| xquark_render    | render based on [bgfx](https://github.com/bkaradzic/bgfx)      | **Dev** |
| xquark_web       | html renderer based on [blink](https://docs.google.com/document/d/1aitSOucL0VHZa9Z2vbRJSyAIsAz24kX8LFByQ5xQnUg/edit#) | **Not started** |
| xquark           | Library with Everything                                        | **Dev** |

## How it compares to Qt
|Qt Module      | XUI Component     |
| ---           | ---               |
|Qt Core        | xquark_core       |
|Qt GUI         | xquark_gui        |
|Qt Multimedia  | xquark_multimedia |
|Qt Widgets     | xquark_widgets    |
|Qt Webengine   | xquark_web        |

# Credits
Some of the parts of this project were inspired by [azul](https://azul.rs), [Qt Framework](https://qt.io) and [servo](https://servo.org)