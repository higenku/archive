
use std::{
    env, 
    path::{Path}, 
    net::{IpAddr},
};
use actix_web::{HttpServer, App, web, middleware, HttpResponse};

use dotenv::dotenv;

// #[cfg(test)]
// mod tests;

// mod graphql;
// mod templates;
// mod api;
// mod abstraction;
// mod models;
// mod services;
// mod utils;

// use crate::graphql::{generate_schema, gql_handler};
// use services::{Captcha, Jwt, Mailer};
// use tokio::sync::Mutex;

// use crate::{abstraction::AppInfo, services::SodiumCrypto};

#[actix_web::main]
async fn main() {
    println!("setting up crypto");
    match sodiumoxide::init() {
        Ok(_) => println!("Sodium Initialization Successful!"),
        Err(_) => {
            println!("Failed to initialize the crypto library");
            return;
        }
    };

    // println!("setting up terminal");
    // utils::setup_term();
    // log::info!("setting up");
    if Path::new(".env").exists() {
        dotenv().ok();
    }

    log::info!("setting up host address");

    let port = env::var("PORT").unwrap_or("3001".to_string()).parse::<u16>().unwrap();
    let host = env::var("HOST").unwrap_or("::1".to_string()).parse::<IpAddr>().unwrap();
    log::info!("setting up services");

    // services
    // let schema = web::Data::new(generate_schema());
    // let app_info = web::Data::new(AppInfo { 
    //     secretkey: env::var("SECRET_KEY").expect("No SECRET_KEY defined"),
    //     host: env::var("DOMAIN").expect("No DOMAIN defined"),
    //     cookiehost: env::var("COOKIE_DOMAIN").expect("No COOKIE_DOMAIN defined"),
    //     cryptokey: env::var("CRYPTO_KEY").expect("No CRYPTO_KEY defined"),
    // });
    // let db = web::Data::new(Mutex::new(utils::setup_db().await.expect("Failed to setup database")));
    // let sodiumcrypto = web::Data::new(SodiumCrypto::new(&app_info.cryptokey).expect("Failed to initialize Sodium Crypto Module"));
    // let captcha = web::Data::new(Captcha::new(env::var("HCAPTCHA_KEY").expect("HCaptcha Key not present")));
    // let mailer = web::Data::new(Mailer::new());
    // let jwt = web::Data::new(Jwt::new());

    
    log::info!("Setting up server");
    let server = HttpServer::new(move || {
        App::new()
        // logging
        .wrap(middleware::Logger::new("%D %s %r"))
        .wrap(middleware::DefaultHeaders::new()
            .add(("server", "Actix/Higenku Servers"))
            .add(("Access-Control-Allow-Credentials", "true"))
            // .add(("Access-Control-Allow-Origin", "*"))
            // .add(("Access-Control-Allow-Headers", "*"))
            // .add(("Access-Control-Allow-Methods", "*"))
        )
        
        .wrap(middleware::NormalizePath::new(middleware::TrailingSlash::Trim))
        .wrap(actix_cors::Cors::default()
            .allowed_origin("http://localhost:3000")
            .allowed_origin("http://127.0.0.1:3000")
            .allowed_origin("https://user.higenku.org")
            .allowed_methods(vec!["GET", "POST", "PUT"])
            .supports_credentials()
            .allow_any_header()
            .max_age(3600))
        // Services
        

        // Routes
        .service(
            web::resource("/")
                .route(web::get().to(|| HttpResponse::Ok()))
        )
    })
    .bind((host, port)).expect("Failed to bind IP Address")
    .run();

    log::warn!("Listening on http://[{host}]:{port}");

    match server.await {
        Ok(_) => {},
        Err(e) => {
            log::error!("An error occured while stopping the server {e:?}")
        },
    };
}