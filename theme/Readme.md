# Higenku Theme (XThm) [![pipeline status](https://gitlab.com/higenku/sdks/theme/badges/dev/pipeline.svg)](https://gitlab.com/higenku/sdks/theme/-/commits/dev)
Based on the [github primer/css](https://primer.style/css), [Bootstrap](https://getbootstrap.com/) and [Elastic UI](https://ui.elastic.co/), this is the repository that holds our Theme for almost all our sites and applications.

## Introduction
The main idea behind this theme is that you can use it with whatever framework you want, It's only implemented with web components, Javascript and CSS, so It should be very simple and easy to get it working with Svelte, Vue, Preact, Aurelia, etc. 

We made it so that we extract the base logic out of reusable components, so that we can optimize for size, and speed. Take a look into tests/index.html to learn more. Which makes us achieve 50kb for javascript and 40kb for css.

## Feedback
You can start a duscussion on the [Feedback Cattegory](https://community.higenku.org) or use our [Discord / Matrix](https://higenku.org/contact)

## Contributing
See the [contributing guide](./Contributing.md)

## Acknowledgments
See the [main site](https://higenku.org/acknowledgments)

## License
[see the license](./LICENSE)

## Development
1. for development
> You will need pnpm
```shell
pnpm i
pnpm dev
```

2. for production
```Shell
pnpm i
pnpm build
```
