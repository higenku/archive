# DISCLAIMER

Icons inside the `remix/` directory are from [remixicon.com](https://remixicon.com/). License at https://github.com/Remix-Design/RemixIcon/blob/master/License.

Icons inside the `higenku/` directory are logos from our software groups and software.


Icons inside the `feather/` are from [github.com/feathericons/feather v4.29.0](https://github.com/feathericons/feather/releases). License at https://github.com/feathericons/feather/blob/master/LICENSE