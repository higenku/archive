import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'

// https://vitejs.dev/config/
export default defineConfig({
    mode: 'production',
    build: {
        outDir: './dist',
        lib: {
            entry : 'src/lib.ts',
            name: "higenkuTe",
            formats: [ 'cjs', 'es'],
            fileName: 'xthm_te'
        },
    },
    plugins: [svelte()]
})
