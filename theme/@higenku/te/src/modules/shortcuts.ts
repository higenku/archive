import type Quill from "quill";
import type Module from "quill/core/module";
import Delta from "quill-delta"

class Counter {
    private container
    constructor(private quill: Quill, private options) {

        this.container = document.querySelector(options.container);
        quill.on("text-change", this.update.bind(this));
        this.update(); // Account for initial contents
    }

    calculate() {
        let text = this.quill.getText();
        if (this.options.unit === "word") {
            text = text.trim();
            // Splitting empty text returns a non-empty array
            return text.length > 0 ? text.split(/\s+/).length : 0;
        } else {
            return text.length;
        }
    }

    update() {
        var length = this.calculate();
        var label = this.options.unit;
        if (length !== 1) {
            label += "s";
        }
        this.container.innerText = length + " " + label;
    }
}

class KeyboardController {
    private container
    constructor(private quill: Quill, private options) {

        this.container = document.querySelector(options.container);
        quill.on("text-change", this.update.bind(this));
        this.update(); // Account for initial contents
    }

    calculate() {
        let text = this.quill.getText();
        if (this.options.unit === "word") {
            text = text.trim();
            // Splitting empty text returns a non-empty array
            return text.length > 0 ? text.split(/\s+/).length : 0;
        } else {
            return text.length;
        }
    }

    update() {
        var length = this.calculate();
        var label = this.options.unit;
        if (length !== 1) {
            label += "s";
        }
        this.container.innerText = length + " " + label;
    }
}

const registerShortcuts = async (quill: Quill) => {
    // @ts-ignore <- resets the keyboard shortcuts
    quill.keyboard.bindings = {} 

    quill.on('selection-change', (range, oldrange, source) => {
        console.log(`range`, range)
        console.log(`oldrange`, oldrange)
        console.log(`source`, source)
    })

    quill.on('editor-change', () => {

    })

    quill.on('text-change', (delta, olddelta, source) => {
        console.log(`text change`, delta, olddelta, source)
    })

    quill.keyboard.addBinding(
        {
            key: "b",
            ctrlKey: true,
            // shiftKey: true,
        },
        function (range, context) {
            console.log(`doing shit`)
            // quill.format(range, "bold", true);
        }
    );

    // new ().
    let opt = new Delta([
        { insert: 'Hello ', attributes: {bold: true} },
        { insert: 'World ', attributes: {italic: true} },
        { insert: 'My friends \n' }
    ])
        

    quill.setContents(opt as any, 'api');
      
};

export { registerShortcuts, Counter };
