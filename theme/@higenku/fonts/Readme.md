# Fonts used by Higenku Projects

we use [Manrope](https://fonts.google.com/specimen/Manrope) from google fonts. 

This font is Licensed with the [SIL Open Font License](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL)