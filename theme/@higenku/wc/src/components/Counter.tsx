import { createSignal } from "solid-js";
import {customElement, ComponentType} from "solid-element"

import "./Counter.css";

export default function Counter() {
  const [count, setCount] = createSignal(0);
  return (
    <button class="increment" onClick={() => setCount(count() + 1)}>
      Clicks: {count()}
    </button>
  );
}


// customElement('x-counter', Counter)