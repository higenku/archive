import { sveltekit } from '@sveltejs/kit/vite';
import { resolve } from 'path';
import type { UserConfig } from 'vite';

const config: UserConfig = {
    server: {
        port: 3000
    },
	resolve: {
        alias: {
            '$lib': resolve(__dirname, "src/lib")
        }
    },
    plugins: [sveltekit()]
    
};

export default config;
