import { tick } from "svelte";
import type { Variant } from "../types";

export interface INotification {
    id: number;
    closeBtn: boolean;
    timeout?: number;
    title: string;
    htmlContent: string;
    variant: Variant;
    loading: boolean;
}

export const newNotification = async (
    title: string,
    htmlContent: string,
    closeBtn = false,
    variant: Variant = "",
    timeout = -1,
    loading = false
) => {
    dispatchEvent(
        new CustomEvent("xthm--notification-new", {
            detail: {
                id: Date.now() * Math.floor(Math.random() * 100),
                closeBtn,
                title,
                htmlContent,
                loading,
                variant: variant ?? "",
                timeout: timeout ?? -1,
            },
        })
    );

    await tick();
};

export const clearNotifications = async () =>
    dispatchEvent(new Event("xthm--notification-clear"));
