import { writable } from "svelte/store";

export const targets = writable<HTMLElement[]>([]);
export const ctxMenus = writable<Function[]>([]);
