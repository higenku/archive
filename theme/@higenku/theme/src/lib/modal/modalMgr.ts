import { writable } from "svelte/store";

export interface IModal {
    title: string;

    open: boolean;
    content: string;
    htmlContent: boolean;
    allowOutsideClick: boolean;

    confirm?: string;
    cancel?: string;
    danger?: string;

    /*returns true to close the modal */
    onCancel: () => boolean;
    /*returns true to close the modal */
    onConfirm: () => boolean;
    /*returns true to close the modal */
    onDanger: () => boolean;
}

export const modals = writable<IModal[]>([]);

export const newModal = ({
    title,
    content,
    confirm,
    cancel,
    danger,
    onConfirm,
    onCancel,
    onDanger,
    open = true,
    allowOutsideClick = false,
    htmlContent = false,
}: IModal) => {
    console.log(`Creating new modal`);
    modals.update((mdls) => [
        ...mdls,
        {
            title,
            content,
            confirm,
            cancel,
            danger,
            open,
            onConfirm,
            onCancel,
            onDanger,
            allowOutsideClick,
            htmlContent,
        },
    ]);
    console.log(`Done`);
};

export const clearModals = () => modals.set([]);