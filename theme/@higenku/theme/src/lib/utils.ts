export function outsideClick(node: HTMLDivElement) {
    const handleClick = (ev: MouseEvent) => {
        node.dispatchEvent(new CustomEvent("outclick", { detail: ev }));
    };

    document.addEventListener("click", handleClick, true);

    return {
        destroy() {
            document.removeEventListener("click", handleClick, true);
        },
    };
}
