import CmdPallet from "./cmdpallet/CmdPallet.svelte";
// Footer
import Footer from "./footer/Footer.svelte";
import FooterItem from "./footer/FooterItem.svelte";
import FooterSection from "./footer/FooterSection.svelte";

// Forms
import Checkbox from "./forms/Checkbox.svelte";
import FileInput from "./forms/FileInput.svelte";
import Input from "./forms/Input.svelte";
import MultiSelect from "./forms/MultiSelect.svelte";
import PasswordInput from "./forms/PasswordInput.svelte";
import Select from "./forms/Select.svelte";

// Loaders
import Skeleton from "./loaders/Skeleton.svelte";
import SkeletonBox from "./loaders/SkeletonBox.svelte";
import Spinner from "./loaders/Spinner.svelte";

// menu
import CtxMenu from "./menu/CtxMenu.svelte";
import DropMenu from "./menu/DropMenu.svelte";
import MenuHeader from "./menu/MenuHeader.svelte";
import MenuItem from "./menu/MenuItem.svelte";
import MenuNavigation from "./menu/MenuNavigation.svelte";
import MenuSection from "./menu/MenuSection.svelte";

// Modal
import Modal from "./modal/Modal.svelte";
import Modals from "./modal/Modals.svelte";

// Nav
import Nav from "./nav/Nav.svelte";
import NavSection from "./nav/NavSection.svelte";
import NavItem from "./nav/NavItem.svelte";

// Notification
import Notification from "./notification/Notification.svelte";
import Notifications from "./notification/Notifications.svelte";

// Page
import Page from "./page/Page.svelte";
import PageContent from "./page/PageContent.svelte";
import PageSidebar from "./page/PageSidebar.svelte";
import PageToc from "./page/PageToc.svelte";

// Step
import Step from "./step/Step.svelte";
import StepGroup from "./step/StepGroup.svelte";

// Tabs
import Tab from "./tabs/Tab.svelte";
import Tabs from "./tabs/Tabs.svelte";

// Treeview
import TreeView from "./treeview/TreeView.svelte";
import TreeHeader from "./treeview/TreeHeader.svelte";
import TreeItem from "./treeview/TreeItem.svelte";

// Other components
import Alert from "./Alert.svelte";
import Avatar from "./Avatar.svelte";
import Badge from "./Badge.svelte";
import Btn from "./Btn.svelte";
import Card from "./Card.svelte";
import Container from "./Container.svelte";
import Dots from "./Dots.svelte";
import Highlight from "./Highlight.svelte";
import Icon from "./Icon.svelte";
import Image from "./Image.svelte";
import Popover from "./Popover.svelte";
import Progress from "./Progress.svelte";
import Table from "./Table.svelte";
import VideoPlayer from "./VideoPlayer.svelte";

import {
    newModal as m_newModal,
    clearModals as m_clearModals,
} from "./modal/modalMgr";

import {
    newNotification as m_notification,
    clearNotifications as m_clearNotifications,
} from "./notification/system.js";

namespace utils {
    // export const newModal = m_modal;
    export const newNotification = m_notification;
    export const clearNotifications = m_clearNotifications;

    export const newModal = m_newModal;
    export const clearModals = m_clearModals;
}

export { utils,
    CmdPallet,
    Footer, FooterItem, FooterSection, 
    Checkbox,
    FileInput,
    Input,
    MultiSelect,
    PasswordInput,
    Select,
    Skeleton,
    SkeletonBox,
    Spinner,
    CtxMenu,
    DropMenu,
    MenuHeader,
    MenuItem,
    MenuNavigation,
    MenuSection,
    Modal,
    Modals,
    Nav,
    NavSection,
    NavItem,
    Notification, 
    Notifications,
    Page,
    PageContent,
    PageSidebar,
    PageToc,
    Step,
    StepGroup,
    Tab,
    Tabs,
    TreeView,
    TreeHeader,
    TreeItem,
    Alert,
    Avatar,
    Badge,
    Btn,
    Card,
    Container,
    Dots,
    Highlight,
    Icon,
    Image,
    Popover,
    Progress,
    Table,
    VideoPlayer,
};
