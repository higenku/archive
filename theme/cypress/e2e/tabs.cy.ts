describe('Tabs', () => {
    beforeEach(() => {
        cy.visit("http://localhost:3000")
    })
    
    it('Should show the first tab for default', () => {
        // arrange
        cy.visit('/tabs')
        
        // assert
        let content = cy.get('.tabs-content')
        content.should('contain.text', 'Contents of Tab 1')
        content.should('not.contain.text', 'Contents of Tab 2')
    

        let header = cy.get('.active');
        header.should('contain', "Tab 1");      
        header.should('not.contain', "Tab 2");      
        header.should('not.contain', "Tab 3");      
    })

    // Todo: Needs to be re done 
    it('should change tab when clicking on tab header', () => {
        
        // arrange
        cy.visit('/tabs')
        
        // act
        /* Clicks on the Tab element */
        cy.get('.tabs-item') 
            .next()
            .first()
            .click();
        
        // assert
        /* Gets tab content */  
        cy.get('.tabs-content')
            .should('contain', 'Contents of Tab 2')
            .should('not.contain', 'Contents of Tab 1')
            .should('not.contain', 'Contents of Tab 3')
        
        /* Gets the active header item */
        cy.get('.tabs-header .active')
            .should('contain.text', "Tab 2")
            .should('not.contain.text', "Tab 1")
            .should('not.contain.text', "Tab 3")      
        
        // act
        /* Clicks on the tab 3 element */
        cy.get('.tabs-item')
            .next().next()
            .first()
            .click();
        
        // assert
        /* Gets tab content */
        cy.get('.tabs-content')
            .should('contain.text', 'Contents of Tab 3')
            .should('not.contain.text', 'Contents of Tab 1')
            .should('not.contain.text', 'Contents of Tab 2')
  
        /* Gets the active header item */
        cy.get('.tabs-header .active')
            .should('contain', "Tab 3")
            .should('not.contain', "Tab 1")
            .should('not.contain', "Tab 2")
    })
})