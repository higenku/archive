describe('Alerts', () => {
    beforeEach(() => {
        cy.visit("http://localhost:3000")
    })
    
    it('Should exists', () => {
        // arrange
        cy.visit('/alerts')
        
        // assert
        cy.get('.info')
            .should('contain.text', 'Simple info alert')
        cy.get('.succ')
            .should('contain.text', 'Simple succ alert')
        cy.get('.warn')
            .should('contain.text', 'Simple warn alert')
        cy.get('.erro')
            .should('contain.text', 'Simple erro alert')      
    })
})

// Todo:

describe('Videos', () => {
    beforeEach(() => {
        cy.visit("http://localhost:3000")
    })
    
    it('NEEDS A WAY TO TEST IT', () => {})
})

describe('Text Editor', () => {
    beforeEach(() => {
        cy.visit("http://localhost:3000")
    })
    
    it('NEEDS A WAY TO TEST IT', () => {})
})

describe('Typography', () => {
    beforeEach(() => {
        cy.visit("http://localhost:3000")
    })
    
    it('NEEDS A WAY TO TEST IT', () => {})
})

describe('treeview', () => {
    beforeEach(() => {
        cy.visit("http://localhost:3000")
    })
    
    it('NEEDS A WAY TO TEST IT', () => {})
})

describe('Progress', () => {
    beforeEach(() => {
        cy.visit("http://localhost:3000")
    })
    
    it('NEEDS A WAY TO TEST IT', () => {})
})

describe('Popover', () => {
    beforeEach(() => {
        cy.visit("http://localhost:3000")
    })
    
    it('NEEDS A WAY TO TEST IT', () => {})
})

describe('Notifications', () => {
    beforeEach(() => {
        cy.visit("http://localhost:3000")
    })
    
    it('NEEDS A WAY TO TEST IT', () => {})
})

describe('Navs', () => {
    beforeEach(() => {
        cy.visit("http://localhost:3000")
    })
    
    it('NEEDS A WAY TO TEST IT', () => {})
})

describe('Modal', () => {
    beforeEach(() => {
        cy.visit("http://localhost:3000")
    })
    
    it('NEEDS A WAY TO TEST IT', () => {})
})

describe('Menus', () => {
    beforeEach(() => {
        cy.visit("http://localhost:3000")
    })
    
    it('NEEDS A WAY TO TEST IT', () => {})
})

describe('Loaders', () => {
    beforeEach(() => {
        cy.visit("http://localhost:3000")
    })
    
    it('NEEDS A WAY TO TEST IT', () => {})
})

describe('Components', () => {
    beforeEach(() => {
        cy.visit("http://localhost:3000")
    })
    
    it('NEEDS A WAY TO TEST IT', () => {})
})

describe('Forms', () => {
    beforeEach(() => {
        cy.visit("http://localhost:3000")
    })
    
    it('NEEDS A WAY TO TEST IT', () => {})
})

describe('Example', () => {
    beforeEach(() => {
        cy.visit("http://localhost:3000")
    })
    
    it('NEEDS A WAY TO TEST IT', () => {})
})

describe('DragNDrop', () => {
    beforeEach(() => {
        cy.visit("http://localhost:3000")
    })
    
    it('NEEDS A WAY TO TEST IT', () => {})
})

describe('CmdPallet', () => {
    beforeEach(() => {
        cy.visit("http://localhost:3000")
    })
    
    it('NEEDS A WAY TO TEST IT', () => {})
})

describe('Icons', () => {
    beforeEach(() => {
        cy.visit("http://localhost:3000")
    })
    
    it('NEEDS A WAY TO TEST IT', () => {})
})

describe('Images', () => {
    beforeEach(() => {
        cy.visit("http://localhost:3000")
    })
    
    it('NEEDS A WAY TO TEST IT', () => {})
})