# Contributing guide
Like the other projects, this contributing guide is just a supplementary of the main one present at [Contributing guide](https://higenku.org/legal/contributing)

## Code Style
On typescript files, please follow the [JavaScript coding style](https://javascript.info/coding-style) and in scss follow the following:

## creating a new component
please make sure you can't achieve the behavior with the existing components or combination of those. If not, each component should have it's scss and ts file separated, if you are making a family of components (like forms) you should create a new folder for those.

In each scss file:

```scss

@mixin component-name-dark {
    // ... dark variant
}

@mixin component-name-light {
    // ... light variant 
}

.component-name {
    // ... code
}

```

then in the `themes/[variant].scss` you should include:

```scss
@use "../component-name" as *;

@mixin theme-[variant] {
    & {
        @include component-name-[variant];
        // .. code for the rest of the themes
    }
}
```

prefer to use css when possible and only use javascript for what it's necessary.