#![allow(non_snake_case)]

const VERSION: &'static str = env!("CARGO_PKG_VERSION");

mod lib;

fn main() {
    println!("Higenku Backup System version {}\n", VERSION);
    let commands: Vec<String> = std::env::args().collect();
    let helpCommand = format!(
        "{}\n{}", 
        "  \"start\"        to start the backup in the current directory",
        "  \"restore\" [ID] to restore the backup with the [ID] to the \"restore\" directory"
    );


    match commands[1].as_str() {
        "start" => lib::start::run_command(),
        "restore" => lib::restore::run_command(),
        "help" => println!("{}", helpCommand),
        _ => println!("Command: {} not found \nuse -h for help", commands[1])
    }
}