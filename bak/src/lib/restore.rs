use std::{env, fs::File, io, path::Path};

use tar::Archive;
use zstd::Decoder;

pub fn run_command() {
    let ids: Vec<String> = env::args().collect();
    let id  = &ids[2];

    if Path::new(&format!(".xbackup/{}.tar.zst", id)).exists() {
        println!("Restoring {} to restore/", id);
        println!("[1/2] Decompress");
        decompress_zst(id);
        println!("[2/2] Decompress");
        decompress_tar(id);
        println!("Done Decompress");
    }
    else {
        println!("Failed to start restore, ID doens't exists")
    }
}


fn decompress_zst(file: &String) {
    let zstd_stream = File::open(format!(".xbackup/{}.tar.zst", &&file)).expect("unable to open backup file");
    let mut stream = File::create(format!(".xbackup/{}.tar", &&file)).expect("unable to open backup file");
    let mut decoder = Decoder::new(zstd_stream).unwrap();
    io::copy(&mut decoder, &mut stream).unwrap();
    decoder.finish();
}

fn decompress_tar(file: &String) {
    let file = format!(".xbackup/{}.tar", &file);
    let stream = File::open(&file).expect("unable to open backup file");
    let mut archive = Archive::new(stream);
    archive.unpack("restore").expect("Failed extracting to restore directory");
    
    std::fs::remove_file(file).unwrap();
}