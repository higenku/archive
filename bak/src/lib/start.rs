use std::{fs::{self, File}, io, path::Path};

use tar::Builder;
use uuid::Uuid;



pub fn run_command() {
    let id = Uuid::new_v4().to_simple().to_string();
    let tarfile = format!(".xbackup/{}.tar", id);
    println!("[1/2] Compress");
    compress_tar(&tarfile);
    println!("[2/2] Compress");
    compress_zst(&tarfile);
    println!("Compress Done");

    fs::remove_file(tarfile).unwrap();
    println!("Removing tar file");
    println!("ID: {}", id);
}

fn compress_zst(tarfile: &String) {
    let zstd_stream = File::create(format!("{}.zst", &tarfile)).expect("unable to create backup file");
    let mut stream = File::open(&tarfile).expect("unable to create backup file");
    let mut encoder = zstd::stream::Encoder::new(zstd_stream, 1).unwrap();
    io::copy(&mut stream, &mut encoder).unwrap();
    encoder.finish().unwrap();
}

fn compress_tar(tarfile: &String) {
    if !Path::new(".xbackup").exists() {
        fs::create_dir(".xbackup").expect("Unable to create directory .xbackup");
    }

    let stream = File::create(&tarfile).expect("unable to create backup file");
    
    let mut builder = tar::Builder::new(stream);
    builder.follow_symlinks(true);
    
    let curdir = std::env::current_dir().unwrap();
    let curdir = curdir.to_str().unwrap();

    add(curdir, curdir, &mut builder);

    builder.finish().expect("unable to save the backup");
}

fn add(path: &str, root: &str, builder: &mut Builder<File>) {

    if path.contains(".git") || path.contains(".xbackup") { return; } 

    let file_path = Path::new(path);

    let mut archivePath = path.replace(root, "");   
    if archivePath == String::from("") {
        archivePath = format!("./{}", archivePath);
    }
    else if archivePath.starts_with("/") {
        archivePath = archivePath.replace(root, "");
        archivePath = format!(".{}", archivePath);
    }

    if file_path.is_dir() {
        println!("===> Adding Directory {} ", path);
        builder.append_dir(&archivePath, path).unwrap();
        let entries = fs::read_dir(path).unwrap()
            .map(|res| res.map(|e| e.path()))
            .collect::<Result<Vec<_>, io::Error>>().unwrap();

        for i in entries {
            add(i.to_str().unwrap(), root, builder);
        }
    }
    else {
        println!("=> Adding File {} ", path);
        builder.append_file(archivePath, &mut File::open(file_path).unwrap()).unwrap();
    }

}