use std::fs::{File, read};
use xkrome::baselib::application::window::Icon;


// Copyright 2019-2021 Tauri Programme within The Commons Conservancy
// SPDX-License-Identifier: Apache-2.0
// SPDX-License-Identifier: MIT

pub fn main() -> xkrome::baselib::Result<()> {

  use  xkrome::baselib::{
    application::{
      event::{Event, StartCause, WindowEvent},
      event_loop::{ControlFlow, EventLoop},
      window::WindowBuilder,
    },
    webview::WebViewBuilder,
  };
  
  // let decoder = png::Decoder::new(File::open("assets/XKrome.png").unwrap());
  let decoder = png::Decoder::new(File::open("assets/icon.png").unwrap());
  let (info, mut reader) = decoder.read_info().unwrap();
  let mut icon: Vec<u8> = vec![0; info.buffer_size()];
  reader.next_frame(&mut icon).unwrap();
  
  let event_loop = EventLoop::new();
  let window = WindowBuilder::new()
    .with_title("Hello World")
    .with_window_icon(Some(Icon::from_rgba(icon, info.width, info.height).unwrap()))
    .build(&event_loop)
    .unwrap();

  let _webview = WebViewBuilder::new(window)
    .unwrap()
    .with_custom_protocol("wry".into(), move |_, requested_asset_path| {
      // Remove url scheme
      let path = requested_asset_path.replace("wry://", "");
      // Read the file content from file path
      println!("requesting {}", &path);
      let content = read(&path)?;
      // Return asset contents and mime types based on file extentions
      // If you don't want to do this manually, there are some crates for you.
      // Such as `infer` and `mime_guess`.
      if path.ends_with(".html") {
        Ok((content, String::from("text/html")))
      } else if path.ends_with(".js") {
        Ok((content, String::from("text/javascript")))
      } else if path.ends_with(".png") {
        Ok((content, String::from("image/png")))
      } else {
        unimplemented!();
      }
    })
    // tell the webview to load the custom protocol
    .with_url("wry://assets/index.html")?
    .build()?;


  event_loop.run(move |event, _, control_flow| {
    *control_flow = ControlFlow::Wait;

    match event {
      Event::NewEvents(StartCause::Init) => {}
      Event::WindowEvent {
        event: WindowEvent::CloseRequested,
        ..
      } => *control_flow = ControlFlow::Exit,
      _ => (),
    }
  });
}
