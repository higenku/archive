
#![feature(format_args_capture)]

mod wry;

use std::path::PathBuf;

use wry::{
  multi_window,
  hello_world,
  custom_data_directory,
  custom_protocol,
  custom_titlebar,
  detect_js_ecma,
  dragndrop,
  fullscreen,
  html_test,
  menu_bar,
  rpc,
  system_tray,
  transparent,
  system_tray_no_menu,
  validate_webview,
};
use xkrome::{CEventData, ceventdata_to_eventdata, string_to_cstring};

extern fn callback(_data: CEventData) -> CEventData {
  let _data = ceventdata_to_eventdata(_data);
  // WindowEvent::CloseRequested
  if _data._type == "WindowEvent" && _data._subtype == "CloseRequested" {
    println!("Finishing Call");
    return CEventData {
      _type:  string_to_cstring("ControlFlow".to_string()),
      _result:  string_to_cstring("Exit".to_string()),
      _data: string_to_cstring("".to_string()),
      _subtype:  string_to_cstring("".to_string()),
    };
  }
  // println!("Received event: {}::{}", _data._type, _data._subtype);
  CEventData {
    _data: string_to_cstring("".to_string()),
    _result:  string_to_cstring("".to_string()),
    _subtype:  string_to_cstring("".to_string()),
    _type:  string_to_cstring("".to_string())
  }
}

fn clib() {
  use xkrome::*;
  let eloop = xk_eloop_new();
  let context = xk_webcontext_new(string_to_cstring("./xkrome_data".to_string()));
  let win = xk_window_new(&eloop);
  let win2 = xk_window_new(&eloop);
  let mut builder = xk_webview_builder_new();
  let mut builder2 = xk_webview_builder_new();
    
  xk_window_set_title(&win, string_to_cstring("Hello wolrd".to_string()));
  xk_window_set_title(&win2, string_to_cstring("Hello wolrd2".to_string()));
  xk_window_set_inner_size(&win, 1080, 800);
  xk_window_set_inner_size(&win2, 1080, 800);
  
  xk_webview_builder_with_url(&mut builder, string_to_cstring("https://duck.com".to_string()));
  xk_webview_builder_with_url(&mut builder2, string_to_cstring("https://duck.com".to_string()));
  xk_webview_builder_build(builder, win, &context);
  xk_webview_builder_build(builder2, win2, &context);
  
  xk_eloop_run(eloop, callback);
}

fn main() {

  let names: Vec<String> = std::env::args().collect();
  match names[1].as_str() {
    "custom_data_directory" => custom_data_directory::main().unwrap(),
    "custom_protocol" => custom_protocol::main().unwrap(),
    "custom_titlebar" => custom_titlebar::main().unwrap(),
    "detect_js_ecma" => detect_js_ecma::main().unwrap(),
    "dragndrop" => dragndrop::main().unwrap(),
    "fullscreen" => fullscreen::main().unwrap(),
    "hello_world" => hello_world::main().unwrap(),
    "html_test" => html_test::main().unwrap(),
    "menu_bar" => menu_bar::main().unwrap(),
    "multi_window" => multi_window::main().unwrap(),
    "rpc" => rpc::main().unwrap(),
    "system_tray_no_menu" => system_tray_no_menu::main().unwrap(),
    "system_tray" => system_tray::main().unwrap(),
    "transparent" => transparent::main().unwrap(),
    "validate_webview" => validate_webview::main().unwrap(),
    "clib" => clib(),
    _ => {

      let mut content = String::from ("Please select one of the examples: \n");  
      content.push_str(" custom_data_directory\n");
      content.push_str(" custom_protocol\n");
      content.push_str(" custom_titlebar\n");
      content.push_str(" detect_js_ecma\n");
      content.push_str(" dragndrop\n");
      content.push_str(" fullscreen\n");
      content.push_str(" hello_world\n");
      content.push_str(" html_test\n");
      content.push_str(" menu_bar\n");
      content.push_str(" multi_window\n");
      content.push_str(" rpc\n");
      content.push_str(" system_tray_no_menu\n");
      content.push_str(" system_tray\n");
      content.push_str(" transparent\n");
      content.push_str(" validate_webview\n");
      println!("{}", content);
      println!("{}", names[1]);
      println!("{:#?}", names);

    }
  }
}
