// ! prefix pub extern fn xk_eloop_

use libc::c_char;

use crate::{IPtr, Ptr, baselib::application::{
        event::{DeviceEvent, Event, TrayEvent, WindowEvent}, 
        event_loop::{ControlFlow, EventLoop}
    }, ceventdata_to_eventdata, eventdata_to_ceventdata};




#[repr(C)]
pub struct CEventData {
    pub _type: *const c_char,
    pub _subtype: *const c_char,
    pub _data: *const c_char,
    pub _result: *const c_char,
}

#[derive(Debug)]
pub struct EventData {
    pub _type: String,
    pub _subtype: String,
    pub _data: String,
    pub _result: String,
}

#[no_mangle] 
pub extern fn xk_eloop_new() -> Ptr {
    IPtr::from(EventLoop::new())
}  

#[no_mangle] 
pub extern fn xk_eloop_run(eloop:  Ptr, callback: extern fn(CEventData) -> CEventData) {
    let eloop = *IPtr::to::<EventLoop<()>>(eloop);
    println!("Event loop is running");
    eloop.run( move |ev: Event<()>, _, control_flow| {
        *control_flow = ControlFlow::Wait;

        let mut edt = EventData {
            _type: "".to_string(),
            _subtype: "".to_string(),
            _data: "".to_string(),
            _result: "".to_string(),
        };

        match ev {
            Event::NewEvents(sc) => {
                edt._type = "NewEvents".to_string();
                edt._subtype = format!("{:?}", sc);
            }
            Event::WindowEvent {event, window_id, ..} => {
                edt._type = "WindowEvent".to_string();
                edt._data = format!("{:?}", window_id);
                edt._subtype = format!("{:?}", event);


                match event {
                    
                    WindowEvent::Resized(size) => {
                        edt._result = format!("{}:{}", size.width, size.height);
                    }
                    
                    WindowEvent::Moved(pos) => {
                        edt._result = format!("{}:{}", pos.x, pos.y);
                    }

                    WindowEvent::DroppedFile(path) => {                        
                        let path = path.as_os_str().to_str().unwrap();
                        edt._result = path.to_string();
                    }
                    
                    WindowEvent::HoveredFile(path) => {
                        let path = path.as_os_str().to_str().unwrap();
                        edt._result = path.to_string();
                    }
                    
                    WindowEvent::ReceivedImeText(text) => {
                        edt._result = text;
                    }
                    
                    WindowEvent::Focused(focus) => {     
                        edt._data = format!("{}", focus);
                    }
                    
                    _ => {}
                }
            },

            Event::DeviceEvent { device_id, event, .. } => {
                edt._type = "DeviceEvent".to_string();
                edt._data = format!("{:?}", device_id);
                edt._subtype = format!("{:?}", event);

                if let DeviceEvent::Key(event) = event {
                    edt._result = format!("{:?}", event);

                }
            }
            
            Event::UserEvent (data) => {
                edt._type = "UserEvent".to_string();
                edt._data = format!("{:?}", data);
            },

            Event::TrayEvent { bounds, event, position, ..} => {
                edt._type = "TrayEvent".to_string();
                edt._data = format!("{}:{}", position.x, position.y);
                edt._result = format!("{}:{};{}:{}", bounds.position.x, bounds.position.y, bounds.size.width, bounds.size.height);
                match event {
                    TrayEvent::DoubleClick => edt._subtype = "DoubleClick".to_string(),
                    TrayEvent::LeftClick => edt._subtype = "LeftClick".to_string(),
                    TrayEvent::RightClick => edt._subtype = "RightClick".to_string(),
                    _ => {},
                }
            },

            _ => {
                edt._type = "NOT_EVENT".to_string();
            }
        }

        if edt._type != "NOT_EVENT" || edt._subtype != "NOT_EVENT" {
            let result = ceventdata_to_eventdata(callback(eventdata_to_ceventdata(edt)));
            
            if result._type == "ControlFlow" {
                match result._result.as_str() {
                    "Exit" => *control_flow = ControlFlow::Exit,
                    "Wait" => *control_flow = ControlFlow::Wait,
                    "Poll" => *control_flow = ControlFlow::Poll,
                    // "WaitUntil(Instant)" => *control_flow = ControlFlow::WaitUntil(Instant),
                    _ => {println!("Callback resulted in {}::{}", result._type,result._subtype )},
                }
            }
        }
    });
}
