
use libc::c_char;
use url::Url;
use wry::webview::WebViewAttributes;

use crate::{IPtr, Ptr, baselib::{webview::{WebViewBuilder}, application::window::Window}, cstring_to_string, string_to_cstring};

#[repr(C)]
pub struct Response {
    pub content: *mut u8,
    pub content_len: u32,
    pub indentifier: *const c_char,
} 

#[no_mangle]
pub extern fn xk_webview_builder_new() -> Ptr {
    IPtr::from(WebViewAttributes::default())
}

#[no_mangle]
pub extern fn xk_webview_builder_with_transparent(builder: &mut Ptr, transparent: bool) {
    let mut builder = IPtr::to_mut_ref::<WebViewAttributes>(builder);
    builder.transparent = transparent;
}

#[no_mangle]
pub extern fn xk_webview_builder_with_visible(builder: &mut Ptr, visible: bool) {
    let mut builder = IPtr::to_mut_ref::<WebViewAttributes>(builder);
    builder.visible = visible;
}

#[no_mangle]
pub extern fn xk_webview_builder_with_initialization_script(builder: &mut Ptr, js: *const c_char) {
    let builder = IPtr::to_mut_ref::<WebViewAttributes>(builder);
    builder.initialization_scripts.push(cstring_to_string(js));
}


/// req_handler handles the webview request, must returns a file content and the type example: "<h1>this is a file content</h1>" text/html
/// The req_handler takes a window id, a request_path and returns a response
#[no_mangle]
pub extern fn xk_webview_builder_with_custom_protocol(builder: &mut Ptr, name: *const c_char, req_handler: extern fn (u32, *const c_char) -> Response) {
    let builder = IPtr::to_mut_ref::<WebViewAttributes>(builder);

    builder.custom_protocols.push(
        (
            cstring_to_string(name), 
            Box::new(move |win: &Window, request: &str| {
                let id = win.id().to_int();
                let res = req_handler(id, string_to_cstring(String::from(request)));
                let content = unsafe { Vec::from_raw_parts(res.content, res.content_len as usize, res.content_len as usize) };

                Ok( (content, cstring_to_string(res.indentifier)))
            })
        )
    );
}

// #[no_mangle]
// pub extern fn xk_webview_builder_with_rpc_handler(builder: &Ptr) {

// }

// #[no_mangle]
// pub extern fn xk_webview_builder_with_file_drop_handler(builder: &Ptr) {

// }

#[no_mangle]
pub extern fn xk_webview_builder_with_url(builder: &mut Ptr, url: *const c_char) {
    let builder = IPtr::to_mut_ref::<WebViewAttributes>(builder);
    builder.url = Some(Url::parse(&cstring_to_string(url)).unwrap());
}

#[no_mangle]
pub extern fn xk_webview_builder_build(builder: Ptr, win: Ptr, web_context: &Ptr) -> Ptr {
    let win = IPtr::to(win);
    let web_context = IPtr::to_ref(web_context);
    let attributes = IPtr::to::<WebViewAttributes>(builder);

    let mut nbuilder = WebViewBuilder::new(*win).unwrap()
        .with_web_context(web_context);

    nbuilder.webview = *attributes;
    
    IPtr::from(nbuilder.build().unwrap())
}