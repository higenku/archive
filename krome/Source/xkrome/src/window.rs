// use xkrome::webview::*;
use baselib::application::{event_loop::*, window::{
        Window,
        // Theme,
        Fullscreen
    }};

use libc::c_char;
use wry::application::{dpi::{PhysicalPosition, PhysicalSize}, window::{ UserAttentionType}};
use std::{ffi::CStr, u32};

use crate::{IPtr, Position, Ptr, Size, baselib};
// ! prefix pub extern fn xk_window

#[no_mangle] 
pub extern fn xk_window_new(eloop: &Ptr) -> Ptr {
    let eloop = IPtr::to_ref::<EventLoop<()>>(eloop);
    IPtr::from(Window::new(eloop).unwrap())
}

#[no_mangle] 
pub extern fn xk_window_request_redraw(win: &Ptr) {
    let win = IPtr::to_ref::<Window>(win);
    win.request_redraw();
}


/* 
TODO:
    Position and Size
*/

#[no_mangle]
pub extern fn xk_window_inner_position(win: &Ptr) -> Position {
    let win = IPtr::to_ref::<Window>(win);
    let PhysicalPosition { x, y } = win.inner_position().unwrap();
    Position {
        x, y
    }
}
#[no_mangle]
pub extern fn xk_window_outer_position(win: &Ptr) -> Position {
    let win = IPtr::to_ref::<Window>(win);
    let PhysicalPosition {x, y} = win.outer_position().unwrap(); 
    Position {
        x, y 
    }
}
#[no_mangle]
pub extern fn xk_window_set_outer_position(win: &Ptr, x: i32, y: i32) {
    let win = IPtr::to_ref::<Window>(win);
    win.set_outer_position(PhysicalPosition {x, y});
}
#[no_mangle]
pub extern fn xk_window_inner_size(win: &Ptr) -> Size {
    let win = IPtr::to_ref::<Window>(win);
    let PhysicalSize {width, height}= win.inner_size();
    Size {
        width, height
    }
}
#[no_mangle]
pub extern fn xk_window_set_inner_size(win: &Ptr, width: u32, height: u32) {
    let win = IPtr::to_ref::<Window>(win);
    win.set_inner_size(PhysicalSize {width, height});
}
#[no_mangle]
pub extern fn xk_window_outer_size(win: &Ptr) -> Size {
    let win = IPtr::to_ref::<Window>(win);
    let PhysicalSize { width, height } = win.outer_size();
    Size {
        width, height
    }

}

/*
    Use -1 for default width and height 
*/
#[no_mangle]
pub extern fn xk_window_set_min_inner_size(win: &Ptr, width: i32, height: i32) {
    let win = IPtr::to_ref::<Window>(win);

    if width == -1 && height == -1 {
        win.set_min_inner_size::<PhysicalSize<u32>>(None);
    }
    else {
        win.set_min_inner_size(Some(PhysicalSize {width, height}));
    }
     
    
}

/*
    Use -1 for default width and height 
*/
#[no_mangle]
pub extern fn xk_window_set_max_inner_size(win: &Ptr, width: i32, height: i32) {
    let win = IPtr::to_ref::<Window>(win);
    if width == -1 && height == -1 {
        win.set_max_inner_size::<PhysicalSize<u32>>(None);
    }
    else {
        win.set_max_inner_size(Some(PhysicalSize {width, height}));
    }
} 


#[no_mangle] 
pub extern fn xk_window_set_title(win: &Ptr, title: *const c_char) {
    let title = unsafe { CStr::from_ptr(title).to_str().unwrap() };
    let win = IPtr::to_ref::<Window>(win);
    win.set_title(title);

}

//? Currently not supported
// #[no_mangle] 
// pub extern fn xk_window_set_menu() {

// }

#[no_mangle] 
pub extern fn xk_window_set_visible(win: &Ptr, visible: bool) {
    let win = IPtr::to_ref::<Window>(win);
    win.set_visible(visible);
}

#[no_mangle] 
pub extern fn xk_window_set_focus(win: &Ptr) {
    let win = IPtr::to_ref::<Window>(win);
    win.set_focus();
}

#[no_mangle] 
pub extern fn xk_window_set_resizable(win: &Ptr, resizable: bool) {
    let win = IPtr::to_ref::<Window>(win);
    win.set_resizable(resizable);
}

#[no_mangle] 
pub extern fn xk_window_set_minimized(win: &Ptr, minimized: bool) {
    let win = IPtr::to_ref::<Window>(win);
    win.set_minimized(minimized);
}

#[no_mangle] 
pub extern fn xk_window_set_maximized(win: &Ptr, maximized: bool) {
    let win = IPtr::to_ref::<Window>(win);
    win.set_maximized(maximized);
}

#[no_mangle] 
pub extern fn xk_window_is_maximized(win: &Ptr) -> bool {
    let win = IPtr::to_ref::<Window>(win);
    win.is_maximized()
}

#[no_mangle] 
pub extern fn xk_window_is_visible(win: &Ptr) -> bool {
    let win = IPtr::to_ref::<Window>(win);
    win.is_visible()
}

#[no_mangle] 
pub extern fn xk_window_is_resizable(win: &Ptr) -> bool {
    let win = IPtr::to_ref::<Window>(win);
    win.is_resizable()
}

#[no_mangle] 
pub extern fn xk_window_is_decorated(win: &Ptr) -> bool {
    let win = IPtr::to_ref::<Window>(win);
    win.is_decorated()
}

#[no_mangle] 
pub extern fn xk_window_set_fullscreen(win: &Ptr, fullscreen: bool) {
    let win = IPtr::to_ref::<Window>(win);
    if fullscreen {
        win.set_fullscreen(Some(Fullscreen::Borderless(None)))
    }
    else {
        win.set_fullscreen(None);
    }
}

// #[no_mangle] 
// pub extern fn xk_window_fullscreen(win: Ptr) {
//     win.fullscreen()
// }

#[no_mangle] 
pub extern fn xk_window_set_decorations() {

}

#[no_mangle] 
pub extern fn xk_window_set_always_on_top(win: &Ptr, always_on_top: bool) {
    let win = IPtr::to_ref::<Window>(win);
    win.set_always_on_top(always_on_top)
}
// Do from Html, api not supported
// #[no_mangle] 
// pub extern fn xk_window_set_window_icon(win: &Ptr, path: *const c_char) {
//     let win = IPtr::to_ref::<Window>(win);
//     let path = cstring_to_string(path).as_str();
//     win.set_window_icon(Some(Icon::from_rgba(vec![250, 250, 250, 2], 4, 4).unwrap()));
// }

// TODO
// #[no_mangle] 
// pub extern fn xk_window_set_ime_position(win: &Ptr) -> bool {
//     let win = IPtr::to_ref::<Window>(win);
    
//     let pos = Position {

//     };
//     win.set_ime_position({

//     });
// }

#[no_mangle] 
pub extern fn xk_window_request_user_attention(win: &Ptr, is_important: bool, is_critical: bool) {
    let win = IPtr::to_ref::<Window>(win);
    if is_important {
    
        if is_critical {
            win.request_user_attention( Some(UserAttentionType::Critical));
        }
        else {
            win.request_user_attention( Some(UserAttentionType::Informational));
        }

    }
    else {
        win.request_user_attention(None);
    }
}

#[no_mangle] 
pub extern fn xk_window_hide_menu(win: &Ptr)  {
    let win = IPtr::to_ref::<Window>(win);
    win.hide_menu();
}

#[no_mangle] 
pub extern fn xk_window_show_menu(win: &Ptr)  {
    let win = IPtr::to_ref::<Window>(win);
    win.show_menu();
}