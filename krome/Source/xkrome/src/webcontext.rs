use std::path::PathBuf;

use libc::c_char;

use crate::{IPtr, Ptr, baselib::{
        webview::WebContext,
    }, cstring_to_string, string_to_cstring};



#[no_mangle]
pub extern fn xk_webcontext_new(data_dir: *const c_char) -> Ptr {
    IPtr::from(WebContext::new(
        Some(PathBuf::from(
            cstring_to_string(data_dir)
        ))
    ))
}

#[no_mangle]
pub extern fn xk_webcontext_data_directory(webcontext: &Ptr) -> *const c_char {
    let webcontext = IPtr::to_ref::<WebContext>(webcontext);
    if let Some(path) = webcontext.data_directory() {
        let path: String = path.as_os_str().to_str().unwrap().to_string();
        string_to_cstring(path)
    }
    else {
        string_to_cstring("".to_string())
    }
}

#[no_mangle]
pub extern fn xk_webcontext_set_allows_automation() {

}
