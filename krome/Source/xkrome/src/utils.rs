use std::{ffi::{CStr, CString}, mem};

use libc::c_char;

use crate::eventloop;
use eventloop::{CEventData, EventData};

pub type Ptr = *const usize;

pub struct IPtr;

#[repr(C)]
pub struct Position {
    pub x: i32,
    pub y: i32
}

#[repr(C)]
pub struct Size {
    pub width: u32,
    pub height: u32
}


impl IPtr {
    // take the data and convert it to a boxed pointer
    pub fn from<T>(stuff: T) -> Ptr {
        Box::into_raw(Box::new(stuff)) as Ptr
    }
    pub fn from_ref<T>(stuff: &Box<T>) -> &Ptr {
        unsafe { 
            mem::transmute::<&Box<T>, &Ptr>(stuff) 
        }
    }

    // return the data of a boxed pointer 
    pub fn to<T>(ptr: Ptr) -> Box<T> {
        let stuff: Box<T> = unsafe {Box::from_raw(ptr as *mut _)};
        stuff
    }

    pub fn to_ref<T>(ptr: &Ptr) -> &Box<T> {    
        unsafe { 
            mem::transmute::<&Ptr, &Box<T>>(ptr)
        }
    }


    pub fn to_mut_ref<T>(ptr: &mut Ptr) -> &mut Box<T> {    
        unsafe { 
            mem::transmute::<&mut Ptr, &mut Box<T>>(ptr)
        }
    }
}

pub fn cstring_to_string(from: *const c_char) -> String {
    let string = unsafe {CStr::from_ptr(from)};
    string.to_str().unwrap().to_string()
}

pub fn string_to_cstring(to: String) -> *mut c_char {
    let c_str = CString::new(to).unwrap();
    CString::into_raw(c_str)
}  

pub fn ceventdata_to_eventdata(edt: CEventData) -> EventData {
    EventData {
        _type: cstring_to_string(edt._type),
        _subtype: cstring_to_string(edt._subtype),
        _data: cstring_to_string(edt._data),
        _result: cstring_to_string(edt._result),
    }
}

pub fn eventdata_to_ceventdata(edt: EventData) -> CEventData {
    CEventData {
        _type: string_to_cstring(edt._type),
        _subtype: string_to_cstring(edt._subtype),
        _data: string_to_cstring(edt._data),
        _result: string_to_cstring(edt._result),
    }
}

pub fn convert_ref_from_ptr<T>(ptr: &Ptr) -> &Box<T> {
    unsafe { 
        mem::transmute(ptr) 
    }
}
