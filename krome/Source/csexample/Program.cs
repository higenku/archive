﻿using System;
using System.Runtime.InteropServices;

namespace csexample
{

    [StructLayout(LayoutKind.Sequential)]
    struct CEventData {
        public string _type;
        public string _subtype;
        public string _data;
        public string _result;
    }

    class XKrome 
    {
        public const string LibName = "xkrome";
        public delegate CEventData callback(CEventData e);

        [DllImport(LibName)]
        public static extern IntPtr xk_eloop_new();
        [DllImport(LibName)]
        public static extern void xk_eloop_run(IntPtr eloopptr, callback callback);
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var eloop = XKrome.xk_eloop_new();
            XKrome.xk_eloop_run(eloop, (CEventData data ) => {
                System.Console.WriteLine($"Received A Event of type {data._type}::{data._subtype}");
                return new CEventData {
                    _data = "",
                    _subtype = "",
                    _type = "",
                    _result = "",
                };
            });
        }
    }
}
