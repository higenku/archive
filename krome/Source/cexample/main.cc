#include "xkrome.h"
#include "iostream"
#include <cstring>

CEventData callback(CEventData data) {
      
    if ( !std::strcmp(data._type, "WindowEvent") && !std::strcmp(data._subtype, "CloseRequested")) {
        std::cout << "Received a callback of type " << data._type << ":" << data._subtype << "\n";  
        std::cout << "Finishing call\n";
        return CEventData {
            ._type = "ControlFlow",
            ._subtype = "",
            ._data = "",
            ._result = "Exit",
        };
    }
    return CEventData {
        ._type = "",
        ._subtype = "",
        ._data = "",
        ._result = "",
    };
}

int main() {
    std::cout << "Hello world\n";
    
    Ptr eloop = xk_eloop_new();
    std::cout << "1\n";
    Ptr window = xk_window_new(&eloop);
    Ptr webcontext = xk_webcontext_new("./xkrome_data");
    Ptr builder = xk_webview_builder_new();

    
    xk_webview_builder_with_url(&builder, "https://duck.com");

    std::cout << "2\n";
    xk_window_set_title(&window, "Hello world!");
    // xk_window_set_window_icon(&window, "/home/litch0/dev/Higenku/Sdks/XKrome/Source/examples/icon.png");
    xk_window_set_inner_size(&window, 1080, 900);
    std::cout << "3\n";
    
    xk_webview_builder_build(builder, window, &webcontext);
    xk_eloop_run(eloop, callback);
    std::cout << "4\n";
    std::cout << "Finished\n";
}

