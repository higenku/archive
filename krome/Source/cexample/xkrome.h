#include <cstdarg>
#include <cstdint>
#include <cstdlib>
#include <ostream>
#include <new>

using Ptr = const uintptr_t*;

struct Response {
  uint8_t *content;
  uint32_t content_len;
  const char *indentifier;
};

struct Position {
  int32_t x;
  int32_t y;
};

struct Size {
  uint32_t width;
  uint32_t height;
};

struct CEventData {
  const char *_type;
  const char *_subtype;
  const char *_data;
  const char *_result;
};

extern "C" {

Ptr xk_webcontext_new(const char *data_dir);

const char *xk_webcontext_data_directory(const Ptr *webcontext);

void xk_webcontext_set_allows_automation();

Ptr xk_webview_builder_new();

void xk_webview_builder_with_transparent(Ptr *builder, bool transparent);

void xk_webview_builder_with_visible(Ptr *builder, bool visible);

void xk_webview_builder_with_initialization_script(Ptr *builder, const char *js);

/// req_handler handles the webview request, must returns a file content and the type example: "<h1>this is a file content</h1>" text/html
/// The req_handler takes a window id, a request_path and returns a response
void xk_webview_builder_with_custom_protocol(Ptr *builder,
                                             const char *name,
                                             Response (*req_handler)(uint32_t, const char*));

void xk_webview_builder_with_url(Ptr *builder, const char *url);

Ptr xk_webview_builder_build(Ptr builder, Ptr win, const Ptr *web_context);

Ptr xk_window_new(const Ptr *eloop);

void xk_window_request_redraw(const Ptr *win);

Position xk_window_inner_position(const Ptr *win);

Position xk_window_outer_position(const Ptr *win);

void xk_window_set_outer_position(const Ptr *win, int32_t x, int32_t y);

Size xk_window_inner_size(const Ptr *win);

void xk_window_set_inner_size(const Ptr *win, uint32_t width, uint32_t height);

Size xk_window_outer_size(const Ptr *win);

void xk_window_set_min_inner_size(const Ptr *win, int32_t width, int32_t height);

void xk_window_set_max_inner_size(const Ptr *win, int32_t width, int32_t height);

void xk_window_set_title(const Ptr *win, const char *title);

void xk_window_set_visible(const Ptr *win, bool visible);

void xk_window_set_focus(const Ptr *win);

void xk_window_set_resizable(const Ptr *win, bool resizable);

void xk_window_set_minimized(const Ptr *win, bool minimized);

void xk_window_set_maximized(const Ptr *win, bool maximized);

bool xk_window_is_maximized(const Ptr *win);

bool xk_window_is_visible(const Ptr *win);

bool xk_window_is_resizable(const Ptr *win);

bool xk_window_is_decorated(const Ptr *win);

void xk_window_set_fullscreen(const Ptr *win, bool fullscreen);

void xk_window_set_decorations();

void xk_window_set_always_on_top(const Ptr *win, bool always_on_top);

void xk_window_request_user_attention(const Ptr *win, bool is_important, bool is_critical);

void xk_window_hide_menu(const Ptr *win);

void xk_window_show_menu(const Ptr *win);

Ptr xk_eloop_new();

void xk_eloop_run(Ptr eloop, CEventData (*callback)(CEventData));

} // extern "C"
