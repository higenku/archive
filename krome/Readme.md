# Higenku Krome (XKrome)

a cross-platform webview with system dialgos (message box, file dialogs, etc)
this is a fork of [Tauri wry](https://github.com/tauri-apps/wry) but without the javascript specific stuff.

## License
Higenku's Krome is license under the [LGPL v2.1](./LICENSE) and the original soure code is license under the MIT and APACHE with can vbe found under [Tauri-Licenses](./Tauri-Licenses) Folder

## Projects Using:
- XPwaR
- XClient
- XMail
