# Higenku Build Environments (XBens) [![pipeline status](https://gitlab.com/higenku/tools/buildenvironments/badges/dev/pipeline.svg)](https://gitlab.com/higenku/tools/buildenvironments/-/commits/dev)
Cross Compilers Build Environments for applications based on Clang and llvm. this repository is 100% automated with weekly builds to update dependencies 
