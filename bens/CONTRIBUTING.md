# Contributing guide
Like the other projects, this contributing guide is just a supplementary of the main one present at [Contributing guide](https://higenku.org/legal/contributing)

## Code Style
There isn't any code style

## creating a new image
there is a convention for images which is: <br>
`quay.io/higenku/b_<name>`

Images should derive from `ghcr.io/higenku/bens/runner:latest` as there is the cross platform compilers, and should be placed at `/docker/Dockerfile.<name>` in the repository
