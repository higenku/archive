use std::{fs, path::Path, rc::Rc};
use glob::glob;
use super::Package;


pub fn build_database(dir_path: &str, repo_url: String) -> Vec<Package> {
    println!("{}", dir_path);
    let mut packs = vec![];
    let files = glob(&Path::new(&dir_path).join("*").to_string_lossy()).expect("Couldn't create database, directory read failed");

    for file in files {
        match file {
            Ok(file) => {
                let pack_path = &file.file_name().unwrap().to_string_lossy();
                println!("Reading {}", pack_path);
                let contents =fs::read_to_string(Path::new(dir_path).join(pack_path.to_string()).join("desc")).expect("Package not found");
                let mut pack = get_info_from_pack_string(contents);
                pack.download_url = Rc::new(format!("{}/{}", &repo_url, &pack.filename));
                pack.repo = Rc::new(repo_url.clone());
                packs.push(pack);
            }
            Err(e) => {
                println!("Error reading folder");
                println!("{:#?}", e);
            }
        }
    }

    packs
}

fn get_info_from_pack_string(contents: String) -> Package {
    let contents = contents.split("\n\n");
    let mut result = Package::default(); 

    for i in contents {

        if i.contains("%FILENAME%") {
            result.filename = Rc::new(i.replace("%FILENAME%\n", ""));
        }
        else if i.contains("%NAME%") {
            result.name = Rc::new(i.replace("%NAME%\n", ""));
        }
        else if i.contains("%BASE%") {
            result.base = Rc::new(i.replace("%BASE%\n", ""));

        }
        else if i.contains("%VERSION%") {
            result.version = Rc::new(i.replace("%VERSION%\n", ""));

        }
        else if i.contains("%SHA256SUM%") {
            result.sha256sum = Rc::new(i.replace("%SHA256SUM%\n", ""));

        }
        else if i.contains("%LICENSE%") {
            result.license = Rc::new(i.replace("%LICENSE%\n", ""));

        }
        else if i.contains("%ARCH%") {
            result.arch = Rc::new(i.replace("%ARCH%\n", ""));

        }
        else if i.contains("%DEPENDS%") {
            let depends = i.replace("%DEPENDS%\n", "");
            let depends: Vec<String> = depends.split("\n").map(|x| String::from(x)).collect();
            
            // get the names of the dependencies
            depends.iter().for_each(|i| result.depends.push(Rc::new(i.to_owned())));
        }
    }

    result
}