use std::{fmt::Debug, rc::Rc};

pub struct Package {
    pub name: Rc<String>,
    pub base: Rc<String>,
    pub version: Rc<String>,
    pub license: Rc<String>,
    pub arch: Rc<String>,
    pub filename: Rc<String>,
    pub sha256sum: Rc<String>,
    pub download_dir: Rc<String>,
    pub depenpath: Rc<String>,
    pub depends: Vec<Rc<String>>,
    pub repo: Rc<String>,
    pub download_url: Rc<String>,
}

impl Package {

    pub fn default() -> Self {
        Package {
            name: Rc::new("".to_string()),
            base: Rc::new("".to_string()),
            version: Rc::new("".to_string()),
            license: Rc::new("".to_string()),
            arch: Rc::new("".to_string()),
            filename: Rc::new("".to_string()),
            sha256sum: Rc::new("".to_string()),
            download_dir: Rc::new("".to_string()),
            depenpath: Rc::new("".to_string()),
            depends: vec![],
            repo: Rc::new("".to_string()),
            download_url: Rc::new("".to_string()),
        }
    }
}

impl Debug for Package {
    fn fmt(&self, _: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        println!("name: {}, version: {}, depenPath: {};", self.name, self.version, self.depenpath);

        Ok(())
    }
}