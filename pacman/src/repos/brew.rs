use std::{sync::{Arc, Mutex}};

use curl::easy::{Easy};
use serde_json::Value;
use crate::utils::{download_with_bearer};

fn get_token_for(tap: String) -> String {
    let url = format!("https://ghcr.io/token?service=ghcr.io&scope=repository:{}:pull", tap);
    let mut handle = Easy::new();
    let response = Arc::new(Mutex::new(Vec::<u8>::new()));

    handle.url(&url).unwrap();
    handle.get(true).unwrap();

    let mut transfer = handle.transfer();
    let response2 = Arc::clone(&response);
    transfer.write_function(move |data| {
        let mut response = response2.lock().unwrap();
        response.extend_from_slice(data);
        Ok(data.len())
    }).unwrap();

    
    transfer.perform().unwrap();
    let token = String::from_utf8(response.lock().unwrap().to_vec()).unwrap();
    let token = token.replace("{\"token\":\"", "").replace("\"}\n", "");
    token
}

fn get(url: String) -> String {

    let mut handle = Easy::new();
    let response = Arc::new(Mutex::new(Vec::<u8>::new()));

    handle.url(&url).unwrap();
    handle.get(true).unwrap();

    let mut transfer = handle.transfer();
    let response2 = Arc::clone(&response);
    transfer.write_function(move |data| {
        let mut response = response2.lock().unwrap();
        response.extend_from_slice(data);
        Ok(data.len())
    }).unwrap();

    
    transfer.perform().unwrap();
    let res = String::from_utf8(response.lock().unwrap().to_vec()).unwrap();
    res
}

pub struct BrewRepository {
    // client: Client<HttpConnector> 
}

impl BrewRepository {
    pub fn new() -> Self {
        BrewRepository {
            // client: Client::builder().build(HttpConnector::new())
        }
    }
    #[export_name = "hello_world"]
    pub fn download_pack(&self, name: &String, path: &String) -> Vec<String> {
        println!("Brew: Downloading {}", name);
        let mut depends = vec![];
        let bottle: Value;
        let url = format!("https://formulae.brew.sh/api/bottle/{}.json", name);
        let contents = get(url);
        bottle = serde_json::from_str(&contents).expect(format!("couldn't read bottle raw: \n{}", contents).as_str());

        for i in bottle["dependencies"].as_array().unwrap() {
            depends.push(i["name"].as_str().unwrap().to_string());
        }

        // https://ghcr.io/v2/{name}/{name2}/{package}/...
        let mut tap = String::from("");
        for (_, value) in bottle["bottles"].as_object().unwrap() {
            tap = value["url"].to_string();
            break;
        }

        let tap: Vec<&str> = tap.split("/").collect();
        let tap = format!("{}/{}/{}", tap[4], tap[5], tap[6]);

        let token = get_token_for(tap);
        
        if bottle["bottles"].get("all").is_some() {
            download_with_bearer(bottle["bottles"]["all"]["url"].as_str().unwrap().to_string(), format!("{}/{}.all.tar.gz", path, bottle["name"].as_str().unwrap()), token.clone());
        }
        else {
            download_with_bearer(bottle["bottles"]["arm64_monterey"]["url"].as_str().unwrap().to_string(), format!("{}/{}.aarch64.tar.gz", path, bottle["name"].as_str().unwrap()), token.clone());
            download_with_bearer(bottle["bottles"]["catalina"]["url"].as_str().unwrap().to_string(), format!("{}/{}.amd64.tar.gz", path, bottle["name"].as_str().unwrap()), token);
        }
        depends
    }

}