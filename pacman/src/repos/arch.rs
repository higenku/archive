use polodb_bson::{Value, mk_array, mk_document};
use polodb_core::{Database};

use crate::{
    pacman::{build_database}, 
    utils::{
        download,
        extract,
    },

};

use std::{fs};
use std::path::Path;

const REPO_EXTRA: &str = "https://ftp5.gwdg.de/pub/linux/archlinux/extra/os/x86_64/";
const REPO_CORE: &str = "https://ftp5.gwdg.de/pub/linux/archlinux/core/os/x86_64/";
const REPO_EXTRA_DB: &str = "extra.db.tar.gz";
const REPO_CORE_DB: &str = "core.db.tar.gz";

pub struct ArchRepository {
    pub db: Database,
}

impl ArchRepository {
    pub fn new(remake_db: bool) -> Self {
        // basic setup
        let tmp  = String::from("tmp/arch");
        let tmppath  = Path::new(&tmp);
        let coredb = format!("{}/{}", tmp, REPO_CORE_DB);
        let extradb = format!("{}/{}", tmp, REPO_EXTRA_DB);
        
        if remake_db {
            if Path::new("arch.db").exists() {
                fs::remove_file("arch.db").expect("Coudln't remove db file");
            }
    
            if tmppath.exists() {
                fs::remove_dir_all(tmppath).expect("tmp dir does not exist");
            }
    
            fs::create_dir_all(&tmppath).expect("Error Creating dirs");
    
            println!("Updating database");
            download(format!("{}{}", REPO_CORE, REPO_CORE_DB).as_str(),    coredb.as_str());
            download(format!("{}{}", REPO_EXTRA, REPO_EXTRA_DB).as_str(),   extradb.as_str());
            
            println!("Extracting database");
            extract(format!("{}", coredb).as_str(), "tmp/core");
            extract(format!("{}", extradb).as_str(), "tmp/extra");
    
            fs::create_dir_all(tmp.clone() + "/core").unwrap();
            fs::create_dir_all(tmp.clone() + "/extra").unwrap();
    
            println!("Merging databases");
            subprocess::Exec::shell(format!("tar zxf {}  -C {}", coredb,  tmp.clone() + "/core")).popen().expect("error extracting core.db");
            subprocess::Exec::shell(format!("tar zxf {}  -C {}", extradb, tmp.clone() + "/extra")).popen().expect("error extracting extra.db");
    
            fs::remove_file(coredb).unwrap();
            fs::remove_file(extradb).unwrap();
            
            println!("Creating database");
            let mut packs = Vec::new();
            packs.append(&mut build_database(&(tmp.clone() + "/extra"), REPO_EXTRA.to_string()));
            packs.append(&mut build_database(&(tmp.clone() + "/core" ), REPO_CORE.to_string()));
    
            let mut db = Database::open_file("arch.db").expect("Couldn't open database");
            let mut col = db.collection("pkgs").expect("coudn't create database");
            
            for i in packs {
                let mut depends = mk_array!();
                
                for d in i.depends {
                    depends.push(Value::String(d));
                }

                let mut doc = &mut mk_document!{
                    "name": i.name,
                    "base": i.base,
                    "version": i.version,
                    "license": i.license,
                    "arch": i.arch,
                    "filename": i.filename,
                    "sha256sum": i.sha256sum,
                    "download_dir": i.download_dir,
                    "depenpath": i.depenpath,
                    "depends":  depends,
                    "repo": i.repo,
                    "download_url": i.download_url,
                };
                col.insert(&mut doc).expect("Couldn't alter database");
            }
            println!("Finished!");
            return ArchRepository { db };
        }
        let db = Database::open_file("arch.db").expect("Couldn't open database");
        ArchRepository { db }
    }
    
    pub fn download_pack(&mut self, name: &String, path: &String) -> Vec<String> {
        let mut name = name.clone();
        if name.starts_with("\"") && name.ends_with("\"") {
            name = name[1..name.len()-1].to_string();
        }
        let mut col = self.db.collection("pkgs").expect("Couldn't open database");
        let doc = col.find_one(&mk_document! {
            "name": name.as_str()
        }).expect("couldn't open connection to database");

        if let Some(doc) = doc {
            let url = doc.get("download_url").unwrap();
            let path = Path::new(path).join(format!("{}.tar.zst", doc.get("name").unwrap().to_string()));
            
            download(url.to_string(), path.to_string_lossy().to_string());
            
            let dependencies = doc.get("depends").unwrap().try_array().unwrap();
            let mut _dependencies = vec![];
            for i in dependencies.iter() {
                let i = i.try_string().unwrap();
                _dependencies.push(i.to_string());
            }
            return _dependencies;
        }
        panic!("Package not found {}", name)
    }
}