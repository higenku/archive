mod utils;
mod repos;
mod pacman;

use std::env;

use repos::{ArchRepository, BrewRepository, MsysRepository};

fn main() {    
    let curdir = env::current_dir().expect("Failed to get current directory");
    let packs_dir = if let Ok(e) = env::var("PACKS_DIR") { println!("PACKS_DIR is setted"); e } else { curdir.to_string_lossy().to_string() };

    let mut args: Vec<String> = std::env::args().collect();
    let sync = args.contains(&"-s".to_string());
    if sync {
        args.remove(args.iter().position(|e| e == &"-s".to_string()).unwrap());
    }

    let exedir = env::current_exe().unwrap();
    let exedir = exedir.as_path().parent().unwrap();

    env::set_current_dir(exedir).unwrap();
    let mut arch = ArchRepository::new(sync);
    let brew = BrewRepository::new();
    let mut msys = MsysRepository::new(sync);
    env::set_current_dir(curdir).unwrap();
 
    let args = args[1..args.len()].to_vec();
    for pack in args {
        let osx_path = format!("download/osx");
        let win_path = format!("download/win");
        let lin_path = format!("download/lin");

        let depens_win  = msys.download_pack(&format!("mingw-w64-ucrt-x86_64-{}", &pack), &win_path);
        let depens_osx = brew.download_pack(&pack, &osx_path);
        let depens_lin = arch.download_pack(&pack, &lin_path);

        for depen in depens_osx {
            // TODO: compare if it exists in the packs folder
            brew.download_pack(&depen, &osx_path);
        }
        
        for depen in depens_win {
            // TODO: compare if it exists in the packs folder
            msys.download_pack(&depen, &win_path);
        }
        
        for depen in depens_lin {
            // TODO: compare if it exists in the packs folder
            arch.download_pack(&depen, &lin_path);
        }

        
    }
}   