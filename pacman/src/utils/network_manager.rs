use std::path::Path;
use subprocess::*;

pub fn download<S: AsRef<str>>(url: S, file_path: S) {
    let url = url.as_ref();
    let file_path = file_path.as_ref();
    
    println!("downloading {}", url);
    std::fs::create_dir_all(Path::new(file_path).parent().unwrap()).expect("Could not create the path to the downloaded file");
    Exec::shell(format!("wget -q --show-progress --progress=bar:force -O {} {}", file_path, url)).stdout(Redirection::None).popen().unwrap();
}


pub fn download_with_bearer<S: AsRef<str>>(url: S, file_path: S, token: S) {
    let url = url.as_ref();
    let file_path = file_path.as_ref();
    let token = token.as_ref();
    
    println!("downloading {}", url);
    std::fs::create_dir_all(Path::new(file_path).parent().unwrap()).expect("Could not create the path to the downloaded file");
    Exec::shell(format!("wget -q --show-progress --progress=bar:force -O {} {} --header=\"Authorization: Bearer {}\"", file_path, url, token)).stdout(Redirection::None).popen().unwrap();
}