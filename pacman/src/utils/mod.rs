pub mod network_manager;
pub use network_manager::*;

pub fn extract(path: &str, out: &str) {
    subprocess::Exec::shell(format!("yes | 7z x {}  -o{}", path,  out)).popen().expect("error extracting core.db");
}
