# Higenku Universal Package Manager (XPacman)

Reimplementation of Pamcan but only for downloading and extracting  packages from [homebrew](https://brew.sh), [archlinux](https://archlinux.org/packages/) and [msys2](https://packages.msys2.org)

## Functionality
This project was created to download dependencies from multiple repositories and get the dependencies. We interact directly with the pacman database and get the information from there

It is currently used by the GkPacks repository